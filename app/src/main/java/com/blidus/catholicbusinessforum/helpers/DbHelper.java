package com.blidus.catholicbusinessforum.helpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.blidus.catholicbusinessforum.adapters.dbAdapters.MessageDbAdapter;
import com.blidus.catholicbusinessforum.global.Constants;

import static com.blidus.catholicbusinessforum.adapters.dbAdapters.ContactDBAdapter.CONTACTS_TABLE_NAME;
import static com.blidus.catholicbusinessforum.adapters.dbAdapters.ContactDBAdapter.CREATE_CONTACT_TABLE;

/**
 * Created by Bimal on 17-02-2017.
 */

public class DbHelper extends SQLiteOpenHelper {
    public DbHelper(Context cxt) {
        super(cxt, Constants.DATABASE.DATABASE_NAME, null, Constants.DATABASE.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_CONTACT_TABLE);
        db.execSQL(MessageDbAdapter.CREATE_MESSAGE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w("DB Upgredation", "Upgrading database from version " + oldVersion + " to " + newVersion + ". This will erase all the data.");
        db.execSQL("DROP TABLE IF EXIST " + CONTACTS_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXIST " + MessageDbAdapter.MESSAGE_TABLE_NAME);
        onCreate(db);
    }
}
