package com.blidus.catholicbusinessforum.helpers;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by Bimal on 11-12-2016.
 */
public class DrawerToggle extends View {
    private Context context;
    private Paint mainPaint;
    private Paint centerLinePaint;
    private float dp;
    private float drawerSlideProgress = 0f;

    public DrawerToggle(Context context) {
        super(context);
        init(context);
    }

    public DrawerToggle(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public DrawerToggle(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public DrawerToggle(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context c) {
        this.context = c;
        this.dp = context.getResources().getDisplayMetrics().density;
        mainPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mainPaint.setColor(0xFFFFFFFF);
        mainPaint.setStrokeWidth(5f);
        mainPaint.setStyle(Paint.Style.STROKE);

        centerLinePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        centerLinePaint.setColor(0xFFFFFFFF);
        centerLinePaint.setStrokeWidth(5f);
        centerLinePaint.setStyle(Paint.Style.STROKE);

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
//        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
//        int widthMag = MeasureSpec.getSize(widthMeasureSpec);
//        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
//        int heightMag = MeasureSpec.getSize(heightMeasureSpec);
//
//        if (widthMode == MeasureSpec.AT_MOST) widthMag = (int) (30 * dp);
//        if (heightMode == MeasureSpec.AT_MOST) heightMag = (int) (30 * dp);
//
//        setMeasuredDimension(MeasureSpec.makeMeasureSpec(widthMag, widthMode), MeasureSpec.makeMeasureSpec(heightMag, heightMode));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        int width = getWidth();
        int height = getHeight();
        int dim = Math.min(width, height);
        float widthPadding;
        float heightPadding;
        if (width > height) {
            widthPadding = (width - height) / 2f;
            heightPadding = 0f;
        } else {
            heightPadding = (height - width) / 2f;
            widthPadding = 0f;
        }
        float xPos1 = (dim / 4f + widthPadding) + 2 * dp;
        float xPos2 = (3 * dim / 4f + widthPadding) - 2 * dp;
        float yPos1 = (dim / 4f + heightPadding) + 2 * dp;
        float yPos2 = (3 * dim / 4f + heightPadding) - 2 * dp;
        float yPosC = (dim / 2f + heightPadding);

        centerLinePaint.setAlpha((int) (255 * (1 - drawerSlideProgress)));
//        canvas.drawCircle(dim / 2 + widthPadding, dim / 2 + heightPadding, dim / 2 - 2*dp, mainPaint);
        canvas.drawLine(xPos1, yPos1, xPos2, yPos1 + (drawerSlideProgress * (yPos2 - yPos1)), mainPaint);
        canvas.drawLine(xPos1, yPos2, xPos2, yPos2 - (drawerSlideProgress * (yPos2 - yPos1)), mainPaint);
        canvas.drawLine(xPos1, yPosC, xPos2, yPosC, centerLinePaint);
    }

    public void setDrawerSlideProgress(float progress) {
        drawerSlideProgress = progress;
        invalidate();
    }
}
