package com.blidus.catholicbusinessforum.dataModels.callbackDatatypes;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bimal on 04-02-2017.
 */

public class ChatMsgItem implements Parcelable {

    public static final Creator<ChatMsgItem> CREATOR = new Creator<ChatMsgItem>() {
        @Override
        public ChatMsgItem createFromParcel(Parcel in) {
            return new ChatMsgItem(in);
        }

        @Override
        public ChatMsgItem[] newArray(int size) {
            return new ChatMsgItem[size];
        }
    };

    private String MessageID;
    private String UserId;
    private String UserFullName;
    private String UserMobile;
    private String UserEmail;
    private String UserCompanyName;
    private String MessageText;
    private String MessageFile;
    private String MessageType;
    private String MsgDateTime;
    private String MessageDelFlag;

    protected ChatMsgItem(Parcel in) {
        MessageID = in.readString();
        UserId = in.readString();
        UserFullName = in.readString();
        UserMobile = in.readString();
        UserEmail = in.readString();
        UserCompanyName = in.readString();
        MessageText = in.readString();
        MessageFile = in.readString();
        MessageType = in.readString();
        MsgDateTime = in.readString();
        MessageDelFlag = in.readString();
    }

    public String getMessageID() {
        return MessageID;
    }
    public void setMessageID(String messageID) {
        MessageID = messageID;
    }
    public String getUserId() {
        return UserId;
    }
    public void setUserId(String userId) {
        UserId = userId;
    }
    public String getUserFullName() {
        return UserFullName;
    }
    public void setUserFullName(String userFullName) {
        UserFullName = userFullName;
    }
    public String getUserMobile() {
        return UserMobile;
    }
    public void setUserMobile(String userMobile) {
        UserMobile = userMobile;
    }
    public String getUserEmail() {
        return UserEmail;
    }
    public void setUserEmail(String userEmail) {
        UserEmail = userEmail;
    }
    public String getUserCompanyName() {
        return UserCompanyName;
    }
    public void setUserCompanyName(String userCompanyName) {
        UserCompanyName = userCompanyName;
    }
    public String getMessageText() {
        return MessageText;
    }
    public void setMessageText(String messageText) {
        MessageText = messageText;
    }
    public String getMessageFile() {
        return MessageFile;
    }
    public void setMessageFile(String messageFile) {
        MessageFile = messageFile;
    }
    public String getMessageType() {
        return MessageType;
    }
    public void setMessageType(String messageType) {
        MessageType = messageType;
    }
    public String getMsgDateTime() {
        return MsgDateTime;
    }
    public void setMsgDateTime(String msgDateTime) {
        MsgDateTime = msgDateTime;
    }
    public String getMessageDelFlag() {
        return MessageDelFlag;
    }
    public void setMessageDelFlag(String messageDelFlag) {
        MessageDelFlag = messageDelFlag;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(MessageID);
        parcel.writeString(UserId);
        parcel.writeString(UserFullName);
        parcel.writeString(UserMobile);
        parcel.writeString(UserEmail);
        parcel.writeString(UserCompanyName);
        parcel.writeString(MessageText);
        parcel.writeString(MessageFile);
        parcel.writeString(MessageType);
        parcel.writeString(MsgDateTime);
        parcel.writeString(MessageDelFlag);

    }
}
