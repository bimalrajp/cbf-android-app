package com.blidus.catholicbusinessforum.dataModels.dbDatatypes;

/**
 * Created by Bimal on 16-02-2017.
 */

public class Contact {

    private String PKUserID;
    private String UserType;
    private String UserFlag;
    private String UserMessageFlag;
    private String UserFullName;
    private String UserGender;
    private String UserHouseName;
    private String UserAddress;
    private String UserCity;
    private String UserPin;
    private String UserMobile;
    private String UserEmail;
    private String UserDescription;
    private String UserImage;
    private String CompanyName;
    private String ProductName;
    private String ProductDescreption;

    public Contact(String PKUserID, String userFlag, String userMessageFlag, String userFullName, String userGender, String userHouseName, String userAddress, String userCity, String userPin, String userMobile, String userEmail, String userDescription, String userImage, String companyName, String productName, String productDescreption) {
        this.PKUserID = PKUserID;
        this.UserFlag = userFlag;
        this.UserMessageFlag = userMessageFlag;
        this.UserFullName = userFullName;
        this.UserGender = userGender;
        this.UserHouseName = userHouseName;
        this.UserAddress = userAddress;
        this.UserCity = userCity;
        this.UserPin = userPin;
        this.UserMobile = userMobile;
        this.UserEmail = userEmail;
        this.UserDescription = userDescription;
        this.UserImage = userImage;
        this.CompanyName = companyName;
        this.ProductName = productName;
        this.ProductDescreption = productDescreption;
    }

    public String getPKUserID() {
        return PKUserID;
    }
    /*public String getUserType() {
        return UserType;
    }*/
    public String getUserFlag() {
        return UserFlag;
    }
    public String getUserMessageFlag() {
        return UserMessageFlag;
    }
    public String getUserFullName() {
        return UserFullName;
    }
    public String getUserGender() {
        return UserGender;
    }
    public String getUserHouseName() {
        return UserHouseName;
    }
    public String getUserAddress() {
        return UserAddress;
    }
    public String getUserCity() {
        return UserCity;
    }
    public String getUserPin() {
        return UserPin;
    }
    public String getUserMobile() {
        return UserMobile;
    }
    public String getUserEmail() {
        return UserEmail;
    }
    public String getUserDescription() {
        return UserDescription;
    }
    public String getUserImage() {
        return UserImage;
    }
    public String getCompanyName() {
        return CompanyName;
    }
    public String getProductName() {
        return ProductName;
    }
    public String getProductDescreption() {
        return ProductDescreption;
    }

    public String toString(){
        return "PKUserID: "+PKUserID
                +" UserFlag: "+UserFlag
                +" UserMessageFlag: "+UserMessageFlag
                +" UserFullName: "+UserFullName
                +" UserGender: "+UserGender
                +" UserHouseName: "+UserHouseName
                +" UserAddress: "+UserAddress
                +" UserCity: "+UserCity
                +" UserPin: "+UserPin
                +" UserMobile: "+UserMobile
                +" UserEmail: "+UserEmail
                +" UserDescription: "+UserDescription
                +" UserImage: "+UserImage
                +" CompanyName: "+CompanyName
                +" ProductName: "+ProductName
                +" ProductDescreption: "+ProductDescreption;
    }
}
