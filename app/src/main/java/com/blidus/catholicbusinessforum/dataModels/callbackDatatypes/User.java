package com.blidus.catholicbusinessforum.dataModels.callbackDatatypes;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bimal on 23-12-2016.
 */

public class User implements Parcelable {
	public static final Creator<User> CREATOR = new Creator<User>() {
		@Override
		public User createFromParcel(Parcel in) {
			return new User(in);
		}

		@Override
		public User[] newArray(int size) {
			return new User[size];
		}
	};


	public String FKHouseID;
	public String PBUserImage;
	public String PBUserGender;
	public String FKProfessionID;
	public String PBUserRegistrationTime;
	public String PBUserDOB;
	public String PBUserEmail;
	public String PBUserMobileNo;
	public String PBUserID;
	public String PBUserRegistrationDate;
	public String PBUserBloodGroup;
	public String PBUserName;

	protected User(Parcel in) {
		FKHouseID = in.readString();
		PBUserImage = in.readString();
		PBUserGender = in.readString();
		FKProfessionID = in.readString();
		PBUserRegistrationTime = in.readString();
		PBUserDOB = in.readString();
		PBUserEmail = in.readString();
		PBUserMobileNo = in.readString();
		PBUserID = in.readString();
		PBUserRegistrationDate = in.readString();
		PBUserBloodGroup = in.readString();
		PBUserName = in.readString();
	}

	public String getFKHouseID() {
		return FKHouseID;
	}

	public void setFKHouseID(String FKHouseID) {
		this.FKHouseID = FKHouseID;
	}

	public String getPBUserImage() {
		return PBUserImage;
	}

	public void setPBUserImage(String PBUserImage) {
		this.PBUserImage = PBUserImage;
	}

	public String getPBUserGender() {
		return PBUserGender;
	}

	public void setPBUserGender(String PBUserGender) {
		this.PBUserGender = PBUserGender;
	}

	public String getFKProfessionID() {
		return FKProfessionID;
	}

	public void setFKProfessionID(String FKProfessionID) {
		this.FKProfessionID = FKProfessionID;
	}

	public String getPBUserRegistrationTime() {
		return PBUserRegistrationTime;
	}

	public void setPBUserRegistrationTime(String PBUserRegistrationTime) {
		this.PBUserRegistrationTime = PBUserRegistrationTime;
	}

	public String getPBUserDOB() {
		return PBUserDOB;
	}

	public void setPBUserDOB(String PBUserDOB) {
		this.PBUserDOB = PBUserDOB;
	}

	public String getPBUserEmail() {
		return PBUserEmail;
	}

	public void setPBUserEmail(String PBUserEmail) {
		this.PBUserEmail = PBUserEmail;
	}

	public String getPBUserMobileNo() {
		return PBUserMobileNo;
	}

	public void setPBUserMobileNo(String PBUserMobileNo) {
		this.PBUserMobileNo = PBUserMobileNo;
	}

	public String getPBUserID() {
		return PBUserID;
	}

	public void setPBUserID(String PBUserID) {
		this.PBUserID = PBUserID;
	}

	public String getPBUserRegistrationDate() {
		return PBUserRegistrationDate;
	}

	public void setPBUserRegistrationDate(String PBUserRegistrationDate) {
		this.PBUserRegistrationDate = PBUserRegistrationDate;
	}

	public String getPBUserBloodGroup() {
		return PBUserBloodGroup;
	}

	public void setPBUserBloodGroup(String PBUserBloodGroup) {
		this.PBUserBloodGroup = PBUserBloodGroup;
	}

	public String getPBUserName() {
		return PBUserName;
	}

	public void setPBUserName(String PBUserName) {
		this.PBUserName = PBUserName;
	}

	@Override
	public String toString() {
		return "User [FKHouseID = " + FKHouseID + ", PBUserImage = " + PBUserImage + ", PBUserGender = " + PBUserGender + ", FKProfessionID = " + FKProfessionID + ", PBUserRegistrationTime = " + PBUserRegistrationTime + ", PBUserDOB = " + PBUserDOB + ", PBUserEmail = " + PBUserEmail + ", PBUserMobileNo = " + PBUserMobileNo + ", PBUserID = " + PBUserID + ", PBUserRegistrationDate = " + PBUserRegistrationDate + ", PBUserBloodGroup = " + PBUserBloodGroup + ", PBUserName = " + PBUserName + "]";
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(FKHouseID);
		dest.writeString(PBUserImage);
		dest.writeString(PBUserGender);
		dest.writeString(FKProfessionID);
		dest.writeString(PBUserRegistrationTime);
		dest.writeString(PBUserDOB);
		dest.writeString(PBUserEmail);
		dest.writeString(PBUserMobileNo);
		dest.writeString(PBUserID);
		dest.writeString(PBUserRegistrationDate);
		dest.writeString(PBUserBloodGroup);
		dest.writeString(PBUserName);
	}
}
