package com.blidus.catholicbusinessforum.dataModels.callbackDatatypes;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bimal on 16-02-2017.
 */

public class ContactItem implements Parcelable {

    public static final Creator<ContactItem> CREATOR = new Creator<ContactItem>() {
        @Override
        public ContactItem createFromParcel(Parcel in) {
            return new ContactItem(in);
        }

        @Override
        public ContactItem[] newArray(int size) {
            return new ContactItem[size];
        }
    };

    private String PKUserID;
    private String UserFlag;
    private String UserMessageFlag;
    private String UserFullName;
    private String UserGender;
    private String UserHouseName;
    private String UserAddress;
    private String UserCity;
    private String UserPin;
    private String UserMobile;
    private String UserEmail;
    private String UserDescription;
    private String UserImage;
    private String CompanyName;
    private String ProductName;
    private String ProductDescreption;


    protected ContactItem(Parcel in) {
        PKUserID = in.readString();
        UserFlag = in.readString();
        UserMessageFlag = in.readString();
        UserFullName = in.readString();
        UserGender = in.readString();
        UserHouseName = in.readString();
        UserAddress = in.readString();
        UserCity = in.readString();
        UserPin = in.readString();
        UserMobile = in.readString();
        UserEmail = in.readString();
        UserDescription = in.readString();
        UserImage = in.readString();
        CompanyName = in.readString();
        ProductName = in.readString();
        ProductDescreption = in.readString();
    }

    public String getItemPKUserID() {
        return PKUserID;
    }
    public void setPKUserID(String PKUserID) {
        this.PKUserID = PKUserID;
    }
    public String getUserFlag() {
        return UserFlag;
    }
    public void setUserFlag(String userFlag) {
        UserFlag = userFlag;
    }
    public String getUserMessageFlag() {
        return UserMessageFlag;
    }
    public void setUserMessageFlag(String userMessageFlag) {
        UserMessageFlag = userMessageFlag;
    }
    public String getUserFullName() {
        return UserFullName;
    }
    public void setUserFullName(String userFullName) {
        UserFullName = userFullName;
    }
    public String getUserGender() {
        return UserGender;
    }
    public void setUserGender(String userGender) {
        UserGender = userGender;
    }
    public String getUserHouseName() {
        return UserHouseName;
    }
    public void setUserHouseName(String userHouseName) {
        UserHouseName = userHouseName;
    }
    public String getUserAddress() {
        return UserAddress;
    }
    public void setUserAddress(String userAddress) {
        UserAddress = userAddress;
    }
    public String getUserCity() {
        return UserCity;
    }
    public void setUserCity(String userCity) {
        UserCity = userCity;
    }
    public String getUserPin() {
        return UserPin;
    }
    public void setUserPin(String userPin) {
        UserPin = userPin;
    }
    public String getUserMobile() {
        return UserMobile;
    }
    public void setUserMobile(String userMobile) {
        UserMobile = userMobile;
    }
    public String getUserEmail() {
        return UserEmail;
    }
    public void setUserEmail(String userEmail) {
        UserEmail = userEmail;
    }
    public String getUserDescription() {
        return UserDescription;
    }
    public void setUserDescription(String userDescription) {
        UserDescription = userDescription;
    }
    public String getUserImage() {
        return UserImage;
    }
    public void setUserImage(String userImage) {
        UserImage = userImage;
    }
    public String getCompanyName() {
        return CompanyName;
    }
    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }
    public String getProductName() {
        return ProductName;
    }
    public void setProductName(String productName) {
        ProductName = productName;
    }
    public String getProductDescreption() {
        return ProductDescreption;
    }
    public void setProductDescreption(String productDescreption) {
        ProductDescreption = productDescreption;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(PKUserID);
        parcel.writeString(UserFlag);
        parcel.writeString(UserMessageFlag);
        parcel.writeString(UserFullName);
        parcel.writeString(UserGender);
        parcel.writeString(UserHouseName);
        parcel.writeString(UserAddress);
        parcel.writeString(UserCity);
        parcel.writeString(UserPin);
        parcel.writeString(UserMobile);
        parcel.writeString(UserEmail);
        parcel.writeString(UserDescription);
        parcel.writeString(UserImage);
        parcel.writeString(CompanyName);
        parcel.writeString(ProductName);
        parcel.writeString(ProductDescreption);
    }
}
