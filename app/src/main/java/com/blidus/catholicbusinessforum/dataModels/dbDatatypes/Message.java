package com.blidus.catholicbusinessforum.dataModels.dbDatatypes;

/**
 * Created by Bimal on 12-12-2016.
 */

public class Message {
    private long msg_id;
    private String user_id;
    private String user_name;
    private String company_name;
    private String message;
    private String message_file;
    private long dateCreatedMilli;
    private int type;

    public Message(String user_id, String user_name, String company_name, String message, String message_file, int type, long msg_id, long dateCreatedMilli){
        this.user_id = user_id;
        this.user_name = user_name;
        this.company_name = company_name;
        this.message = message;
        this.message_file = message_file;
        this.type = type;
        this.msg_id = msg_id;
        this.dateCreatedMilli = dateCreatedMilli;
    }

    public String getUser_Id() {
        return user_id;
    }
    public String getUsername() {
        return user_name;
    }
    public String getCompany_Name() {
        return company_name;
    }
    public String getMessage_Text() {
        return message;
    }
    public String getMessage_File() {
        return message_file;
    }
    public int getMessage_Type() {
        return type;
    }
    public long getDateCreatedMilli() {
        return dateCreatedMilli;
    }
    public long getMsgid() {
        return msg_id;
    }

    public String toString(){
        return "ID: "+msg_id
                +" UserId: "+user_id
                +" Username: "+user_name
                +" Company: "+company_name
                +" Message: "+message
                +" MessageFile: "+message_file
                +" MessageType: "+type
                +" Date: ";
    }

    /*public int getAssociatedDrawable(){
        return categoryToDrawable(category);
    }

    private static int categoryToDrawable(Category messageCategory) {
        switch (messageCategory){
            case TEXT:
                return R.drawable.new_note;
            case IMAGE:
                return R.drawable.image;
            case AUDIO:
                return R.drawable.audio;
            case VIDEO:
                return R.drawable.attach;
            case DOCUMENT:
                return R.drawable.attach;
        }
        return R.drawable.attach;
    }*/
}
