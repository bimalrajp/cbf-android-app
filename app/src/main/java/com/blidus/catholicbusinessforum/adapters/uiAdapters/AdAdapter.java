package com.blidus.catholicbusinessforum.adapters.uiAdapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.blidus.catholicbusinessforum.R;
import com.blidus.catholicbusinessforum.global.Constants;

/**
 * Created by Bimal on 25-02-2017.
 */

public class AdAdapter extends PagerAdapter {

    private int[] ads = Constants.ADS.ADS_IDS;
    Context ctx;
    private LayoutInflater layoutInflater;
    public static final int IMG_COUNT = Constants.ADS.ADS_IMAGE_COUNT;

    public AdAdapter(Context ctx){
        this.ctx = ctx;
    }

    @Override
    public int getCount() {
        return ads.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view==(LinearLayout)object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        layoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = layoutInflater.inflate(R.layout.ad_layout, container, false);
        ImageView adImage = (ImageView) itemView.findViewById(R.id.img_ad);
        adImage.setImageResource(ads[position]);
        adImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(Constants.ADS.AD_URLS[position]));
                ctx.startActivity(i);
            }
        });
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

    /*public static Intent getOpenFacebookIntent(Context context) {
        try {
            context.getPackageManager()
                    .getPackageInfo("com.facebook.katana", 0); //Checks if FB is even installed.
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse("fb://profile/254175194653125")); //Trys to make intent with FB's URI
        } catch (Exception e) {
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://www.facebook.com/arkverse")); //catches and opens a url to the desired page
        }
    }*/
}