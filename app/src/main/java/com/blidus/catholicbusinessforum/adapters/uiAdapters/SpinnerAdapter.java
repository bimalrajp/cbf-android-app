package com.blidus.catholicbusinessforum.adapters.uiAdapters;

import android.content.Context;
import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.blidus.catholicbusinessforum.App;
import com.blidus.catholicbusinessforum.R;
import com.blidus.catholicbusinessforum.global.PreferenceKeys;

/**
 * Created by Bimal on 10-03-2017.
 */

public class SpinnerAdapter extends ArrayAdapter <String> {

    private Context ctx;
    private String[] items;
    private TextView txt_spnr_list_item;

    private int colorPrimary, colorAccent, colorBackgroundImage;

    public SpinnerAdapter(Context context, String[] resource) {
        super(context, R.layout.spnr_register_gender_items, resource);
        this.ctx = context;
        this.items = resource;
        if (App.preferences.getBoolean(PreferenceKeys.USER_DEFINED_THEME_COLOR, false)) {
            colorPrimary = Color.parseColor(App.preferences.getString(PreferenceKeys.USER_DEFINED_THEME_COLOR_PRIMARY, null));
            colorAccent = Color.parseColor(App.preferences.getString(PreferenceKeys.USER_DEFINED_THEME_COLOR_ACCENT, null));
            colorBackgroundImage = Color.parseColor(App.preferences.getString(PreferenceKeys.USER_DEFINED_THEME_COLOR_BACKGROUND_IMAGE, null));
        } else {
            colorPrimary = ContextCompat.getColor(ctx, R.color.colorPrimary);
            colorAccent = ContextCompat.getColor(ctx, R.color.colorAccent);
            colorBackgroundImage = ContextCompat.getColor(ctx, R.color.colorBackgroundImage);
        }
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null){
            LayoutInflater layoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.spnr_register_gender_items,null);
        }
        txt_spnr_list_item = (TextView) convertView.findViewById(R.id.txt_spnr_list_item);
        txt_spnr_list_item.setText(items[position]);
        if (position == 0){
            txt_spnr_list_item.setTextColor(colorAccent);
        } else {
            txt_spnr_list_item.setTextColor(colorPrimary);
        }
        return convertView;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null){
            LayoutInflater layoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.spnr_register_gender_items,null);
        }
        txt_spnr_list_item = (TextView) convertView.findViewById(R.id.txt_spnr_list_item);
        txt_spnr_list_item.setText(items[position]);
        if (position == 0){
            txt_spnr_list_item.setTextColor(colorAccent);
        } else {
            txt_spnr_list_item.setTextColor(colorPrimary);
        }
        return convertView;
    }
}
