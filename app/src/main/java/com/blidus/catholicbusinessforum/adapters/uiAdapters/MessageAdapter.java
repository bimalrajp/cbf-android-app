package com.blidus.catholicbusinessforum.adapters.uiAdapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import com.google.android.material.appbar.AppBarLayout;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.Toast;

import com.blidus.catholicbusinessforum.App;
import com.blidus.catholicbusinessforum.R;
import com.blidus.catholicbusinessforum.adapters.dbAdapters.MessageDbAdapter;
import com.blidus.catholicbusinessforum.dataModels.dbDatatypes.Message;
import com.blidus.catholicbusinessforum.global.Constants;
import com.blidus.catholicbusinessforum.global.PreferenceKeys;
import com.blidus.catholicbusinessforum.holders.MessageHolder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;

import org.apache.commons.lang3.StringEscapeUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

import static com.blidus.catholicbusinessforum.ui.ChatActivity.MAX_IMAGE_DIMENSION;

/**
 * Created by Bimal on 12-12-2016.
 */

public class MessageAdapter extends RecyclerView.Adapter<MessageHolder> {

    private DisplayMetrics displayMetrics;
    private float screenWidthInDp;
    private float screenHeightInDp;

    Context ctx;
    ArrayList<Message> messages;
    private AsyncHttpClient http;
    MediaController mediaController;

    private GradientDrawable MessageType_Background;

    private int colorPrimary, colorAccent, colorBackgroundImage, colorMyMessage;


    private File makeMessageFile(int msgType, String filename) {
        File msgFile = null;
        switch (msgType) {
            case 2:
                Constants.FILES_APP.FOLDER_MESSAGE_IMAGE.mkdirs();
                msgFile = new File(Constants.FILES_APP.FOLDER_MESSAGE_IMAGE, filename);
                break;
            case 3:
                Constants.FILES_APP.FOLDER_MESSAGE_VIDEO.mkdirs();
                msgFile = new File(Constants.FILES_APP.FOLDER_MESSAGE_VIDEO, filename);
                break;
            case 4:
                Constants.FILES_APP.FOLDER_MESSAGE_AUDIO.mkdirs();
                msgFile = new File(Constants.FILES_APP.FOLDER_MESSAGE_AUDIO, filename);
                break;
            case 5:
                Constants.FILES_APP.FOLDER_MESSAGE_DOCUMENT.mkdirs();
                msgFile = new File(Constants.FILES_APP.FOLDER_MESSAGE_DOCUMENT, filename);
                break;
        }
        return msgFile;
    }

    private void fetchMessageFile(String url, final int msgType, final String filename, final int position) {
        final File msgFile = makeMessageFile(msgType, filename);
        http.get(url, new FileAsyncHttpResponseHandler(msgFile) {
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                notifyItemChanged(position);
                Toast.makeText(ctx, "Sorry..!! Unable to download the file or the uploaded file may be corrupted..!!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                notifyItemChanged(position);
                Constants.FILES_APP.FOLDER_MESSAGE_VIDEO_THUMBNAILS.mkdirs();
                if (msgType == 3)
                try {
                    String fileExtension = MimeTypeMap.getFileExtensionFromUrl(filename);
                    Log.d("Thumbnail Extension", fileExtension);
                    String thumbName = filename.replace(fileExtension,"jpg");
                    Log.d("Thumbnail Name", thumbName);
                    File msgThumb = new File(Constants.FILES_APP.FOLDER_MESSAGE_VIDEO_THUMBNAILS, thumbName);
                    Bitmap thumbnail = ThumbnailUtils.createVideoThumbnail(msgFile.getAbsolutePath(), MediaStore.Images.Thumbnails.MINI_KIND);
                    Bitmap scaledBm = resizeBitmap(thumbnail);
                    FileOutputStream fos = new FileOutputStream(msgThumb.getAbsolutePath());
                    scaledBm.compress(Bitmap.CompressFormat.JPEG, 90, fos);
                    fos.close();
                } catch (Exception e) {

                }
            }
        });
    }

    public MessageAdapter(Context ctx, ArrayList<Message> messages) {
        if (App.preferences.getBoolean(PreferenceKeys.USER_DEFINED_THEME_COLOR, false)) {
            colorPrimary = Color.parseColor(App.preferences.getString(PreferenceKeys.USER_DEFINED_THEME_COLOR_PRIMARY, null));
            colorAccent = Color.parseColor(App.preferences.getString(PreferenceKeys.USER_DEFINED_THEME_COLOR_ACCENT, null));
            colorBackgroundImage = Color.parseColor(App.preferences.getString(PreferenceKeys.USER_DEFINED_THEME_COLOR_BACKGROUND_IMAGE, null));
            colorMyMessage = Color.parseColor(App.preferences.getString(PreferenceKeys.USER_DEFINED_THEME_COLOR_MY_MESSAGE, null));
        } else {
            colorPrimary = ContextCompat.getColor(ctx, R.color.colorPrimary);
            colorAccent = ContextCompat.getColor(ctx, R.color.colorAccent);
            colorBackgroundImage = ContextCompat.getColor(ctx, R.color.colorBackgroundImage);
            colorMyMessage = ContextCompat.getColor(ctx, R.color.colorMyMsg);
        }
        this.displayMetrics = ctx.getResources().getDisplayMetrics();
        screenWidthInDp = displayMetrics.widthPixels / displayMetrics.density;
        screenHeightInDp = displayMetrics.heightPixels / displayMetrics.density;
        this.ctx = ctx;
        this.messages = messages;
        this.http = new AsyncHttpClient();
    }

    private void rotate(MessageHolder holder, float degree1, float degree2) {
        final RotateAnimation rotateAnim = new RotateAnimation(degree1, degree2,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f);

        rotateAnim.setDuration(125);
        rotateAnim.setRepeatCount(Animation.INFINITE);
        rotateAnim.setFillAfter(true);
        holder.DownloadStatus.startAnimation(rotateAnim);
    }

    @Override
    public MessageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.message_layout, parent, false);
        MessageHolder othersholder = new MessageHolder(view);
        return othersholder;
    }

    @Override
    public void onBindViewHolder(final MessageHolder holder, final int position) {
        mediaController = new MediaController(ctx);
        final Message message = messages.get(position);
        holder.UserFullName.setText(message.getUsername());
        holder.UserCompanyName.setText(message.getCompany_Name());
        final int msgType = message.getMessage_Type();
        File msgFile = null;
        boolean doesFileExist = false;
        final String passingfilename = message.getMessage_File();
        String tempFileUrl = null;
        switch (msgType) {
            case 1:
                holder.FileContent.setVisibility(View.GONE);
                holder.MessageType.setVisibility(View.GONE);
                holder.MessageVideo.setVisibility(View.GONE);
                holder.PlayButton.setVisibility(View.GONE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    holder.MessageType.setBackground(ctx.getResources().getDrawable(R.drawable.button_style));
                    MessageType_Background = (GradientDrawable) holder.MessageType.getBackground();
                    MessageType_Background.setColor(colorPrimary);
                } else {
                    holder.MessageType.setBackgroundDrawable(ctx.getResources().getDrawable(R.drawable.button_style));
                    MessageType_Background = (GradientDrawable) holder.MessageType.getBackground();
                    MessageType_Background.setColor(colorPrimary);
                }
                break;
            case 2:
                tempFileUrl = Constants.HTTP.BASE_URL + Constants.FOLDERS_ONLINE.FOLDER_MESSAGE_IMAGE + passingfilename;
                if (App.preferences.getBoolean(PreferenceKeys.PERMISSION_STR, false)) {
                    msgFile = new File(Constants.FILES_APP.FOLDER_MESSAGE_IMAGE, message.getMessage_File());
                    if (msgFile.exists()) {
                        doesFileExist = true;
                    }
                }
                if (doesFileExist) {
                    holder.DownloadStatus.setImageResource(R.drawable.done);
                    FileInputStream fis = null;
                    //FileOutputStream fos = null;
                    try {
                        fis = new FileInputStream(msgFile);
                        Bitmap bm = BitmapFactory.decodeStream(fis);
                        Bitmap scaledBm = resizeBitmap(bm);
                        //fos = new FileOutputStream(msgFile.getAbsolutePath());
                        //scaledBm.compress(Bitmap.CompressFormat.JPEG, 90, fos);
                        //fos.close();
                        holder.MessageType.setImageBitmap(scaledBm);
                        holder.MessageType.setBackgroundColor(ctx.getResources().getColor(R.color.colorTransperent));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    //holder.MessageType.setLayoutParams(frameSetMargins(ctx, 5, 5, 5, 5));
                } else {
                    if (App.preferences.getBoolean(PreferenceKeys.PERMISSION_STR, false) && App.preferences.getBoolean(PreferenceKeys.MEDIA_AUTO_DOWNLOAD_IMAGE, false)) {
                        rotate(holder, 0, 360);
                        fetchMessageFile(tempFileUrl, msgType, passingfilename, position);
                    }
                    holder.DownloadStatus.setImageResource(R.drawable.download);
                    holder.MessageType.setImageResource(R.drawable.image);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        holder.MessageType.setBackground(ctx.getResources().getDrawable(R.drawable.button_style));
                        MessageType_Background = (GradientDrawable) holder.MessageType.getBackground();
                        MessageType_Background.setColor(colorPrimary);
                    } else {
                        holder.MessageType.setBackgroundDrawable(ctx.getResources().getDrawable(R.drawable.button_style));
                        MessageType_Background = (GradientDrawable) holder.MessageType.getBackground();
                        MessageType_Background.setColor(colorPrimary);
                    }
                }
                holder.MessageType.setVisibility(View.VISIBLE);
                holder.MessageVideo.setVisibility(View.GONE);
                holder.PlayButton.setVisibility(View.GONE);
                holder.FileContent.setVisibility(View.VISIBLE);
                break;
            case 3:
                boolean correptedVideoFile = false;
                tempFileUrl = Constants.HTTP.BASE_URL + Constants.FOLDERS_ONLINE.FOLDER_MESSAGE_VIDEO + passingfilename;
                if (App.preferences.getBoolean(PreferenceKeys.PERMISSION_STR, false)) {
                    msgFile = new File(Constants.FILES_APP.FOLDER_MESSAGE_VIDEO, message.getMessage_File());
                    if (msgFile.exists()) {
                        float msgFileSize = (Float.parseFloat(String.valueOf(msgFile.length() / Constants.FILE_SIZES.ONE_KB)));
                        if (msgFileSize < 5) {
                            correptedVideoFile = true;
                            //msgFile.delete();
                        }
                        doesFileExist = true;
                    }
                }
                if (doesFileExist) {
                    if (correptedVideoFile) {
                        holder.DownloadStatus.setImageResource(R.drawable.close);
                        holder.MessageVideo.setVisibility(View.GONE);
                        holder.PlayButton.setVisibility(View.GONE);
                        holder.MessageType.setImageResource(R.drawable.video);
                        holder.MessageType.setVisibility(View.VISIBLE);
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            holder.DownloadStatus.setImageResource(R.drawable.done);
                            String msgFilePath = msgFile.getAbsolutePath();
                            /*holder.MessageVideo.setVideoPath(msgFilePath);
                            mediaController.setAnchorView(holder.MessageVideo);
                            mediaController.setMediaPlayer(holder.MessageVideo);
                            holder.MessageVideo.setMediaController(mediaController);
                            holder.MessageVideo.setVisibility(View.VISIBLE);
                            holder.MessageType.setVisibility(View.GONE);*/
                            holder.MessageVideo.setVisibility(View.GONE);
                            holder.MessageType.setVisibility(View.VISIBLE);
                            holder.PlayButton.setVisibility(View.VISIBLE);
                            FileInputStream fis = null;
                            try {
                                /*Bitmap thumbnail = ThumbnailUtils.createVideoThumbnail(msgFilePath, MediaStore.Images.Thumbnails.MINI_KIND);
                                Bitmap scaledBm = resizeBitmap(thumbnail);*/
                                String fileExtension = MimeTypeMap.getFileExtensionFromUrl(passingfilename);
                                String thumbName = passingfilename.replace(fileExtension,"jpg");
                                File msgThumb = new File(Constants.FILES_APP.FOLDER_MESSAGE_VIDEO_THUMBNAILS, thumbName);
                                fis = new FileInputStream(msgThumb);
                                Bitmap bm = BitmapFactory.decodeStream(fis);
                                Bitmap scaledBm = resizeBitmap(bm);
                                holder.MessageType.setImageBitmap(scaledBm);
                            } catch (Exception e) {

                            }
                            //holder.MessageType.setImageResource(R.drawable.video);
                            //holder.MessageVideo.start();
                        } else {
                            holder.DownloadStatus.setImageResource(R.drawable.done);
                            holder.MessageVideo.setVisibility(View.GONE);
                            holder.PlayButton.setVisibility(View.GONE);
                            holder.MessageType.setImageResource(R.drawable.video);
                            holder.MessageType.setVisibility(View.VISIBLE);
                        }
                    }
                } else {
                    if (App.preferences.getBoolean(PreferenceKeys.PERMISSION_STR, false) && App.preferences.getBoolean(PreferenceKeys.MEDIA_AUTO_DOWNLOAD_VIDEO, false)) {
                        rotate(holder, 0, 360);
                        fetchMessageFile(tempFileUrl, msgType, passingfilename, position);
                    }
                    holder.DownloadStatus.setImageResource(R.drawable.download);
                    holder.MessageVideo.setVisibility(View.GONE);
                    holder.PlayButton.setVisibility(View.GONE);
                    holder.MessageType.setImageResource(R.drawable.video);
                    holder.MessageType.setVisibility(View.VISIBLE);
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    holder.MessageType.setBackground(ctx.getResources().getDrawable(R.drawable.button_style));
                    MessageType_Background = (GradientDrawable) holder.MessageType.getBackground();
                    MessageType_Background.setColor(colorPrimary);
                } else {
                    holder.MessageType.setBackgroundDrawable(ctx.getResources().getDrawable(R.drawable.button_style));
                    MessageType_Background = (GradientDrawable) holder.MessageType.getBackground();
                    MessageType_Background.setColor(colorPrimary);
                }
                holder.FileContent.setVisibility(View.VISIBLE);
                break;
            case 4:
                boolean correptedAudioFile = false;
                tempFileUrl = Constants.HTTP.BASE_URL + Constants.FOLDERS_ONLINE.FOLDER_MESSAGE_AUDIO + passingfilename;
                if (App.preferences.getBoolean(PreferenceKeys.PERMISSION_STR, false)) {
                    msgFile = new File(Constants.FILES_APP.FOLDER_MESSAGE_AUDIO, message.getMessage_File());
                    if (msgFile.exists()) {
                        float msgFileSize = (Float.parseFloat(String.valueOf(msgFile.length() / Constants.FILE_SIZES.ONE_KB)));
                        if (msgFileSize < 2) {
                            correptedAudioFile = true;
                            //msgFile.delete();
                        }
                        doesFileExist = true;
                    }
                }
                if (doesFileExist) {
                    if (correptedAudioFile) {
                        holder.DownloadStatus.setImageResource(R.drawable.close);
                        holder.PlayButton.setVisibility(View.GONE);
                    } else {
                        holder.DownloadStatus.setImageResource(R.drawable.done);
                        holder.PlayButton.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (App.preferences.getBoolean(PreferenceKeys.PERMISSION_STR, false) && App.preferences.getBoolean(PreferenceKeys.MEDIA_AUTO_DOWNLOAD_AUDIO, false)) {
                        rotate(holder, 0, 360);
                        fetchMessageFile(tempFileUrl, msgType, passingfilename, position);
                    }
                    holder.DownloadStatus.setImageResource(R.drawable.download);
                    holder.PlayButton.setVisibility(View.GONE);
                }
                holder.MessageType.setImageResource(R.drawable.audio);
                holder.MessageType.setVisibility(View.VISIBLE);
                holder.MessageVideo.setVisibility(View.GONE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    holder.MessageType.setBackground(ctx.getResources().getDrawable(R.drawable.button_style));
                    MessageType_Background = (GradientDrawable) holder.MessageType.getBackground();
                    MessageType_Background.setColor(colorPrimary);
                } else {
                    holder.MessageType.setBackgroundDrawable(ctx.getResources().getDrawable(R.drawable.button_style));
                    MessageType_Background = (GradientDrawable) holder.MessageType.getBackground();
                    MessageType_Background.setColor(colorPrimary);
                }
                holder.FileContent.setVisibility(View.VISIBLE);
                break;
            case 5:
                tempFileUrl = Constants.HTTP.BASE_URL + Constants.FOLDERS_ONLINE.FOLDER_MESSAGE_DOCUMENT + passingfilename;
                if (App.preferences.getBoolean(PreferenceKeys.PERMISSION_STR, false)) {
                    msgFile = new File(Constants.FILES_APP.FOLDER_MESSAGE_DOCUMENT, message.getMessage_File());
                    if (msgFile.exists()) {
                        doesFileExist = true;
                    }
                }
                if (doesFileExist) {
                    holder.DownloadStatus.setImageResource(R.drawable.done);
                } else {
                    if (App.preferences.getBoolean(PreferenceKeys.PERMISSION_STR, false) && App.preferences.getBoolean(PreferenceKeys.MEDIA_AUTO_DOWNLOAD_FILE, false)) {
                        rotate(holder, 0, 360);
                        fetchMessageFile(tempFileUrl, msgType, passingfilename, position);
                    }
                    holder.DownloadStatus.setImageResource(R.drawable.download);
                }
                holder.MessageType.setImageResource(R.drawable.attach);
                holder.MessageType.setVisibility(View.VISIBLE);
                holder.MessageVideo.setVisibility(View.GONE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    holder.MessageType.setBackground(ctx.getResources().getDrawable(R.drawable.button_style));
                    MessageType_Background = (GradientDrawable) holder.MessageType.getBackground();
                    MessageType_Background.setColor(colorPrimary);
                } else {
                    holder.MessageType.setBackgroundDrawable(ctx.getResources().getDrawable(R.drawable.button_style));
                    MessageType_Background = (GradientDrawable) holder.MessageType.getBackground();
                    MessageType_Background.setColor(colorPrimary);
                }
                holder.FileContent.setVisibility(View.VISIBLE);
                break;
        }
        holder.MessageText.setText(StringEscapeUtils.unescapeJava(message.getMessage_Text()));
        changeDrawableColor(holder.DownloadStatus, colorPrimary);
        String msg_userid = message.getUser_Id();
        int login_userid = App.preferences.getInt(PreferenceKeys.LOGIN_ID, 0);
        boolean me = (Integer.parseInt(msg_userid) == login_userid) ? true : false;
        setMessageTemplate(holder, me);
        final boolean finalDoesFileExist = doesFileExist;
        final File finalMsgFile = msgFile;
        final String passingFileUrl = tempFileUrl;
        if (msgType != 1) {
            holder.MsgCardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!(App.preferences.getBoolean(PreferenceKeys.PERMISSION_STR, false))) {
                        Toast.makeText(ctx, "STORAGE PERMISSIONS NOT GRANTED..!!", Toast.LENGTH_SHORT).show();
                    } else {
                        //RotateAnimation anim = new RotateAnimation(0f, 350f, 15f, 15f);
                        float DSH = holder.DownloadStatus.getHeight();
                        float DSW = holder.DownloadStatus.getWidth();
                        RotateAnimation anim = new RotateAnimation(0f, 360f, DSH / 2, DSW / 2);
                        anim.setInterpolator(new LinearInterpolator());
                        anim.setRepeatCount(Animation.INFINITE);
                        anim.setDuration(150);

                        Uri fileUri = FileProvider.getUriForFile(ctx, ctx.getApplicationContext().getPackageName() + ".provider", finalMsgFile);
                        String fileName = fileUri.getLastPathSegment();
                        String fileExtension = MimeTypeMap.getFileExtensionFromUrl(fileName);
                        String fileType = "";
                        if (fileExtension != null) {
                            fileType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension);
                        }
                        switch (msgType) {
                            case 2:
                                if (finalDoesFileExist) {
                                    Log.d("File URI", fileUri.toString());
                                    try {
                                        ctx.startActivity(new Intent().setAction(Intent.ACTION_VIEW).setDataAndType(fileUri, fileType).addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION));
                                    } catch (Exception e) {
                                        Log.e("Exeption", e.getMessage());
                                    }
                                } else {
                                    holder.DownloadStatus.startAnimation(anim);
                                    fetchMessageFile(passingFileUrl, msgType, passingfilename, position);
                                }
                                break;
                            case 3:
                                if (finalDoesFileExist) {
                                    /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                        Log.d("File URI", fileUri.toString());
                                    } else {*/
                                    float msgFileSize = (Float.parseFloat(String.valueOf(finalMsgFile.length() / Constants.FILE_SIZES.ONE_KB)));
                                    if (msgFileSize > 5) {
                                        Log.d("File URI", fileUri.toString());
                                        try {
                                            ctx.startActivity(new Intent().setAction(Intent.ACTION_VIEW).setDataAndType(fileUri, fileType).addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION));
                                        } catch (Exception e) {
                                            Log.e("Exeption", e.getMessage());
                                        }
                                    }
                                    //}
                                } else {
                                    holder.DownloadStatus.startAnimation(anim);
                                    fetchMessageFile(passingFileUrl, msgType, passingfilename, position);
                                }
                                break;
                            case 4:
                                if (finalDoesFileExist) {
                                    float msgFileSize = (Float.parseFloat(String.valueOf(finalMsgFile.length() / Constants.FILE_SIZES.ONE_KB)));
                                    if (msgFileSize > 2) {
                                        Log.d("File URI", fileUri.toString());
                                        try {
                                            ctx.startActivity(new Intent().setAction(Intent.ACTION_VIEW).setDataAndType(fileUri, fileType).addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION));
                                        } catch (Exception e) {
                                            Log.e("Exeption", e.getMessage());
                                        }
                                    }
                                } else {
                                    holder.DownloadStatus.startAnimation(anim);
                                    fetchMessageFile(passingFileUrl, msgType, passingfilename, position);
                                }
                                break;
                            case 5:
                                if (finalDoesFileExist) {
                                    Log.d("File URI", fileUri.toString());
                                    try {
                                        ctx.startActivity(new Intent().setAction(Intent.ACTION_VIEW).setDataAndType(fileUri, fileType).addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION));
                                    } catch (Exception e) {
                                        Log.e("Exeption", e.getMessage());
                                        Toast.makeText(ctx, "No suitable application found..!!", Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                    holder.DownloadStatus.startAnimation(anim);
                                    fetchMessageFile(passingFileUrl, msgType, passingfilename, position);
                                }
                                break;
                        }
                    }
                }
            });
        }
        final boolean finalDoesFileExist1 = doesFileExist;
        holder.MsgCardView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                final long msgId = message.getMsgid();
                Log.d("Message ID", msgId+"");
                final AlertDialog.Builder alertBuilderMsgDel = new AlertDialog.Builder(ctx);
                final AlertDialog alertDialogMsgDel;
                alertBuilderMsgDel.setTitle("Delete Message !!");
                alertBuilderMsgDel.setMessage("Do you want to delete this message?");
                alertBuilderMsgDel.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                alertBuilderMsgDel.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (msgType == 1) {
                            messages.remove(position);
                            notifyItemRemoved(position);
                            notifyDataSetChanged();
                            deleteMessage(msgId, false);
                        } else {
                            if (finalDoesFileExist1) {
                                final AlertDialog.Builder alertBuilderMsgDelMedia = new AlertDialog.Builder(ctx);
                                final AlertDialog alertDialogMsgDelMedia;
                                alertBuilderMsgDelMedia.setTitle("Delete Media File !!");
                                alertBuilderMsgDelMedia.setMessage("Do you want to delete the media file?");
                                alertBuilderMsgDelMedia.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        messages.remove(position);
                                        notifyItemRemoved(position);
                                        notifyDataSetChanged();
                                        deleteMessage(msgId, false);
                                    }
                                });
                                alertBuilderMsgDelMedia.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        messages.remove(position);
                                        notifyItemRemoved(position);
                                        notifyDataSetChanged();
                                        deleteMessage(msgId, true);
                                    }
                                });
                                alertDialogMsgDelMedia = alertBuilderMsgDelMedia.create();
                                alertDialogMsgDelMedia.show();
                            } else {
                                messages.remove(position);
                                notifyItemRemoved(position);
                                notifyDataSetChanged();
                                deleteMessage(msgId, false);
                            }
                        }
                    }
                });
                alertDialogMsgDel = alertBuilderMsgDel.create();
                alertDialogMsgDel.show();
                return false;
            }
        });
    }

    public void deleteMessage(long msgId, boolean fileDelete){
        MessageDbAdapter dbAdapter = new MessageDbAdapter(ctx);
        dbAdapter.open();
        dbAdapter.delMessage(msgId, fileDelete);
        dbAdapter.close();
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    private void setMessageTemplate(MessageHolder holder, boolean me) {
        LinearLayout.LayoutParams myparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, AppBarLayout.LayoutParams.WRAP_CONTENT);
        LinearLayout.LayoutParams othparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, AppBarLayout.LayoutParams.WRAP_CONTENT);
        //LinearLayout.LayoutParams myparams = linearSetMargins(ctx, 50, 0, 0, 0);
        //LinearLayout.LayoutParams othparams = linearSetMargins(ctx, 0, 0, 50, 0);
        //FrameLayout.LayoutParams onldotparams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, AppBarLayout.LayoutParams.WRAP_CONTENT);
        float lmar = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 50, ctx.getResources().getDisplayMetrics());
        float smar = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 3, ctx.getResources().getDisplayMetrics());
        //float dmar = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 6,ctx.getResources().getDisplayMetrics());
        myparams.setMargins((int) lmar, (int) smar, (int) smar, (int) smar);
        othparams.setMargins((int) smar, (int) smar, (int) lmar, (int) smar);
        //onldotparams.setMargins(0, 0, (int) dmar, (int) dmar);
        //holder.MyOnlineDot.setLayoutParams(onldotparams);
        if (me) {
            //holder.MsgCardView.setCardBackgroundColor(Color.parseColor("#AA4B6C8F"));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                holder.MyOnlineDot.setBackground(ctx.getResources().getDrawable(R.drawable.my_online_dot));
            } else {
                holder.MyOnlineDot.setBackgroundDrawable(ctx.getResources().getDrawable(R.drawable.my_online_dot));
            }
            //holder.MsgCardView.setCardBackgroundColor(ctx.getResources().getColor(R.color.colorMyMsg));
            holder.MsgCardView.setCardBackgroundColor(colorMyMessage);
            holder.MsgCardView.setLayoutParams(myparams);
        } else {
            holder.MyOnlineDot.setBackgroundColor(ctx.getResources().getColor(R.color.colorTransperent));
            holder.MsgCardView.setCardBackgroundColor(ctx.getResources().getColor(R.color.cardview_light_background));
            holder.MsgCardView.setLayoutParams(othparams);
        }
    }

    private Bitmap resizeBitmap(Bitmap bm) {
        int width = 0;
        int height = 0;
        int targetWidth = 0;
        int targetHeight = 0;

        width = bm.getWidth();
        height = bm.getHeight();
        //Log.d(TAG, "resizeBitmap w: " + width + " h: " + height);
        float scaleFactor;
        if (width > height) {
            scaleFactor = ((float) MAX_IMAGE_DIMENSION) / ((float) width);
            targetWidth = MAX_IMAGE_DIMENSION;
            targetHeight = (int) (height * scaleFactor);
        } else {
            scaleFactor = ((float) MAX_IMAGE_DIMENSION) / ((float) height);
            targetHeight = MAX_IMAGE_DIMENSION;
            targetWidth = (int) (width * scaleFactor);
        }

        //Log.d(TAG, "resizeBitmap scaled to w: " + targetWidth + " h: " + targetHeight);
        return Bitmap.createScaledBitmap(bm, targetWidth, targetHeight, true);
    }

    private void changeDrawableColor(ImageView imageView, int color) {
        Drawable d = imageView.getDrawable();
        d.mutate();
        d.setColorFilter(color, PorterDuff.Mode.MULTIPLY);
        imageView.setImageDrawable(d);
    }
}
