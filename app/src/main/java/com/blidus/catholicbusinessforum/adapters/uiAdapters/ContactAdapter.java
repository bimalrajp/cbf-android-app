package com.blidus.catholicbusinessforum.adapters.uiAdapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import com.google.android.material.appbar.AppBarLayout;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.blidus.catholicbusinessforum.App;
import com.blidus.catholicbusinessforum.R;
import com.blidus.catholicbusinessforum.adapters.dbAdapters.ContactDBAdapter;
import com.blidus.catholicbusinessforum.apiCallbacks.Approval;
import com.blidus.catholicbusinessforum.dataModels.dbDatatypes.Contact;
import com.blidus.catholicbusinessforum.global.Constants;
import com.blidus.catholicbusinessforum.global.PreferenceKeys;
import com.blidus.catholicbusinessforum.holders.ContactHolder;
import com.blidus.catholicbusinessforum.ui.ContactDetailActivity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.blidus.catholicbusinessforum.ui.ChatActivity.MAX_IMAGE_DIMENSION;
import static com.blidus.catholicbusinessforum.ui.ContactsActivity.EXTRA_CONTACT_ID;

/**
 * Created by Bimal on 16-02-2017.
 */

public class ContactAdapter extends RecyclerView.Adapter<ContactHolder> {

    Context ctx;
    ArrayList<Contact> contacts;

    private int colorPrimary, colorAccent, colorBackgroundImage;
    private GradientDrawable ll_user_flags_background;


    public ContactAdapter(Context ctx, ArrayList<Contact> contacts) {
        this.ctx = ctx;
        this.contacts = contacts;
        if (App.preferences.getBoolean(PreferenceKeys.USER_DEFINED_THEME_COLOR, false)) {
            this.colorPrimary = Color.parseColor(App.preferences.getString(PreferenceKeys.USER_DEFINED_THEME_COLOR_PRIMARY, null));
            this.colorAccent = Color.parseColor(App.preferences.getString(PreferenceKeys.USER_DEFINED_THEME_COLOR_ACCENT, null));
            this.colorBackgroundImage = Color.parseColor(App.preferences.getString(PreferenceKeys.USER_DEFINED_THEME_COLOR_BACKGROUND_IMAGE, null));
        } else {
            this.colorPrimary = ContextCompat.getColor(ctx, R.color.colorPrimary);
            this.colorAccent = ContextCompat.getColor(ctx, R.color.colorAccent);
            this.colorBackgroundImage = ContextCompat.getColor(ctx, R.color.colorBackgroundImage);
        }
    }

    @Override
    public ContactHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_layout, parent, false);
        ContactHolder holder = new ContactHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder( ContactHolder holder, final int position) {
        final Contact contact = contacts.get(position);

        final String cnt_id = contact.getPKUserID();
        int login_userid = App.preferences.getInt(PreferenceKeys.LOGIN_ID, 0);
        boolean me = (Integer.parseInt(cnt_id) == login_userid) ? true : false;
        if (!me) {
            if (App.preferences.getBoolean(PreferenceKeys.PERMISSION_STR, false)) {
                File contactImage = new File(Constants.FILES_APP.FOLDER_USER_IMAGE, contact.getUserImage());
                FileInputStream fis = null;
                FileOutputStream fos = null;
                try {
                    fis = new FileInputStream(contactImage);
                    Bitmap bm = BitmapFactory.decodeStream(fis);
                    fis.close();
                    Bitmap scaledBm = resizeBitmap(bm);
                    fos = new FileOutputStream(contactImage.getAbsolutePath());
                    scaledBm.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                    fos.close();
                    holder.ContactImage.setImageBitmap(scaledBm);
                    //holder.ContactImage.setImageBitmap(bm);
                } catch (Exception e) {
                    holder.ContactImage.setImageResource(R.drawable.profile);
                    changeDrawableColor(holder.ContactImage, colorPrimary);
                }
            }
            holder.UserFullName.setText(contact.getUserFullName());
            holder.UserCompanyName.setText(contact.getCompanyName());
            if (App.preferences.getInt(PreferenceKeys.LOGIN_TYPE, 0) == 0){
                holder.UserFlags.setVisibility(View.GONE);
            } else if (App.preferences.getInt(PreferenceKeys.LOGIN_TYPE, 0) == 1){
                if (App.preferences.getBoolean(PreferenceKeys.ADMIN_CONTACTS_EDIT_MODE, false)) {
                    holder.UserFlags.setVisibility(View.VISIBLE);
                } else {
                    holder.UserFlags.setVisibility(View.GONE);
                }
            }
            /*if (Integer.parseInt(contact.getUserType()) == 0){
                holder.UserSection.setBackgroundColor(ContextCompat.getColor(ctx, R.color.colorWhite));
            } else {
                holder.UserSection.setBackgroundColor(colorAccent);
            }*/
            ll_user_flags_background = (GradientDrawable) holder.UserFlags.getBackground();
            ll_user_flags_background.setColor(ContextCompat.getColor(ctx, R.color.colorWhite));
            ll_user_flags_background.setStroke(3, colorPrimary);

            holder.ProfileSwitch.setChecked(Integer.parseInt(contact.getUserFlag()) == 1);
            holder.MessagingSwitch.setChecked(Integer.parseInt(contact.getUserMessageFlag()) == 1);
            holder.ContactDescreption.setText(contact.getUserDescription());
            setContactTemplate(holder, me);
            holder.ContactCardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //App.preferences.edit().putInt(PreferenceKeys.CURRENT_CNT_POS, position).apply();
                    ctx.startActivity(new Intent(ctx, ContactDetailActivity.class).putExtra(EXTRA_CONTACT_ID, cnt_id));
                    notifyItemChanged(position);
                    //Toast.makeText(ctx, contact.getItemPKUserID(),Toast.LENGTH_SHORT).show();
                }
            });
            holder.ContactCardView.setOnLongClickListener(new View.OnLongClickListener(){
                @Override
                public boolean onLongClick(View v) {
                    final String cntId = contact.getPKUserID();
                    final AlertDialog.Builder alertBuilderCntDel = new AlertDialog.Builder(ctx);
                    final AlertDialog alertDialogCntDel;
                    alertBuilderCntDel.setTitle("Delete Contact !!");
                    alertBuilderCntDel.setMessage("Do you want to delete this contact?");
                    alertBuilderCntDel.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    alertBuilderCntDel.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            contacts.remove(position);
                            notifyItemRemoved(position);
                            notifyDataSetChanged();
                            deleteContact(cntId);
                        }
                    });
                    alertDialogCntDel = alertBuilderCntDel.create();
                    alertDialogCntDel.show();
                    return false;
                }
            });
            holder.ProfileSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (App.preferences.getInt(PreferenceKeys.LOGIN_TYPE, 0) == 1) {
                        int userProfileFlag = 0;
                        if (buttonView.isChecked()) {
                            userProfileFlag = 1;
                        } else {
                            userProfileFlag = 0;
                        }
                        final int finalUserProfileFlag = userProfileFlag;
                        App.api.profileApproval(App.preferences.getString(PreferenceKeys.TOKEN, null), contact.getPKUserID(), userProfileFlag, new Callback<Approval>() {
                            @Override
                            public void success(Approval approval, Response response) {
                                Log.d("Approval Flag", approval.ApprovalFlag + "");
                                if (approval.ApprovalFlag == 1) {
                                    ContactDBAdapter dbAdapter = new ContactDBAdapter(ctx);
                                    dbAdapter.open();
                                    dbAdapter.updateContactProfileFlag(Integer.parseInt(contact.getPKUserID()), finalUserProfileFlag);
                                    dbAdapter.close();
                                    //notifyItemChanged(position);
                                }
                            }

                            @Override
                            public void failure(RetrofitError error) {

                            }
                        });
                    }
                }
            });
            holder.MessagingSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (App.preferences.getInt(PreferenceKeys.LOGIN_TYPE, 0) == 1) {
                        int userMessageFlag = 0;
                        if (buttonView.isChecked()) {
                            userMessageFlag = 1;
                        } else {
                            userMessageFlag = 0;
                        }
                        final int finalUserMessageFlag = userMessageFlag;
                        App.api.messagingApproval(App.preferences.getString(PreferenceKeys.TOKEN, null), contact.getPKUserID(), finalUserMessageFlag, new Callback<Approval>() {
                            @Override
                            public void success(Approval approval, Response response) {
                                if (approval.ApprovalFlag == 1) {
                                    ContactDBAdapter dbAdapter = new ContactDBAdapter(ctx);
                                    dbAdapter.open();
                                    dbAdapter.updateContactMessagingFlag(Integer.parseInt(contact.getPKUserID()), finalUserMessageFlag);
                                    dbAdapter.close();
                                    //notifyItemChanged(position);
                                }
                            }

                            @Override
                            public void failure(RetrofitError error) {

                            }
                        });
                    }
                }
            });
            holder.ContactCardView.setVisibility(View.VISIBLE);
        } else {
            holder.ContactCardView.setVisibility(View.GONE);
        }
    }


    public void deleteContact(final String cntId){
        if (App.preferences.getInt(PreferenceKeys.LOGIN_TYPE, 0) == 1) {
            final int userProfileFlag = 2;
            App.api.profileDelete(App.preferences.getString(PreferenceKeys.TOKEN, null), cntId, userProfileFlag, new Callback<Approval>() {
                @Override
                public void success(Approval approval, Response response) {
                    Log.d("Approval Flag", approval.ApprovalFlag + "");
                    if (approval.ApprovalFlag == 1) {
                        ContactDBAdapter dbAdapter = new ContactDBAdapter(ctx);
                        dbAdapter.open();
                        dbAdapter.updateContactProfileFlag(Integer.parseInt(cntId), userProfileFlag);
                        dbAdapter.close();
                        //notifyItemChanged(position);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    Toast.makeText(ctx, "Unable to delete contact !! Please try again later !!", Toast.LENGTH_SHORT).show();
                }
            });
        }
        /*ContactDBAdapter dbAdapter = new ContactDBAdapter(ctx);
        dbAdapter.open();
        dbAdapter.delContact(msgId);
        dbAdapter.close();*/
    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }

    private void setContactTemplate(ContactHolder holder, boolean me) {
        LinearLayout.LayoutParams myparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, AppBarLayout.LayoutParams.WRAP_CONTENT);
        //FrameLayout.LayoutParams onldotparams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, AppBarLayout.LayoutParams.WRAP_CONTENT);
        float smar = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 3, ctx.getResources().getDisplayMetrics());
        //float dmar = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 6,ctx.getResources().getDisplayMetrics());
        myparams.setMargins((int) smar, (int) smar, (int) smar, (int) smar);
        //onldotparams.setMargins(0, 0, (int) dmar, (int) dmar);
        //holder.MyOnlineDot.setLayoutParams(onldotparams);
        if (me) {
            //holder.MsgCardView.setCardBackgroundColor(Color.parseColor("#AA4B6C8F"));
            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                holder.ContactOnlineDot.setBackground(ctx.getResources().getDrawable(R.drawable.my_online_dot));
            } else {
                holder.ContactOnlineDot.setBackgroundDrawable(ctx.getResources().getDrawable(R.drawable.my_online_dot));
            }
            holder.ContactCardView.setCardBackgroundColor(ctx.getResources().getColor(R.color.colorMyMsg));
            holder.ContactCardView.setLayoutParams(myparams);*/
        } else {
            holder.ContactOnlineDot.setBackgroundColor(ctx.getResources().getColor(R.color.colorTransperent));
            holder.ContactCardView.setCardBackgroundColor(ctx.getResources().getColor(R.color.cardview_light_background));
        }
        holder.ContactCardView.setLayoutParams(myparams);
    }

    private Bitmap resizeBitmap(Bitmap bm) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        //Log.d(TAG, "resizeBitmap w: " + width + " h: " + height);
        int targetWidth;
        int targetHeight;
        float scaleFactor;
        if (width > height) {
            scaleFactor = ((float) MAX_IMAGE_DIMENSION) / ((float) width);
            targetWidth = MAX_IMAGE_DIMENSION;
            targetHeight = (int) (height * scaleFactor);
        } else {
            scaleFactor = ((float) MAX_IMAGE_DIMENSION / ((float) height));
            targetHeight = MAX_IMAGE_DIMENSION;
            targetWidth = (int) (width * scaleFactor);
        }

        //Log.d(TAG, "resizeBitmap scaled to w: " + targetWidth + " h: " + targetHeight);
        return Bitmap.createScaledBitmap(bm, targetWidth, targetHeight, true);
    }

    private void changeDrawableColor(ImageView imageView, int color) {
        Drawable d = imageView.getDrawable();
        d.mutate();
        d.setColorFilter(color, PorterDuff.Mode.MULTIPLY);
        imageView.setImageDrawable(d);
    }
}
