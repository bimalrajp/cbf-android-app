package com.blidus.catholicbusinessforum.adapters.dbAdapters;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.blidus.catholicbusinessforum.App;
import com.blidus.catholicbusinessforum.controllers.dbControllers.ContactDbController;
import com.blidus.catholicbusinessforum.dataModels.callbackDatatypes.ContactItem;
import com.blidus.catholicbusinessforum.dataModels.dbDatatypes.Contact;
import com.blidus.catholicbusinessforum.global.Constants;
import com.blidus.catholicbusinessforum.global.PreferenceKeys;
import com.blidus.catholicbusinessforum.helpers.DbHelper;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Bimal on 16-02-2017.
 */

public class ContactDBAdapter {

    private static int maxId = 0;
    private static int premaxId = 0;
    private static int minId = 0;

    public enum Extremes {MAX, MIN}

    private ContactItem[] OnlineContacts;
    private Contact ClickedContact;

    private SQLiteDatabase sqlDB;
    private Context context;
    private DbHelper contactsDbHelper;

    public static final String CONTACTS_TABLE_NAME = "contacts";
    public static final String COLUMN_CONTACT_ID = "_PKUserID";
    public static final String COLUMN_CONTACT_PROFIlE_FLAG = "UserFlag";
    public static final String COLUMN_CONTACT_MESSAGE_FLAG = "UserMessageFlag";
    public static final String COLUMN_CONTACT_NAME = "UserFullName";
    public static final String COLUMN_CONTACT_GENDER = "UserGender";
    public static final String COLUMN_CONTACT_HOUSE_NAME = "UserHouseName";
    public static final String COLUMN_CONTACT_ADDRESS = "UserAddress";
    public static final String COLUMN_CONTACT_CITY = "UserCity";
    public static final String COLUMN_CONTACT_PIN = "UserPin";
    public static final String COLUMN_CONTACT_MOBILE = "UserMobile";
    public static final String COLUMN_CONTACT_EMAIL = "UserEmail";
    public static final String COLUMN_CONTACT_DESCREPTION = "UserDescription";
    public static final String COLUMN_CONTACT_IMAGE = "UserImage";
    public static final String COLUMN_CONTACT_COMPANY_NAME = "CompanyName";
    public static final String COLUMN_CONTACT_PRODUCT_NAME = "ProductName";
    public static final String COLUMN_CONTACT_PRODUCT_DESCREPTION = "ProductDescreption";

    private String[] allCntTblColumns = {
            COLUMN_CONTACT_ID,
            COLUMN_CONTACT_PROFIlE_FLAG,
            COLUMN_CONTACT_MESSAGE_FLAG,
            COLUMN_CONTACT_NAME,
            COLUMN_CONTACT_GENDER,
            COLUMN_CONTACT_HOUSE_NAME,
            COLUMN_CONTACT_ADDRESS,
            COLUMN_CONTACT_CITY,
            COLUMN_CONTACT_PIN,
            COLUMN_CONTACT_MOBILE,
            COLUMN_CONTACT_EMAIL,
            COLUMN_CONTACT_DESCREPTION,
            COLUMN_CONTACT_IMAGE,
            COLUMN_CONTACT_COMPANY_NAME,
            COLUMN_CONTACT_PRODUCT_NAME,
            COLUMN_CONTACT_PRODUCT_DESCREPTION
    };

    public static final String CREATE_CONTACT_TABLE = "create table " + CONTACTS_TABLE_NAME + " ( "
            + COLUMN_CONTACT_ID + " integer primary key, "// autoincrement
            + COLUMN_CONTACT_PROFIlE_FLAG + " integer default 0, "
            + COLUMN_CONTACT_MESSAGE_FLAG + " integer default 0, "
            + COLUMN_CONTACT_NAME + " text, "
            + COLUMN_CONTACT_GENDER + " text, "
            + COLUMN_CONTACT_HOUSE_NAME + " text, "
            + COLUMN_CONTACT_ADDRESS + " text, "
            + COLUMN_CONTACT_CITY + " text, "
            + COLUMN_CONTACT_PIN + " text, "
            + COLUMN_CONTACT_MOBILE + " text, "
            + COLUMN_CONTACT_EMAIL + " text, "
            + COLUMN_CONTACT_DESCREPTION + " text, "
            + COLUMN_CONTACT_IMAGE + " text, "
            + COLUMN_CONTACT_COMPANY_NAME + " text, "
            + COLUMN_CONTACT_PRODUCT_NAME + " text, "
            + COLUMN_CONTACT_PRODUCT_DESCREPTION + " text );";

    public ContactDBAdapter(Context ctx) {
        context = ctx;
    }

    public ArrayList<Contact> writeToDb(ContactItem[] Contacts) {

        ArrayList<Contact> newcontacts = new ArrayList<Contact>();

        premaxId = maxId = getExtremeCntIds(Extremes.MAX);
        minId = getExtremeCntIds(Extremes.MIN);
        /*Log.d("Max Contact ID", maxId + "");
        Log.d("Min Contact ID", minId + "");*/
        OnlineContacts = Contacts;
        ContactDbController vc = new ContactDbController();
        for (int i = 0; i < OnlineContacts.length; i++) {
            vc.contact = OnlineContacts[i];
            int cntid = Integer.parseInt(vc.contact.getItemPKUserID());
            if (cntid > premaxId || cntid < minId) {
                Contact newContact = createContacts(1, Integer.parseInt(vc.contact.getItemPKUserID()), Integer.parseInt(vc.contact.getUserFlag()), Integer.parseInt(vc.contact.getUserMessageFlag()), vc.contact.getUserFullName(), vc.contact.getUserGender(), vc.contact.getUserHouseName(), vc.contact.getUserAddress(), vc.contact.getUserCity(), vc.contact.getUserPin(), vc.contact.getUserMobile(), vc.contact.getUserEmail(), vc.contact.getUserDescription(), vc.contact.getUserImage(), vc.contact.getCompanyName(), vc.contact.getProductName(), vc.contact.getProductDescreption());
                Log.d("Contact", "Contact added in App DB");
                newcontacts.add(newContact);
                if (cntid > maxId) {
                    if (maxId == 0) {
                        maxId = premaxId = minId = cntid;
                    } else {
                        maxId = cntid;
                    }
                } else if (cntid < minId) {
                    minId = cntid;
                }
                /*Log.d("Max Contact ID", maxId + "");
                Log.d("Min Contact ID", minId + "");*/
                App.preferences.edit()
                        .putInt(PreferenceKeys.HIGHEST_CNT_ID, maxId)
                        .putInt(PreferenceKeys.LOWEST_CNT_ID, minId)
                        .apply();
            } else {
                createContacts(0, Integer.parseInt(vc.contact.getItemPKUserID()), Integer.parseInt(vc.contact.getUserFlag()), Integer.parseInt(vc.contact.getUserMessageFlag()), vc.contact.getUserFullName(), vc.contact.getUserGender(), vc.contact.getUserHouseName(), vc.contact.getUserAddress(), vc.contact.getUserCity(), vc.contact.getUserPin(), vc.contact.getUserMobile(), vc.contact.getUserEmail(), vc.contact.getUserDescription(), vc.contact.getUserImage(), vc.contact.getCompanyName(), vc.contact.getProductName(), vc.contact.getProductDescreption());
                Log.d("Contact", "Contact updated in App DB");
            }
        }
        return newcontacts;
    }

    public Contact createContacts(int create, int PKUserID, int UserFlag, int UserMessageFlag, String UserFullName, String UserGender, String UserHouseName, String UserAddress, String UserCity, String UserPin, String UserMobile, String UserEmail, String UserDescription, String UserImage, String CompanyName, String ProductName, String ProductDescreption) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_CONTACT_ID, PKUserID);
        values.put(COLUMN_CONTACT_PROFIlE_FLAG, UserFlag);
        values.put(COLUMN_CONTACT_MESSAGE_FLAG, UserMessageFlag);
        values.put(COLUMN_CONTACT_NAME, UserFullName);
        values.put(COLUMN_CONTACT_GENDER, UserGender);
        values.put(COLUMN_CONTACT_HOUSE_NAME, UserHouseName);
        values.put(COLUMN_CONTACT_ADDRESS, UserAddress);
        values.put(COLUMN_CONTACT_CITY, UserCity);
        values.put(COLUMN_CONTACT_PIN, UserPin);
        values.put(COLUMN_CONTACT_MOBILE, UserMobile);
        values.put(COLUMN_CONTACT_EMAIL, UserEmail);
        values.put(COLUMN_CONTACT_DESCREPTION, UserDescription);
        values.put(COLUMN_CONTACT_IMAGE, UserImage);
        values.put(COLUMN_CONTACT_COMPANY_NAME, CompanyName);
        values.put(COLUMN_CONTACT_PRODUCT_NAME, ProductName);
        values.put(COLUMN_CONTACT_PRODUCT_DESCREPTION, ProductDescreption);

        //long insertId = sqlDB.insert(TABLE_NAME, null, values);
        if (create == 1) {
            sqlDB.insert(CONTACTS_TABLE_NAME, null, values);
        } else if (create == 0) {
            Cursor cursor = null;
            cursor = sqlDB.query(CONTACTS_TABLE_NAME, allCntTblColumns, COLUMN_CONTACT_ID + " = " + PKUserID, null, null, null, null);
            cursor.moveToLast();
            Contact newContact = CursorToContact(cursor);
            cursor.close();
            File contactImage = new File(Constants.FILES_APP.FOLDER_USER_IMAGE.getAbsolutePath() + "/" + newContact.getUserImage());
            if (contactImage.exists()) {
                contactImage.delete();
            }
            sqlDB.update(CONTACTS_TABLE_NAME, values, COLUMN_CONTACT_ID + " = " + PKUserID, null);
        }

        Cursor cursor = null;
        cursor = sqlDB.query(CONTACTS_TABLE_NAME, allCntTblColumns, COLUMN_CONTACT_ID + " = " + PKUserID, null, null, null, null);
        App.preferences.edit()
                .putInt(PreferenceKeys.HIGHEST_CNT_ID, PKUserID)
                .apply();
        /*sharedPreferences.edit()
                .putInt(PreferenceKeys.HIGHEST_DB_ID, id).apply();*/
        cursor.moveToLast();
        Contact newContact = CursorToContact(cursor);
        cursor.close();
        return newContact;
    }

    public ArrayList<Contact> getAllContacts() {
        ArrayList<Contact> contacts = new ArrayList<Contact>();

        Cursor cursor = sqlDB.query(CONTACTS_TABLE_NAME, allCntTblColumns, COLUMN_CONTACT_ID + "!=" + App.preferences.getInt(PreferenceKeys.LOGIN_ID, 0), null, null, null, COLUMN_CONTACT_NAME);

        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            Contact contact = CursorToContact(cursor);
            if (Integer.parseInt(contact.getUserFlag()) == 1) {
                contacts.add(contact);
            } else {
                if (App.preferences.getInt(PreferenceKeys.LOGIN_TYPE, 0) == 1){
                    contacts.add(contact);
                }
            }
        }

        cursor.close();

        return contacts;
    }

    public Contact getContact(int contactId) {

        Cursor cursor = sqlDB.query(CONTACTS_TABLE_NAME, allCntTblColumns, COLUMN_CONTACT_ID + "=" + contactId, null, null, null, null);
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            ClickedContact = CursorToContact(cursor);
        }
        cursor.close();
        return ClickedContact;
    }

    public long getTotalContactsCount() {
        long tcc = 0;
        tcc = DatabaseUtils.queryNumEntries(sqlDB, CONTACTS_TABLE_NAME, COLUMN_CONTACT_PROFIlE_FLAG+"!=2");
        return tcc;
    }

    public ArrayList<Contact> getSearchedContacts(String name, String company, String product) {
        ArrayList<Contact> contacts = new ArrayList<Contact>();

        String PassedName = name.replace("'", "`");
        String PassedCompany = company.replace("'", "`");
        String PassedProduct = product.replace("'", "`");
        //String[] searchParameters = {"%"+Name+"%", "%"+Company+"%", "%"+Product+"%", "%"+Product+"%", "%"+Product+"%"};

        String SearchCondition = COLUMN_CONTACT_ID + " != " + App.preferences.getInt(PreferenceKeys.LOGIN_ID, 0);
        if (PassedName != null || PassedCompany != null || PassedProduct != null) {
            SearchCondition += " AND ";
        }
        if (PassedName != "") {
            SearchCondition += COLUMN_CONTACT_NAME + " LIKE '%" + PassedName + "%' ";
        }
        if (PassedName != "" && PassedCompany != "") {
            SearchCondition += " AND ";
        }
        if (PassedCompany != "") {
            SearchCondition += COLUMN_CONTACT_COMPANY_NAME + " LIKE '%" + PassedCompany + "%' ";
        }
        if ((PassedName != "" && PassedProduct != "" && PassedCompany == "") || (PassedCompany != "" && PassedProduct != "")) {
            SearchCondition += " AND ";
        }
        if (PassedProduct != "") {
            SearchCondition += "(" + COLUMN_CONTACT_PRODUCT_NAME + " LIKE '%" + PassedProduct + "%' OR " + COLUMN_CONTACT_DESCREPTION + " LIKE '%" + PassedProduct + "%' OR " + COLUMN_CONTACT_PRODUCT_DESCREPTION + " LIKE '%" + PassedProduct + "%' )";
        }
        Log.d("Searched Condition", SearchCondition);
        if (PassedName == "" && PassedCompany == "" && PassedProduct == "") {
            //SearchCondition = null;
            //SearchCondition = COLUMN_CONTACT_ID + "!='" + String.valueOf(App.preferences.getInt(PreferenceKeys.LOGIN_ID, 0)) + "'";
        }
        Cursor cursor = sqlDB.query(CONTACTS_TABLE_NAME, allCntTblColumns, SearchCondition, null, null, null, COLUMN_CONTACT_NAME);

        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            Contact contact = CursorToContact(cursor);
            contacts.add(contact);
        }

        cursor.close();

        return contacts;
    }

    public void updateContactProfileFlag(int pkUserID, int ProfileFlag) {
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_CONTACT_PROFIlE_FLAG, ProfileFlag);
        sqlDB.update(CONTACTS_TABLE_NAME, cv, COLUMN_CONTACT_ID + " = " + pkUserID, null);
    }

    public void updateContactMessagingFlag(int pkUserID, int MessagingFlag) {
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_CONTACT_MESSAGE_FLAG, MessagingFlag);
        sqlDB.update(CONTACTS_TABLE_NAME, cv, COLUMN_CONTACT_ID + " = " + pkUserID, null);
    }

    private Contact CursorToContact(Cursor cursor) {
        Contact newContact = new Contact(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getString(7), cursor.getString(8), cursor.getString(9), cursor.getString(10), cursor.getString(11), cursor.getString(12), cursor.getString(13), cursor.getString(14), cursor.getString(15));
        return newContact;
    }

    public int getExtremeCntIds(Extremes type) {
        String query = null;
        if (type == Extremes.MIN) {
            query = "SELECT MIN(" + COLUMN_CONTACT_ID + ") AS min_id FROM " + CONTACTS_TABLE_NAME;
        } else if (type == Extremes.MAX) {
            query = "SELECT MAX(" + COLUMN_CONTACT_ID + ") AS max_id FROM " + CONTACTS_TABLE_NAME;
        }
        Cursor cursor = sqlDB.rawQuery(query, null);

        int id = 0;
        if (cursor.moveToFirst()) {
            do {
                id = cursor.getInt(0);
            } while (cursor.moveToNext());
        }
        return id;
    }

    public ContactDBAdapter open() throws android.database.SQLException {
        contactsDbHelper = new DbHelper(context);
        sqlDB = contactsDbHelper.getWritableDatabase();
        return this;
    }

    public void renew(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + CONTACTS_TABLE_NAME);
        db.execSQL(CREATE_CONTACT_TABLE);
    }

    public void close() {
        contactsDbHelper.close();
    }
}
