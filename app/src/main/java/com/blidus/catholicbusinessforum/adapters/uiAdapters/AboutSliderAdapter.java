package com.blidus.catholicbusinessforum.adapters.uiAdapters;

import android.content.Context;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.blidus.catholicbusinessforum.R;

/**
 * Created by Bimal on 13-03-2017.
 */

public class AboutSliderAdapter extends PagerAdapter {

    private int[] aboutSlides= {R.drawable.slide1, R.drawable.slide2, R.drawable.slide3, R.drawable.slide4};
    Context ctx;
    private LayoutInflater layoutInflater;
    public static final int IMG_COUNT = 4;

    public AboutSliderAdapter(Context ctx){
        this.ctx = ctx;
    }

    @Override
    public int getCount() {
        return aboutSlides.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view==(LinearLayout)object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        layoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = layoutInflater.inflate(R.layout.about_slider_layout, container, false);
        ImageView aboutSliderImage = (ImageView) itemView.findViewById(R.id.img_about_slider);
        aboutSliderImage.setImageResource(aboutSlides[position]);
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
