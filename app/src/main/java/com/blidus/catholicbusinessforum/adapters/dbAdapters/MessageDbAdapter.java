package com.blidus.catholicbusinessforum.adapters.dbAdapters;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.webkit.MimeTypeMap;

import com.blidus.catholicbusinessforum.controllers.dbControllers.MessageDbController;
import com.blidus.catholicbusinessforum.App;
import com.blidus.catholicbusinessforum.dataModels.callbackDatatypes.ChatMsgItem;
import com.blidus.catholicbusinessforum.dataModels.dbDatatypes.Message;
import com.blidus.catholicbusinessforum.global.Constants;
import com.blidus.catholicbusinessforum.global.PreferenceKeys;
import com.blidus.catholicbusinessforum.helpers.DbHelper;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Bimal on 04-02-2017.
 */

public class MessageDbAdapter {

    private static int maxId = 0;
    private static int premaxId = 0;
    private static int minId = 0;

    public enum InsertTypes {OLD, NEW}

    public enum Extremes {MAX, MIN}

    public enum CountTypes {TOTAL, NEW, READ, DEL, NONDEL}

    private SQLiteDatabase sqlDB;
    private Context context;
    private DbHelper messageDbHelper;

    private ChatMsgItem[] OnlineMessages;

    public static final String MESSAGE_TABLE_NAME = "messages";
    public static final String COLUMN_MESSAGE_ID = "_PKMessageID";
    public static final String COLUMN_USER_ID = "UserId";
    public static final String COLUMN_USER_NAME = "UserFullName";
    public static final String COLUMN_USER_MOBILE = "UserMobile";
    public static final String COLUMN_USER_EMAIL = "UserEmail";
    public static final String COLUMN_USER_COMPANY = "UserCompany";
    public static final String COLUMN_MESSAGE_TEXT = "MessageText";
    public static final String COLUMN_MESSAGE_FILE = "MessageFile";
    public static final String COLUMN_MESSAGE_TYPE = "MessageType";
    public static final String COLUMN_MESSAGE_DATETIME = "MessageDateTime";
    public static final String COLUMN_MESSAGE_READ_FLAG = "MessageReadFlag";
    public static final String COLUMN_MESSAGE_DEL_FLAG = "MessageDelFlag";

    private String[] allMsgTblColumns = {
            COLUMN_MESSAGE_ID,
            COLUMN_USER_ID,
            COLUMN_USER_NAME,
            COLUMN_USER_MOBILE,
            COLUMN_USER_EMAIL,
            COLUMN_USER_COMPANY,
            COLUMN_MESSAGE_TEXT,
            COLUMN_MESSAGE_FILE,
            COLUMN_MESSAGE_TYPE,
            COLUMN_MESSAGE_DATETIME,
            COLUMN_MESSAGE_READ_FLAG,
            COLUMN_MESSAGE_DEL_FLAG
    };

    public static final String CREATE_MESSAGE_TABLE = "create table " + MESSAGE_TABLE_NAME + " ( "
            + COLUMN_MESSAGE_ID + " integer primary key, "// autoincrement
            + COLUMN_USER_ID + " text not null, "
            + COLUMN_USER_NAME + " text not null, "
            + COLUMN_USER_MOBILE + " text, "
            + COLUMN_USER_EMAIL + " text, "
            + COLUMN_USER_COMPANY + " text not null, "
            + COLUMN_MESSAGE_TEXT + " text not null, "
            + COLUMN_MESSAGE_FILE + " text, "
            + COLUMN_MESSAGE_TYPE + " integer default 1, "
            + COLUMN_MESSAGE_DATETIME + " text, "
            + COLUMN_MESSAGE_READ_FLAG + " integer default 0, "
            + COLUMN_MESSAGE_DEL_FLAG + "  integer default 0 );";

    public MessageDbAdapter(Context ctx) {
        context = ctx;
    }

    public ArrayList<Message> writeToDb(ChatMsgItem[] Messages, InsertTypes insertType) {

        ArrayList<Message> newmessages = new ArrayList<Message>();

        premaxId = maxId = getExtremeMsgIds(Extremes.MAX);
        minId = getExtremeMsgIds(Extremes.MIN);
        /*Log.d("Max Message ID", maxId + "");
        Log.d("Min Message ID", minId + "");*/
        OnlineMessages = Messages;
        MessageDbController vc = new MessageDbController();
        for (int i = 0; i < OnlineMessages.length; i++) {
            vc.message = OnlineMessages[i];
            int msgid = Integer.parseInt(vc.message.getMessageID());
            if (msgid > premaxId || msgid < minId) {
                int read_flag = 0;
                if (insertType == InsertTypes.OLD) {
                    read_flag = 1;
                }
                Message newMessage = createMessages(0, Integer.parseInt(vc.message.getMessageID()), vc.message.getUserId(), vc.message.getUserFullName(), vc.message.getUserMobile(), vc.message.getUserEmail(), vc.message.getUserCompanyName(), vc.message.getMessageText(), vc.message.getMessageFile(), Integer.parseInt(vc.message.getMessageType()), vc.message.getMsgDateTime(), read_flag, Integer.parseInt(vc.message.getMessageDelFlag()));
                newmessages.add(newMessage);
                if (msgid > maxId) {
                    if (maxId == 0) {
                        maxId = premaxId = minId = msgid;
                    } else {
                        maxId = msgid;
                    }
                } else if (msgid < minId) {
                    minId = msgid;
                }
                /*Log.d("Max Message ID", maxId + "");
                Log.d("Min Message ID", minId + "");*/
                App.preferences.edit()
                        .putInt(PreferenceKeys.HIGHEST_MSG_ID, maxId)
                        .putInt(PreferenceKeys.LOWEST_MSG_ID, minId)
                        .apply();
            /*long dbid = getMsgIdFromAppDb(msgid);*/
            } else {
                Log.d("New Message", "Message is already written in App DB");
            }
        }
        return newmessages;
        //return OnlineMessages;
    }

    private long getMsgIdFromAppDb(int pmsgid) {
        long dbid = 0;
        Cursor cursor = sqlDB.query(MESSAGE_TABLE_NAME, allMsgTblColumns, COLUMN_MESSAGE_ID + " = " + pmsgid, null, null, null, null);
        if ((cursor.moveToFirst()) || cursor.getCount() != 0) {
            cursor.moveToLast();
            dbid = cursor.getLong(0);
        }
        return dbid;
    }

    public Message createMessages(int calledFrom, int id, String userid, String username, String usermobile, String useremail, String company, String message, String file, int type, String date, int read_flag, int del_flag) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_MESSAGE_ID, id);
        values.put(COLUMN_USER_ID, userid);
        values.put(COLUMN_USER_NAME, username);
        values.put(COLUMN_USER_MOBILE, usermobile);
        values.put(COLUMN_USER_EMAIL, useremail);
        values.put(COLUMN_USER_COMPANY, company);
        values.put(COLUMN_MESSAGE_TEXT, message);
        values.put(COLUMN_MESSAGE_FILE, file);
        values.put(COLUMN_MESSAGE_TYPE, type);
        values.put(COLUMN_MESSAGE_DATETIME, date);
        values.put(COLUMN_MESSAGE_READ_FLAG, read_flag);
        values.put(COLUMN_MESSAGE_DEL_FLAG, del_flag);

        //long insertId = sqlDB.insert(TABLE_NAME, null, values);
        sqlDB.insert(MESSAGE_TABLE_NAME, null, values);

        Cursor cursor = null;
        if (calledFrom == 1) {
            Log.d("New Message", "New Message inserted by User");
            Log.d("Max Message ID", id + "");
            Log.d("Min Message ID", minId + "");
            cursor = sqlDB.query(MESSAGE_TABLE_NAME, allMsgTblColumns, COLUMN_MESSAGE_ID + " = " + id, null, null, null, null);
        } else if (calledFrom == 0) {
            Log.d("New Message", "New Message inserted from Online DB");
            cursor = sqlDB.query(MESSAGE_TABLE_NAME, allMsgTblColumns, COLUMN_MESSAGE_ID + " = " + id, null, null, null, null);
        }
        App.preferences.edit()
                .putInt(PreferenceKeys.HIGHEST_MSG_ID, id)
                .apply();
        /*sharedPreferences.edit()
                .putInt(PreferenceKeys.HIGHEST_DB_ID, id).apply();*/
        cursor.moveToLast();
        Message newMessage = CursorToMessage(cursor);
        cursor.close();
        return newMessage;
    }

    public ArrayList<Message> getAllMessages() {
        ArrayList<Message> messages = new ArrayList<Message>();

        //Cursor cursor = sqlDB.query(MESSAGE_TABLE_NAME, allMsgTblColumns, null, null, null, null, null);
        Cursor cursor = sqlDB.query(MESSAGE_TABLE_NAME, allMsgTblColumns, COLUMN_MESSAGE_DEL_FLAG + "==0", null, null, null, null);

        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            Message message = CursorToMessage(cursor);
            messages.add(message);
        }

        cursor.close();
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_MESSAGE_READ_FLAG, 1);
        sqlDB.update(MESSAGE_TABLE_NAME, cv, null, null);
        /*App.preferences.edit()
                .putInt(PreferenceKeys.NEW_MSG_COUNT, 0)
                .apply();*/
        return messages;
    }

    public ArrayList<Message> getNewMessages(boolean a) {
        ArrayList<Message> messages = new ArrayList<Message>();
        Cursor cursor = sqlDB.query(MESSAGE_TABLE_NAME, allMsgTblColumns, COLUMN_MESSAGE_READ_FLAG + "==0 & " + COLUMN_MESSAGE_DEL_FLAG + "==0", null, null, null, null);
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            Message message = CursorToMessage(cursor);
            messages.add(message);
        }
        cursor.close();
        if (!a) {
            ContentValues cv = new ContentValues();
            cv.put(COLUMN_MESSAGE_READ_FLAG, 1);
            sqlDB.update(MESSAGE_TABLE_NAME, cv, null, null);
        }
        return messages;
    }

    public int getNewMessagesCount() {
        int nmc = 0;
        nmc = (int) DatabaseUtils.queryNumEntries(sqlDB, MESSAGE_TABLE_NAME, COLUMN_MESSAGE_READ_FLAG + "==0 & " + COLUMN_MESSAGE_DEL_FLAG + "==0");
        return nmc;
    }

    public void delMessage(long msgId, boolean delMedia) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_MESSAGE_DEL_FLAG, 1);
        sqlDB.update(MESSAGE_TABLE_NAME, values, COLUMN_MESSAGE_ID + " = " + msgId, null);
        /*int highestMsgPos = App.preferences.getInt(PreferenceKeys.HIGHEST_MSG_POS, 0);
        highestMsgPos -= 1;*/
        int highestMsgPos = getMsgsCount(MessageDbAdapter.CountTypes.NONDEL);
        App.preferences.edit()
                .putInt(PreferenceKeys.HIGHEST_MSG_POS, highestMsgPos)
                .apply();
        if (delMedia) {
            Cursor cursor = sqlDB.query(MESSAGE_TABLE_NAME, allMsgTblColumns, COLUMN_MESSAGE_ID + " = " + msgId, null, null, null, null);
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                Message message = CursorToMessage(cursor);
                int msgType = message.getMessage_Type();
                String msgFile = message.getMessage_File();
                File msgMediaFile = null;
                switch (msgType) {
                    case 2:
                        msgMediaFile = new File(Constants.FILES_APP.FOLDER_MESSAGE_IMAGE, msgFile);
                        break;
                    case 3:
                        msgMediaFile = new File(Constants.FILES_APP.FOLDER_MESSAGE_VIDEO, msgFile);
                        break;
                    case 4:
                        msgMediaFile = new File(Constants.FILES_APP.FOLDER_MESSAGE_AUDIO, msgFile);
                        break;
                    case 5:
                        msgMediaFile = new File(Constants.FILES_APP.FOLDER_MESSAGE_DOCUMENT, msgFile);
                        break;
                }
                if (msgMediaFile.exists()) {
                    msgMediaFile.delete();
                    if (msgType == 3) {
                        String fileExtension = MimeTypeMap.getFileExtensionFromUrl(msgFile);
                        String thumbName = msgFile.replace(fileExtension, "jpg");
                        File msgThumb = new File(Constants.FILES_APP.FOLDER_MESSAGE_VIDEO_THUMBNAILS, thumbName);
                        if (msgThumb.exists()) {
                            msgThumb.delete();
                        }
                    }
                }
            }
            cursor.close();
        }
    }

    private Message CursorToMessage(Cursor cursor) {
        Message newMessage = new Message(cursor.getString(1), cursor.getString(2), cursor.getString(5), cursor.getString(6), cursor.getString(7), cursor.getInt(8), cursor.getLong(0), cursor.getLong(9));
        return newMessage;
    }

    public int getExtremeMsgIds(Extremes type) {
        String query = null;
        if (type == Extremes.MIN) {
            query = "SELECT MIN(" + COLUMN_MESSAGE_ID + ") AS min_id FROM " + MESSAGE_TABLE_NAME;
        } else if (type == Extremes.MAX) {
            query = "SELECT MAX(" + COLUMN_MESSAGE_ID + ") AS max_id FROM " + MESSAGE_TABLE_NAME;
        }
        Cursor cursor = sqlDB.rawQuery(query, null);

        int id = 0;
        if (cursor.moveToFirst()) {
            do {
                id = cursor.getInt(0);
            } while (cursor.moveToNext());
        }
        return id;
    }

    public int getMsgsCount(CountTypes type) {
        String query = null;
        if (type == CountTypes.TOTAL) {
            query = "SELECT COUNT(*) FROM " + MESSAGE_TABLE_NAME;
        } else if (type == CountTypes.NEW) {
            query = "SELECT COUNT(*) FROM " + MESSAGE_TABLE_NAME + " WHERE " + COLUMN_MESSAGE_READ_FLAG + "=0";
        } else if (type == CountTypes.READ) {
            query = "SELECT COUNT(*) FROM " + MESSAGE_TABLE_NAME + " WHERE " + COLUMN_MESSAGE_READ_FLAG + "=1";
        } else if (type == CountTypes.DEL) {
            query = "SELECT COUNT(*) FROM " + MESSAGE_TABLE_NAME + " WHERE " + COLUMN_MESSAGE_DEL_FLAG + "=1";
        } else if (type == CountTypes.NONDEL) {
            query = "SELECT COUNT(*) FROM " + MESSAGE_TABLE_NAME + " WHERE " + COLUMN_MESSAGE_DEL_FLAG + "=0";
        }

        Cursor cursor = sqlDB.rawQuery(query, null);

        int id = 0;
        if (cursor.moveToFirst()) {
            do {
                id = cursor.getInt(0);
            } while (cursor.moveToNext());
        }
        return id;
    }

    public MessageDbAdapter open() throws android.database.SQLException {
        messageDbHelper = new DbHelper(context);
        sqlDB = messageDbHelper.getWritableDatabase();
        return this;
    }

    public void renew(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + MESSAGE_TABLE_NAME);
        db.execSQL(CREATE_MESSAGE_TABLE);
    }

    public void close() {
        messageDbHelper.close();
    }
}
