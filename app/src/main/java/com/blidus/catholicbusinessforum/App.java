package com.blidus.catholicbusinessforum;

import android.app.Application;
import android.content.SharedPreferences;

import com.blidus.catholicbusinessforum.global.Constants;
import com.blidus.catholicbusinessforum.interfaces.API;

import retrofit.RestAdapter;


/**
 * Created by Bimal on 22-12-2016.
 */

public class App extends Application {
    public static API api;
    public static SharedPreferences preferences;

    public static String colorPrimary;
    public static String colorPrimaryDark;
    public static String colorAccent;
    public static String colorMainButtons;

    @Override
    public void onCreate() {
        super.onCreate();
        preferences = getSharedPreferences(getString(R.string.app_name), MODE_PRIVATE);

        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(Constants.HTTP.BASE_URL)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        api = adapter.create(API.class);
    }

}
