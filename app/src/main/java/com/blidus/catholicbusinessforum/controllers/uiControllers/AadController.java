package com.blidus.catholicbusinessforum.controllers.uiControllers;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import androidx.viewpager.widget.ViewPager;
import android.view.animation.Interpolator;
import android.widget.Scroller;

import java.lang.reflect.Field;

/**
 * Created by Bimal on 25-02-2017.
 */

public class AadController extends Scroller {

    private int aadDuration = 1000;

    public void applyViewPagerScrollSpeed(ViewPager viewPager, int duration){
        aadDuration = duration;
        try {
            Field mScroller = ViewPager.class.getDeclaredField("mScroller");
            mScroller.setAccessible(true);
            mScroller.set(viewPager,this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public AadController(Context context) {
        super(context);
    }

    public AadController(Context context, Interpolator interpolator) {
        super(context, interpolator);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public AadController(Context context, Interpolator interpolator, boolean flywheel) {
        super(context, interpolator, flywheel);
    }

    @Override
    public void startScroll(int startX, int startY, int dx, int dy) {
        super.startScroll(startX, startY, dx, dy, aadDuration);
    }

    @Override
    public void startScroll(int startX, int startY, int dx, int dy, int duration) {
        super.startScroll(startX, startY, dx, dy, aadDuration);
    }
}
