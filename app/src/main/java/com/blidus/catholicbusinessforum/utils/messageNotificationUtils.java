package com.blidus.catholicbusinessforum.utils;

import android.util.Log;

import com.blidus.catholicbusinessforum.App;
import com.blidus.catholicbusinessforum.apiCallbacks.ServerKey;
import com.blidus.catholicbusinessforum.global.Constants;
import com.blidus.catholicbusinessforum.global.PreferenceKeys;

import org.json.JSONObject;

import java.net.URLDecoder;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import retrofit.Callback;
import retrofit.RetrofitError;

/**
 * Created by Bimal on 26-02-2017.
 */

public class messageNotificationUtils {

    public static void sendMessage(final int userId, final String userName, final int messageId, final String messageText) {
        Log.d("User ID", userId+"");
        Log.d("User Name", userName+"");
        Log.d("Message ID", messageId+"");
        Log.d("Message Text", messageText+"");

        App.api.getServerKey(App.preferences.getString(PreferenceKeys.TOKEN, null), new Callback<ServerKey>() {
            @Override
            public void success(final ServerKey serverKey, retrofit.client.Response response) {

                new Thread() {
                    @Override
                    public void run() {
                        try {
                            //String afterDecode = URLDecoder.decode("abcdef", "UTF-8");
                            OkHttpClient client = new OkHttpClient();
                            JSONObject root = new JSONObject();
                            root.put("to", Constants.FIREBASE.TOPIC);
                            JSONObject msg = new JSONObject();
                            msg.put("user_Id", userId);
                            msg.put("user_name", userName);
                            msg.put("messageID", messageId);
                            msg.put("messageText", messageText);
                            root.put("data", msg);
                            MediaType mediaType = MediaType.parse("application/json");
                            RequestBody body = RequestBody.create(mediaType, root.toString(0));
                            Request request = new Request.Builder()
                                    .url("https://fcm.googleapis.com/fcm/send")
                                    .post(body)
                                    .addHeader("content-type", "application/json")
                                    .addHeader("authorization", "key="+ URLDecoder.decode(serverKey.FB_CM_Server_Key,"UTF-8"))
                                    .build();

                            Response response = client.newCall(request).execute();
                            Log.d("sendMessage", response.body().string());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }.start();
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d("sendMessage", "Unable to send the message notification");
            }
        });
    }

}