package com.blidus.catholicbusinessforum.global;

import android.os.Environment;

import com.blidus.catholicbusinessforum.R;

import java.io.File;

/**
 * Created by Bimal on 16-02-2017.
 */

public class Constants {

	public static final String TAG = "Catholic Business Forum";

	public static final class HTTP {
		public static final String COMPANY_URL = "http://colonialvision.com";
		/*public static final String BASE_URL = "http://cbfkerala.com";
		public static final String PRIVACY_POLICY_URL = "http://cbfkerala.com/termsandconditions";*/
		/*public static final String BASE_URL = "http://cbfkerala.com/cbftest";
        public static final String PRIVACY_POLICY_URL = "http://cbfkerala.com/cbftest/termsandconditions";*/
		public static final String BASE_URL = "http://jewellgroup.in/cbf";
		public static final String PRIVACY_POLICY_URL = "http://jewellgroup.in/cbf/termsandconditions";
	}

	public static final class FIREBASE {
		//public static final String TOPIC = "/topics/catholicbusinessforum";
		//public static final String TOPIC = "/topics/catholicbusinessforumtest";
		public static final String TOPIC = "/topics/catholicbusinessforumlocaltest";
	}

	public static final class ADS {
		public static final String AD_SPONSOR = "https://www.facebook.com/ESAFMicrofin/";
		public static final int ADS_IMAGE_COUNT = 3;
		public static final int[] ADS_IDS = {R.drawable.ad_example_1, R.drawable.ad_example_2, R.drawable.ad_example_3};
		public static final String[] AD_URLS = {"http://colonialvision.com", "http://fruitomans.com/", "http://www.bednbeyond.in/"};
        /*public static final int ADS_IMAGE_COUNT = 4;
        public static final int[] ADS_IDS = {R.drawable.ad_example_1, R.drawable.ad_example_2, R.drawable.ad_example_3, R.drawable.ad_example_4};
        public static final String[] AD_URLS = {"http://colonialvision.com", "https://play.google.com/store/apps/details?id=com.blidus.mypolice", "https://www.facebook.com", "https://www.google.com", "https://www.instagram.com/?hl=en"};*/
	}

	public static final class DEVELOPERS {
		public static final boolean DEV_FLAG_1 = true;
		public static final boolean DEV_FLAG_2 = true;
		public static final boolean DEV_FLAG_3 = true;
		public static final boolean DEV_FLAG_4 = true;
		public static final boolean DEV_FLAG_5 = true;
	}

	public static final class PERMISSION_REQUESTS {
		public static final int REQUEST_ALL_PERMISSION = 0;
		public static final int REQUEST_CALL_PERMISSION = 1;
		public static final int REQUEST_CONTACT_WRITE_PERMISSION = 2;
		public static final int REQUEST_CAMERA_PERMISSIONS = 3;
		public static final int REQUEST_MICROPHONE_PERMISSIONS = 4;
		public static final int REQUEST_FILE_PERMISSIONS = 5;
	}

	public static final class DATABASE {
		public static final String DATABASE_NAME = "cbf.db";
		public static final int DATABASE_VERSION = 1;
	}

	public static final class FOLDERS_ONLINE {
		public static final String FOLDER_USER_IMAGE = "/assets/user/";
		public static final String FOLDER_MESSAGE_IMAGE = "/assets/website_assets/messages/images/";
		public static final String FOLDER_MESSAGE_AUDIO = "/assets/website_assets/messages/audios/";
		public static final String FOLDER_MESSAGE_VIDEO = "/assets/website_assets/messages/videos/";
		public static final String FOLDER_MESSAGE_DOCUMENT = "/assets/website_assets/messages/documents/";
	}

	public static final class FOLDER_PATHS_APP {
		public static final String PATH_CBF_CACHE = "CBF/.cache/";
		public static final String PATH_USER_IMAGE = "CBF/user/";
		public static final String PATH_MESSAGE_IMAGE = "CBF/messages/images/";
		public static final String PATH_MESSAGE_AUDIO = "CBF/messages/audios/";
		public static final String PATH_MESSAGE_VIDEO = "CBF/messages/videos/";
		public static final String PATH_MESSAGE_VIDEO_THUMBNAILS = "CBF/messages/videos/.thumbnails/";
		public static final String PATH_MESSAGE_DOCUMENT = "CBF/messages/documents/";
	}

	public static final class FILES_APP {
		public static final File FOLDER_ROOT = Environment.getExternalStorageDirectory();
		public static final File FOLDER_CBF_CACHE = new File(FOLDER_ROOT, FOLDER_PATHS_APP.PATH_CBF_CACHE);
		public static final File FOLDER_USER_IMAGE = new File(FOLDER_ROOT, FOLDER_PATHS_APP.PATH_USER_IMAGE);
		public static final File FOLDER_MESSAGE_IMAGE = new File(FOLDER_ROOT, FOLDER_PATHS_APP.PATH_MESSAGE_IMAGE);
		public static final File FOLDER_MESSAGE_AUDIO = new File(FOLDER_ROOT, FOLDER_PATHS_APP.PATH_MESSAGE_AUDIO);
		public static final File FOLDER_MESSAGE_VIDEO = new File(FOLDER_ROOT, FOLDER_PATHS_APP.PATH_MESSAGE_VIDEO);
		public static final File FOLDER_MESSAGE_VIDEO_THUMBNAILS = new File(FOLDER_ROOT, FOLDER_PATHS_APP.PATH_MESSAGE_VIDEO_THUMBNAILS);
		public static final File FOLDER_MESSAGE_DOCUMENT = new File(FOLDER_ROOT, FOLDER_PATHS_APP.PATH_MESSAGE_DOCUMENT);
	}

	public static final class FILE_TYPES {
		public static final String FILE_IMAGE = "image/*";
		public static final String FILE_IMAGE_JPEG = "image/jpeg";
		public static final String FILE_IMAGE_JPG = "image/jpg";
		public static final String FILE_IMAGE_PNG = "image/png";
		public static final String FILE_VIDEO = "video/*";
		public static final String FILE_VIDEO_MP4 = "video/mp4";
		public static final String FILE_VIDEO_3GP = "video/3gp";
		public static final String FILE_VIDEO_3GPP = "video/3gpp";
		public static final String FILE_VIDEO_AVI = "video/avi";
		public static final String FILE_AUDIO = "audio/*";
		public static final String FILE_AUDIO_MPEG = "audio/mpeg";
		public static final String FILE_AUDIO_3GP = "audio/3gp";
		public static final String FILE_AUDIO_AMR = "audio/amr";
		public static final String FILE_AUDIO_WAV = "audio/x-wav";
		public static final String FILE_AUDIO_OGG = "application/ogg";
		public static final String FILE_PDF = "application/pdf";
		public static final String FILE_DOC = "application/msword";
		public static final String FILE_XLS = "application/vnd.ms-excel";
		public static final String FILE_PPT = "application/vnd.ms-powerpoint";
	}

	public static final class FILE_SIZES {
		public static final int ONE_KB = 1024;
		public static final int ONE_MB = 1024 * 1024;
		public static final float FILE_IMAGE_SIZE = 5;
		public static final float FILE_VIDEO_SIZE = 15;
		public static final float FILE_AUDIO_SIZE = 5;
		public static final float FILE_ATTACH_SIZE = 5;
	}

}
