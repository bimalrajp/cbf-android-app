package com.blidus.catholicbusinessforum.global;

/**
 * Created by Bimal on 22-12-2016.
 */

public class PreferenceKeys {

    public static final String FIRST_PERMISSION_REQUEST = "permissionRequest";
    public static final String FIRST_TIME_MAIN_ACTIVITY = "firstTimeMainActivity";

    public static final String PERMISSION_CAM = "permissionCam";
    public static final String PERMISSION_MIC = "permissionMic";
    public static final String PERMISSION_STR = "permissionStr";
    public static final String PERMISSION_CNT = "permissionCnt";
    public static final String PERMISSION_CAL = "permissionCal";
    public static final String PERMISSION_SMS = "permissionSms";

    public static final String MEDIA_AUTO_DOWNLOAD_IMAGE = "mediaImage";
    public static final String MEDIA_AUTO_DOWNLOAD_VIDEO = "mediaVideo";
    public static final String MEDIA_AUTO_DOWNLOAD_AUDIO = "mediaAudio";
    public static final String MEDIA_AUTO_DOWNLOAD_FILE = "mediaFile";

    public static final String ADMIN_CONTACTS_EDIT_MODE = "contactsEditMode";

    public static final String LOGIN = "login";
    public static final String TOKEN = "token";
    public static final String FCM_TOKEN = "fcm_token";
    public static final String LOGIN_ID = "login_id";
    public static final String LOGIN_TYPE = "login_type";
    public static final String PROFILE_ACTIVE = "profile_active";
    public static final String MESSAGE_ACTIVE = "message_active";
    public static final String LOWEST_MSG_ID = "minMsgId";
    public static final String HIGHEST_MSG_ID = "maxMsgId";
    public static final String HIGHEST_MSG_POS = "maxMsgPos";
    public static final String CURRENT_MSG_POS = "curMsgPos";
    public static final String NEW_MSG_COUNT = "newMsgCount";
    public static final String LOWEST_CNT_ID = "minCntId";
    public static final String HIGHEST_CNT_ID = "maxCntId";
    public static final String HIGHEST_CNT_POS = "maxCntPos";
    public static final String CURRENT_CNT_POS = "curCntPos";
    public static final String TOTAL_CNT_COUNT = "totalContactCount";

    public static final String USER_DEFINED_THEME_COLOR = "userDefinedColor";
    public static final String USER_DEFINED_THEME_COLOR_PRIMARY = "userDefinedColorPrimary";
    public static final String USER_DEFINED_THEME_COLOR_ACCENT = "userDefinedColorAccent";
    public static final String USER_DEFINED_THEME_COLOR_BACKGROUND_IMAGE = "userDefinedColorBackgroundImage";
    public static final String USER_DEFINED_THEME_COLOR_MY_MESSAGE = "userDefinedColorMyMessage";

    public static final int FALSE = 0;
    public static final int TRUE = 1;
}