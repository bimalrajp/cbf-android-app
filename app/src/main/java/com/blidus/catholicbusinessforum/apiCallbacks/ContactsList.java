package com.blidus.catholicbusinessforum.apiCallbacks;

import com.blidus.catholicbusinessforum.dataModels.callbackDatatypes.ContactItem;

/**
 * Created by Bimal on 16-02-2017.
 */

public class ContactsList {

    public int ErrorFlag;
    public int TokenFlag;
    public int ContactsCount;
    public ContactItem[] Contacts;
}
