package com.blidus.catholicbusinessforum.apiCallbacks;

import com.blidus.catholicbusinessforum.dataModels.callbackDatatypes.ContactItem;

/**
 * Created by Bimal on 16-03-2017.
 */

public class ProfileUpdate {

    public int ErrorFlag;
    public int TokenFlag;
    public int ProfileUpdateFlag;
    public ContactItem[] Contacts;
}
