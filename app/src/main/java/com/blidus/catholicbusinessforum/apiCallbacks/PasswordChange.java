package com.blidus.catholicbusinessforum.apiCallbacks;

/**
 * Created by Bimal on 17-03-2017.
 */

public class PasswordChange {

    public int ErrorFlag;
    public int TokenFlag;
    public int CurrentPasswordFlag;
    public int PasswordUpdateFlag;
}
