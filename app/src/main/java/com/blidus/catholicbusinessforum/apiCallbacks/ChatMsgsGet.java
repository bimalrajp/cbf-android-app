package com.blidus.catholicbusinessforum.apiCallbacks;

import com.blidus.catholicbusinessforum.dataModels.callbackDatatypes.ChatMsgItem;

/**
 * Created by Bimal on 04-02-2017.
 */

public class ChatMsgsGet {

    public int ErrorFlag;
    public int TokenFlag;
    public int MessagesCount;
    public ChatMsgItem[] Messages;
    public int UserMessageFlag;
}