package com.blidus.catholicbusinessforum.apiCallbacks;

/**
 * Created by Bimal on 02-01-2017.
 */

public class ChatMsgSend {

    public int ErrorFlag;
    public int TokenFlag;
    public int MsgSendFlag;
    public int MessageID;
    public String UserId;
    public String UserFullName;
    public String UserMobile;
    public String UserEmail;
    public String UserCompanyName;
    public String MessageText;
    public String MessageFile;
    public int MessageType;
    public String MsgDateTime;
    public int MessageDelFlag;
    public int UserMessageFlag;
}
