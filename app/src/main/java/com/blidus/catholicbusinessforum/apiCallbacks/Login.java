package com.blidus.catholicbusinessforum.apiCallbacks;

/**
 * Created by Bimal on 22-12-2016.
 */

public class Login {

    public int PKUserID;
    public String UserName;
    public String UserFullName;
    public int Status;
    public int MessageFlag;
    public String LoginTime;
    public int ErrorFlag;
    public String Token;
    public int LoginType;
    public int LoginFlag;
    public int RegisterFlag;
}
