package com.blidus.catholicbusinessforum.services.nativeServices;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.blidus.catholicbusinessforum.App;
import com.blidus.catholicbusinessforum.adapters.dbAdapters.ContactDBAdapter;
import com.blidus.catholicbusinessforum.apiCallbacks.ContactsList;
import com.blidus.catholicbusinessforum.global.PreferenceKeys;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Bimal on 09-03-2017.
 */

public class loadOnlineContactService extends Service {

    public final IBinder onlineContactBinder = new ContactBinder();

    @Override
    public IBinder onBind(Intent intent) {
        return onlineContactBinder;
    }

    public class ContactBinder extends Binder {
        public loadOnlineContactService getService() {
            return loadOnlineContactService.this;
        }
    }

    public boolean cntLoadFromOnlineDb(int create, final TextView textView, final ProgressBar progressBar) {
        if (create == 1) {
            create = App.preferences.getInt(PreferenceKeys.HIGHEST_CNT_ID, 0);
        }
        App.api.contactsget(App.preferences.getString(PreferenceKeys.TOKEN, null), create, new Callback<ContactsList>() {
            @Override
            public void success(final ContactsList contactsList, Response response) {
                if (contactsList.ErrorFlag == 0) {
                    if (contactsList.ContactsCount > 0) {
                        ContactDBAdapter dbAdapter = new ContactDBAdapter(getBaseContext());
                        dbAdapter.open();
                        dbAdapter.writeToDb(contactsList.Contacts);
                        int maxID = App.preferences.getInt(PreferenceKeys.HIGHEST_CNT_ID, 0);
                        int minID = App.preferences.getInt(PreferenceKeys.LOWEST_CNT_ID, 0);
                        if (minID != 0 && maxID != 0) {
                            App.preferences.edit()
                                    .putInt(PreferenceKeys.HIGHEST_CNT_POS, ((maxID - minID) + 1))
                                    .putInt(PreferenceKeys.CURRENT_CNT_POS, 0)
                                    .apply();
                        }
                        dbAdapter.close();
                    }
                    retrieveDbCntData(textView, progressBar);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                retrieveDbCntData(textView, progressBar);
            }
        });
        return true;
    }

    private void retrieveDbCntData(TextView textView, ProgressBar progressBar) {
        int highCntPos = 0;
        ContactDBAdapter cntDbAdapter = new ContactDBAdapter(getBaseContext());
        cntDbAdapter.open();
        int totalContactsCount = (int) cntDbAdapter.getTotalContactsCount();
        int maxID = cntDbAdapter.getExtremeCntIds(ContactDBAdapter.Extremes.MAX);
        int minID = cntDbAdapter.getExtremeCntIds(ContactDBAdapter.Extremes.MIN);
        textView.setText(String.valueOf(totalContactsCount));
        progressBar.setVisibility(View.GONE);
        if (minID != 0 && maxID != 0) {
            highCntPos = ((maxID - minID) + 1);
        }
        App.preferences.edit()
                .putInt(PreferenceKeys.LOWEST_CNT_ID, minID)
                .putInt(PreferenceKeys.HIGHEST_CNT_ID, maxID)
                .putInt(PreferenceKeys.HIGHEST_CNT_POS, highCntPos)
                .putInt(PreferenceKeys.TOTAL_CNT_COUNT, totalContactsCount)
                .apply();
        cntDbAdapter.close();
    }
}
