package com.blidus.catholicbusinessforum.services.nativeServices;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.blidus.catholicbusinessforum.App;
import com.blidus.catholicbusinessforum.adapters.dbAdapters.MessageDbAdapter;
import com.blidus.catholicbusinessforum.apiCallbacks.ChatMsgsGet;
import com.blidus.catholicbusinessforum.global.PreferenceKeys;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.blidus.catholicbusinessforum.ui.MainActivity.EXTRA_MESSAGE_LOAD_DIRECTION;

/**
 * Created by Bimal on 08-03-2017.
 */

public class GetMessageService extends Service {

    private int maxID = 0;
    private int minID = 0;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        int messageLoadDirection = intent.getIntExtra(EXTRA_MESSAGE_LOAD_DIRECTION, 1);
        msgLoadFromOnlineDb(messageLoadDirection);
        return START_STICKY;
    }

    public void msgLoadFromOnlineDb(int direction) {
        App.api.chatmsgsget(App.preferences.getString(PreferenceKeys.TOKEN, null), App.preferences.getInt(PreferenceKeys.HIGHEST_MSG_ID, 0), App.preferences.getInt(PreferenceKeys.LOWEST_MSG_ID, 0), direction, new Callback<ChatMsgsGet>() {
            @Override
            public void success(final ChatMsgsGet chatMsgsGet, Response response) {
                App.preferences.edit()
                        .putInt(PreferenceKeys.MESSAGE_ACTIVE, chatMsgsGet.UserMessageFlag)
                        .apply();
                if (chatMsgsGet.ErrorFlag == 0) {
                    if (chatMsgsGet.MessagesCount > 0) {
                        MessageDbAdapter dbAdapter = new MessageDbAdapter(getBaseContext());
                        dbAdapter.open();
                        dbAdapter.writeToDb(chatMsgsGet.Messages, MessageDbAdapter.InsertTypes.NEW);
                        maxID = App.preferences.getInt(PreferenceKeys.HIGHEST_MSG_ID, 0);
                        minID = App.preferences.getInt(PreferenceKeys.LOWEST_MSG_ID, 0);
                        if (minID != 0 && maxID != 0) {
                            App.preferences.edit()
                                    .putInt(PreferenceKeys.HIGHEST_MSG_POS, ((maxID - minID) + 1))
                                    .putInt(PreferenceKeys.NEW_MSG_COUNT, chatMsgsGet.MessagesCount)
                                    .apply();
                        }
                        dbAdapter.close();
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
            }
        });
    }
}
