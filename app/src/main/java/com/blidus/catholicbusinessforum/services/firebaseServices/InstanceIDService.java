package com.blidus.catholicbusinessforum.services.firebaseServices;

import android.util.Log;

import com.blidus.catholicbusinessforum.App;
import com.blidus.catholicbusinessforum.global.Constants;
import com.blidus.catholicbusinessforum.global.PreferenceKeys;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by Bimal on 26-02-2017.
 */

public class InstanceIDService extends FirebaseInstanceIdService {
    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String token = FirebaseInstanceId.getInstance().getToken();
        App.preferences.edit().putString(PreferenceKeys.FCM_TOKEN, token).apply();
        Log.d(Constants.TAG, "New Token: " + token);
    }
}
