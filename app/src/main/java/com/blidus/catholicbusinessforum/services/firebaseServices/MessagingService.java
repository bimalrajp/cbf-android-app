package com.blidus.catholicbusinessforum.services.firebaseServices;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import androidx.core.app.NotificationCompat;
import android.util.Log;

import com.blidus.catholicbusinessforum.App;
import com.blidus.catholicbusinessforum.R;
import com.blidus.catholicbusinessforum.adapters.dbAdapters.MessageDbAdapter;
import com.blidus.catholicbusinessforum.apiCallbacks.ChatMsgsGet;
import com.blidus.catholicbusinessforum.global.Constants;
import com.blidus.catholicbusinessforum.global.PreferenceKeys;
import com.blidus.catholicbusinessforum.ui.ChatActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.blidus.catholicbusinessforum.ui.MainActivity.EXTRA_FROM_MAIN;

/**
 * Created by Bimal on 26-02-2017.
 */

public class MessagingService extends FirebaseMessagingService {

    private Handler handler;

    public static final String ACTION = "finishAll";

    @Override
    public void onCreate() {
        super.onCreate();
        handler = new Handler();
    }


    @Override
    public void onMessageReceived(RemoteMessage message) {
        if (App.preferences.getInt(PreferenceKeys.MESSAGE_ACTIVE, PreferenceKeys.FALSE) == 1) {
            Log.d(Constants.TAG, "From :" + message.getFrom());

            if (message.getData().size() > 0) {
                Log.d(Constants.TAG, "Message Data : " + message.getData());
            }

            final Map<String, String> msg = message.getData();
            for (String key : msg.keySet()) {
                Log.d(Constants.TAG, key + ": " + msg.get(key));
            }

            if (Integer.parseInt(msg.get("user_Id")) != App.preferences.getInt(PreferenceKeys.LOGIN_ID, 0)) {

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        App.api.chatmsgsget(App.preferences.getString(PreferenceKeys.TOKEN, null), App.preferences.getInt(PreferenceKeys.HIGHEST_MSG_ID, 0), App.preferences.getInt(PreferenceKeys.LOWEST_MSG_ID, 0), 1, new Callback<ChatMsgsGet>() {
                            @Override
                            public void success(final ChatMsgsGet chatMsgsGet, Response response) {
                                if (chatMsgsGet.ErrorFlag == 0 && chatMsgsGet.MessagesCount > 0) {
                                    MessageDbAdapter dbAdapter = new MessageDbAdapter(getBaseContext());
                                    dbAdapter.open();
                                    dbAdapter.writeToDb(chatMsgsGet.Messages, MessageDbAdapter.InsertTypes.NEW);
                                    dbAdapter.close();
                                }
                            }

                            @Override
                            public void failure(RetrofitError error) {
                            }
                        });
                    }
                });

                Intent intent = new Intent(this, ChatActivity.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(EXTRA_FROM_MAIN, false);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                //intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

                //creating notification text
                String notificationText = null;
                notificationText = msg.get("user_name");
                if (msg.get("messageText") != null) {
                    notificationText += " : " + msg.get("messageText");
                }

                //use System.currentTimeMillis() to have a unique ID for the pending intent
                PendingIntent pendingIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, PendingIntent.FLAG_ONE_SHOT | PendingIntent.FLAG_UPDATE_CURRENT);

                // set Notification Sound
                Uri notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(getResources().getString(R.string.app_name))
                        .setContentText(notificationText)
                        .setAutoCancel(true)
                        .setSound(notificationSound)
                        .setContentIntent(pendingIntent);

                Notification cbfNotification = notificationBuilder.build();

                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                notificationManager.notify(0, cbfNotification);
            }
        }
    }
}
