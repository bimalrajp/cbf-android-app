package com.blidus.catholicbusinessforum.services.nativeServices;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.blidus.catholicbusinessforum.App;
import com.blidus.catholicbusinessforum.adapters.dbAdapters.MessageDbAdapter;
import com.blidus.catholicbusinessforum.apiCallbacks.ChatMsgsGet;
import com.blidus.catholicbusinessforum.global.PreferenceKeys;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * Created by Bimal on 07-03-2017.
 */

public class loadOnlineMessageService extends Service {

    public final IBinder onlineMessageBinder = new MessageBinder();

    @Override
    public IBinder onBind(Intent intent) {
        return onlineMessageBinder;
    }

    public class MessageBinder extends Binder {
        public loadOnlineMessageService getService() {
            return loadOnlineMessageService.this;
        }
    }

    public boolean msgLoadFromOnlineDb(int direction, final LinearLayout sb_btn_messages, final LinearLayout ll_user_messaging_status, final LinearLayout ll_chat_essentials, final LinearLayout btn_new_msgs, final TextView textView, final ProgressBar progressBar) {
        App.api.chatmsgsget(App.preferences.getString(PreferenceKeys.TOKEN, null), App.preferences.getInt(PreferenceKeys.HIGHEST_MSG_ID, 0), App.preferences.getInt(PreferenceKeys.LOWEST_MSG_ID, 0), direction, new Callback<ChatMsgsGet>() {
            @Override
            public void success(final ChatMsgsGet chatMsgsGet, Response response) {
                App.preferences.edit()
                        .putInt(PreferenceKeys.MESSAGE_ACTIVE, chatMsgsGet.UserMessageFlag)
                        .apply();
                try {
                    if (chatMsgsGet.ErrorFlag == 0) {
                        if (chatMsgsGet.MessagesCount > 0) {
                            int maxID = 0;
                            int minID = 0;
                            MessageDbAdapter dbAdapter = new MessageDbAdapter(getBaseContext());
                            dbAdapter.open();
                            dbAdapter.writeToDb(chatMsgsGet.Messages, MessageDbAdapter.InsertTypes.NEW);
                            maxID = App.preferences.getInt(PreferenceKeys.HIGHEST_MSG_ID, 0);
                            minID = App.preferences.getInt(PreferenceKeys.LOWEST_MSG_ID, 0);
                            int highestPos = dbAdapter.getMsgsCount(MessageDbAdapter.CountTypes.NONDEL);
                            if (minID != 0 && maxID != 0) {
                                App.preferences.edit()
                                        //.putInt(PreferenceKeys.HIGHEST_MSG_POS, ((maxID - minID) + 1))
                                        .putInt(PreferenceKeys.HIGHEST_MSG_POS, highestPos)
                                        .putInt(PreferenceKeys.NEW_MSG_COUNT, chatMsgsGet.MessagesCount)
                                        .apply();
                            }
                            dbAdapter.close();
                        }
                        retrieveDbMsgData(chatMsgsGet.UserMessageFlag, sb_btn_messages, ll_user_messaging_status, ll_chat_essentials, btn_new_msgs, textView, progressBar);
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                try {
                    retrieveDbMsgData(App.preferences.getInt(PreferenceKeys.MESSAGE_ACTIVE, 0), sb_btn_messages, ll_user_messaging_status, ll_chat_essentials, btn_new_msgs, textView, progressBar);
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        return true;
    }

    private void retrieveDbMsgData(int ma, LinearLayout sb_btn_messages, LinearLayout ll_user_messaging_status, LinearLayout ll_chat_essentials, LinearLayout btn_new_msgs, TextView textView, ProgressBar progressBar) {
        try {
            int highMsgPos = 0;
            MessageDbAdapter msgDbAdapter = new MessageDbAdapter(getBaseContext());
            msgDbAdapter.open();
            int maxID = msgDbAdapter.getExtremeMsgIds(MessageDbAdapter.Extremes.MAX);
            int minID = msgDbAdapter.getExtremeMsgIds(MessageDbAdapter.Extremes.MIN);
            int newMsgs = msgDbAdapter.getMsgsCount(MessageDbAdapter.CountTypes.NEW);
            highMsgPos = msgDbAdapter.getMsgsCount(MessageDbAdapter.CountTypes.NONDEL);
            App.preferences.edit()
                    .putInt(PreferenceKeys.NEW_MSG_COUNT, newMsgs)
                    .apply();
            textView.setText(String.valueOf(newMsgs));
            progressBar.setVisibility(View.GONE);
            configMessagingView(ma, sb_btn_messages, ll_user_messaging_status, ll_chat_essentials, btn_new_msgs);
            /*if (minID != 0 && maxID != 0) {
               highMsgPos = ((maxID - minID) + 1);
            }*/
            App.preferences.edit()
                    .putInt(PreferenceKeys.LOWEST_MSG_ID, minID)
                    .putInt(PreferenceKeys.HIGHEST_MSG_ID, maxID)
                    .putInt(PreferenceKeys.HIGHEST_MSG_POS, highMsgPos)
                    .apply();
            msgDbAdapter.close();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private void configMessagingView(int ma, LinearLayout sb_btn_messages, LinearLayout ll_user_messaging_status, LinearLayout ll_chat_essentials, LinearLayout btn_new_msgs) {
        if (ma == 1) {
            sb_btn_messages.setVisibility(View.VISIBLE);
            ll_user_messaging_status.setVisibility(View.GONE);
            ll_chat_essentials.setVisibility(View.VISIBLE);
            btn_new_msgs.setVisibility(View.VISIBLE);
        } else {
            sb_btn_messages.setVisibility(View.GONE);
            ll_user_messaging_status.setVisibility(View.VISIBLE);
            ll_chat_essentials.setVisibility(View.GONE);
            btn_new_msgs.setVisibility(View.GONE);
        }
    }

}
