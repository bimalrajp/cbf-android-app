package com.blidus.catholicbusinessforum.services.nativeServices;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import androidx.annotation.Nullable;

import com.blidus.catholicbusinessforum.App;
import com.blidus.catholicbusinessforum.adapters.dbAdapters.MessageDbAdapter;
import com.blidus.catholicbusinessforum.dataModels.dbDatatypes.Message;
import com.blidus.catholicbusinessforum.global.Constants;
import com.blidus.catholicbusinessforum.global.PreferenceKeys;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;

import java.io.File;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

import static com.blidus.catholicbusinessforum.ui.MainActivity.EXTRA_NEW_MESSAGES;

/**
 * Created by Bimal on 04-03-2017.
 */

public class MessageFileDownloadService extends Service {

    private static AsyncHttpClient http;
    private MessageDbAdapter messageDbAdapter;
    private ArrayList<Message> newMessages;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */

    @Override
    public void onCreate() {
        super.onCreate();
        http = new AsyncHttpClient();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        boolean newMessagesCheck = intent.getBooleanExtra(EXTRA_NEW_MESSAGES, false);
        messageDbAdapter = new MessageDbAdapter(this);
        messageDbAdapter.open();
        newMessages = messageDbAdapter.getNewMessages(newMessagesCheck);
        messageDbAdapter.close();
        int i;
        for (i = 0; i < newMessages.size(); i++) {
            Message newMessage = newMessages.get(i);
            int messageType = newMessage.getMessage_Type();
            String messageFile = newMessage.getMessage_File();
            downloadMedia(messageType, messageFile);
        }
        if (i == newMessages.size()) {
            stopSelf();
        }
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void downloadMedia(int messageType, String messageFile) {
        String tempFileUrl = null;
        File msgFile;
        switch (messageType) {
            case 2:
                if (App.preferences.getBoolean(PreferenceKeys.MEDIA_AUTO_DOWNLOAD_IMAGE, false)) {
                    tempFileUrl = Constants.HTTP.BASE_URL + Constants.FOLDERS_ONLINE.FOLDER_MESSAGE_IMAGE + messageFile;
                    if (App.preferences.getBoolean(PreferenceKeys.PERMISSION_STR, false)) {
                        msgFile = new File(Constants.FILES_APP.FOLDER_MESSAGE_IMAGE, messageFile);
                        if (!msgFile.exists()) {
                            fetchMessageFile(tempFileUrl, messageType, messageFile);
                        }
                    }
                }
                break;
            case 3:
                if (App.preferences.getBoolean(PreferenceKeys.MEDIA_AUTO_DOWNLOAD_VIDEO, false)) {
                    tempFileUrl = Constants.HTTP.BASE_URL + Constants.FOLDERS_ONLINE.FOLDER_MESSAGE_VIDEO + messageFile;
                    if (App.preferences.getBoolean(PreferenceKeys.PERMISSION_STR, false)) {
                        msgFile = new File(Constants.FILES_APP.FOLDER_MESSAGE_VIDEO, messageFile);
                        if (!msgFile.exists()) {
                            fetchMessageFile(tempFileUrl, messageType, messageFile);
                        }
                    }
                }
                break;
            case 4:
                if (App.preferences.getBoolean(PreferenceKeys.MEDIA_AUTO_DOWNLOAD_AUDIO, false)) {
                    tempFileUrl = Constants.HTTP.BASE_URL + Constants.FOLDERS_ONLINE.FOLDER_MESSAGE_AUDIO + messageFile;
                    if (App.preferences.getBoolean(PreferenceKeys.PERMISSION_STR, false)) {
                        msgFile = new File(Constants.FILES_APP.FOLDER_MESSAGE_AUDIO, messageFile);
                        if (!msgFile.exists()) {
                            fetchMessageFile(tempFileUrl, messageType, messageFile);
                        }
                    }
                }
                break;
            case 5:
                if (App.preferences.getBoolean(PreferenceKeys.MEDIA_AUTO_DOWNLOAD_FILE, false)) {
                    tempFileUrl = Constants.HTTP.BASE_URL + Constants.FOLDERS_ONLINE.FOLDER_MESSAGE_DOCUMENT + messageFile;
                    if (App.preferences.getBoolean(PreferenceKeys.PERMISSION_STR, false)) {
                        msgFile = new File(Constants.FILES_APP.FOLDER_MESSAGE_DOCUMENT, messageFile);
                        if (!msgFile.exists()) {
                            fetchMessageFile(tempFileUrl, messageType, messageFile);
                        }
                    }
                }
                break;
        }
    }

    private void fetchMessageFile(String url, int msgType, String filename) {
        final File msgFile = makeMessageFile(msgType, filename);
        http.get(url, new FileAsyncHttpResponseHandler(msgFile) {
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                stopSelf();
            }
        });
    }

    private static File makeMessageFile(int msgType, String filename) {
        File msgFile = null;
        switch (msgType) {
            case 2:
                Constants.FILES_APP.FOLDER_MESSAGE_IMAGE.mkdirs();
                msgFile = new File(Constants.FILES_APP.FOLDER_MESSAGE_IMAGE, filename);
                break;
            case 3:
                Constants.FILES_APP.FOLDER_MESSAGE_VIDEO.mkdirs();
                msgFile = new File(Constants.FILES_APP.FOLDER_MESSAGE_VIDEO, filename);
                break;
            case 4:
                Constants.FILES_APP.FOLDER_MESSAGE_AUDIO.mkdirs();
                msgFile = new File(Constants.FILES_APP.FOLDER_MESSAGE_AUDIO, filename);
                break;
            case 5:
                Constants.FILES_APP.FOLDER_MESSAGE_DOCUMENT.mkdirs();
                msgFile = new File(Constants.FILES_APP.FOLDER_MESSAGE_DOCUMENT, filename);
                break;
        }
        return msgFile;
    }
}
