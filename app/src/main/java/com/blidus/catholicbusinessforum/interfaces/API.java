package com.blidus.catholicbusinessforum.interfaces;


import com.blidus.catholicbusinessforum.apiCallbacks.Approval;
import com.blidus.catholicbusinessforum.apiCallbacks.ChatMsgSend;
import com.blidus.catholicbusinessforum.apiCallbacks.ChatMsgsGet;
import com.blidus.catholicbusinessforum.apiCallbacks.ContactsList;
import com.blidus.catholicbusinessforum.apiCallbacks.Login;
import com.blidus.catholicbusinessforum.apiCallbacks.Logout;
import com.blidus.catholicbusinessforum.apiCallbacks.PasswordChange;
import com.blidus.catholicbusinessforum.apiCallbacks.ProfileUpdate;
import com.blidus.catholicbusinessforum.apiCallbacks.ServerKey;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;

/**
 * Created by Bimal on 22-12-2016.
 */

public interface API {

    public static final int USER_TYPE_USER = 0;
    public static final int USER_TYPE_ADMIN = 1;

    public static final int STATUS_USER_INACTIVE = 0;
    public static final int STATUS_USER_ACTIVE = 1;

    public static final int LOGIN_FAILED = 0;
    public static final int LOGIN_SUCCESS = 1;
    public static final int LOGIN_INCORRECT_PASSWORD = 2;
    public static final int LOGIN_INCORRECT_USER_CREDENTIALS = 3;

    @FormUrlEncoded
    @POST("/verifylogin/app")
    void login(
            @Field("Username") String userName,
            @Field("Password") String passWord,
            Callback<Login> response
    );

    @Multipart
    @POST("/user/register/app")
    void register(
            @Part("Name") TypedString Name,
            @Part("Gender") TypedString Gender,
            @Part("Mobile") TypedString Mobile,
            @Part("Email") TypedString Email,
            @Part("CompanyName") TypedString CompanyName,
            @Part("ProductName") TypedString ProductName,
            @Part("UserDescription") TypedString UserDescription,
            @Part("RUserName") TypedString RUsername,
            @Part("RPassword") TypedString RPassword,
            @Part("UserImage") TypedFile UserImage,
            Callback<Login> response
    );

    @Multipart
    @POST("/user/update_profile/app")
    void profileUpdate(
            @Part("Token") TypedString Token,
            @Part("PKUserID") TypedString PKUserID,
            @Part("UserImage") TypedFile UserImage,
            @Part("PreUserImage") TypedString PreUserImage,
            @Part("UserCompanyName") TypedString UserCompanyName,
            @Part("UserService") TypedString UserService,
            @Part("UserFullName") TypedString UserFullName,
            @Part("UserHouseName") TypedString UserHouseName,
            @Part("UserGender") TypedString UserGender,
            @Part("UserAddress") TypedString UserAddress,
            @Part("UserCity") TypedString UserCity,
            @Part("UserPin") TypedString UserPin,
            @Part("UserLandLine") TypedString UserLandLine,
            @Part("UserMobile") TypedString UserMobile,
            @Part("UserEmail") TypedString UserEmail,
            @Part("UserDescription") TypedString UserDescription,
            Callback<ProfileUpdate> response
    );

    @FormUrlEncoded
    @POST("/user/passwordchange/app")
    void changePassword(
            @Field("Token") String Token,
            @Field("PKUserID") int PKUserID,
            @Field("CurrentPassword") String CurrentPassword,
            @Field("NewPassword") String NewPassword,
            Callback<PasswordChange> response
    );

    @FormUrlEncoded
    @POST("/user/chatmsg/getkey/app")
    void getServerKey(
            @Field("Token") String token,
            Callback<ServerKey> response
    );

    @FormUrlEncoded
    @POST("/user/chatmsg/send/app")
    void chatmsgsend(
            @Field("Token") String token,
            @Field("MessageType") int MessageType,
            @Field("MessageText") String MessageText,
            Callback<ChatMsgSend> response
    );

    @Multipart
    @POST("/user/chatmsg/send/app")
    void chatfilesend(
            @Part("Token") TypedString token,
            @Part("MessageType") TypedString MessageType,
            @Part("MessageText") TypedString  MessageText,
            @Part("MessageFile") TypedFile MessageFile,
            Callback<ChatMsgSend> response
    );

    @FormUrlEncoded
    @POST("/user/chatmsg/get/app")
    void chatmsgsget(
            @Field("Token") String token,
            @Field("MaxID") int MaxID,
            @Field("MinID") int MinID,
            @Field("UpDown") int UpDown,
            Callback<ChatMsgsGet> response
    );

    @FormUrlEncoded
    @POST("/admin/users/get/app")
    void contactsget(
            @Field("Token") String token,
            @Field("MaxID") int MaxID,
            Callback<ContactsList> response
    );

    @FormUrlEncoded
    @POST("/admin/users/approve/profile/app")
    void profileApproval(
            @Field("Token") String token,
            @Field("UserID") String userId,
            @Field("Approve") int approve,
            Callback<Approval> response
    );

    @FormUrlEncoded
    @POST("/admin/users/approve/delete/app")
    void profileDelete(
            @Field("Token") String token,
            @Field("UserID") String userId,
            @Field("Approve") int approve,
            Callback<Approval> response
    );

    @FormUrlEncoded
    @POST("/admin/users/approve/message/app")
    void messagingApproval(
            @Field("Token") String token,
            @Field("UserID") String userId,
            @Field("Approve") int approve,
            Callback<Approval> response
    );

    @FormUrlEncoded
    @POST("/logout/app")
    void logout(
            @Field("Token") String Token,
            Callback<Logout> response
    );
}
