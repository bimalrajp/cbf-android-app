package com.blidus.catholicbusinessforum.holders;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.blidus.catholicbusinessforum.R;

/**
 * Created by Bimal on 15-02-2017.
 */

public class MessageHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public CardView MsgCardView;
    public TextView UserFullName;
    public TextView UserCompanyName;
    public FrameLayout FileContent;
    public ImageView MessageType;
    public LinearLayout MessageVideoCover;
    public VideoView MessageVideo;
    public ImageView PlayButton;
    public ImageView DownloadStatus;
    public TextView MessageText;
    public LinearLayout MyOnlineDot;

    public MessageHolder(View itemView) {
        super(itemView);
        MsgCardView = (CardView) itemView.findViewById(R.id.MsgCardView);
        UserFullName = (TextView) itemView.findViewById(R.id.txt_user_name);
        UserCompanyName = (TextView) itemView.findViewById(R.id.txt_user_company);
        FileContent = (FrameLayout) itemView.findViewById(R.id.fl_file_msg);
        MessageVideoCover = (LinearLayout) itemView.findViewById(R.id.ll_vid_msg_cover);
        MessageType = (ImageView) itemView.findViewById(R.id.img_msg);
        MessageVideo = (VideoView) itemView.findViewById(R.id.vid_msg);
        PlayButton = (ImageView) itemView.findViewById(R.id.img_play_btn);
        DownloadStatus = (ImageView) itemView.findViewById(R.id.img_download_status);
        MessageText = (TextView) itemView.findViewById(R.id.txt_message);
        MyOnlineDot = (LinearLayout) itemView.findViewById(R.id.MyOnlineDot);

        //DownloadStatus.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        //Snackbar.make(v,"Clicked button at position: " + getAdapterPosition(),Snackbar.LENGTH_LONG).show();
    }
}
