package com.blidus.catholicbusinessforum.holders;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.blidus.catholicbusinessforum.R;

/**
 * Created by Bimal on 16-02-2017.
 */

public class ContactHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public CardView ContactCardView;
    public LinearLayout UserSection;
    public ImageView ContactImage;
    public TextView UserFullName;
    public TextView UserCompanyName;
    public TextView ContactDescreption;
    public LinearLayout ContactOnlineDot;
    public LinearLayout UserFlags;
    public Switch ProfileSwitch, MessagingSwitch;

    public ContactHolder(View itemView) {
        super(itemView);
        ContactCardView = (CardView) itemView.findViewById(R.id.ContactCardView);
        UserSection = (LinearLayout) itemView.findViewById(R.id.ll_user);
        ContactImage = (ImageView) itemView.findViewById(R.id.img_user);
        UserFullName = (TextView) itemView.findViewById(R.id.txt_user_name);
        ContactOnlineDot = (LinearLayout) itemView.findViewById(R.id.ContactOnlineDot);
        UserCompanyName = (TextView) itemView.findViewById(R.id.txt_user_company);
        UserFlags = (LinearLayout) itemView.findViewById(R.id.ll_user_flags);
        ProfileSwitch = (Switch) itemView.findViewById(R.id.swh_profile);
        MessagingSwitch = (Switch) itemView.findViewById(R.id.swh_message);
        ContactDescreption = (TextView) itemView.findViewById(R.id.txt_user_desc);
    }
    @Override
    public void onClick(View v) {

    }
}
