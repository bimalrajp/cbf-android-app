package com.blidus.catholicbusinessforum.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;

import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blidus.catholicbusinessforum.App;
import com.blidus.catholicbusinessforum.R;
import com.blidus.catholicbusinessforum.global.Constants;
import com.blidus.catholicbusinessforum.global.PreferenceKeys;

public class DevelopersActivity extends AppCompatActivity implements View.OnClickListener {

	private int colorPrimary, colorAccent, colorBackgroundImage;

	private Intent facebookIntent;

	private static String[] Developer_FB_Profile_Website_Links = {"https://www.facebook.com/joeabrahamm", "https://www.facebook.com/anukbiju", "https://www.facebook.com/Bimalrajp", "https://www.facebook.com/ljishere", "https://www.facebook.com/SHINZ.THOMAS"};
	private static String[] Developer_FB_Profile_App_Links = {"fb://profile/669401802", "fb://profile/100002201329516", "fb://profile/100000632458852", "fb://profile/524108373", "fb://profile/100003093429638"};

	private LinearLayout ll_actionbar;

	private TextView txt_company_name;
	private GradientDrawable txt_company_name_background;

	private TextView txt_developers_title;
	private LinearLayout ll_developer_1, ll_developer_2, ll_developer_3, ll_developer_4, ll_developer_5;
	private TextView txt_developer_1_name, txt_developer_2_name, txt_developer_3_name, txt_developer_4_name, txt_developer_5_name;
	private ImageView img_dev_1_arrow, img_dev_2_arrow, img_dev_3_arrow, img_dev_4_arrow, img_dev_5_arrow;
	private LinearLayout ll_developer_1_social_links, ll_developer_2_social_links, ll_developer_3_social_links, ll_developer_4_social_links, ll_developer_5_social_links;
	private ImageView img_dev_1_social_link_1, img_dev_2_social_link_1, img_dev_3_social_link_1, img_dev_4_social_link_1, img_dev_5_social_link_1;
	private static boolean dev_1_active = Constants.DEVELOPERS.DEV_FLAG_1, dev_2_active = Constants.DEVELOPERS.DEV_FLAG_2, dev_3_active = Constants.DEVELOPERS.DEV_FLAG_3, dev_4_active = Constants.DEVELOPERS.DEV_FLAG_4, dev_5_active = Constants.DEVELOPERS.DEV_FLAG_5;
	private boolean dev_1_clicked = false, dev_2_clicked = false, dev_3_clicked = false, dev_4_clicked = false, dev_5_clicked = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_developers);
		if (App.preferences.getBoolean(PreferenceKeys.USER_DEFINED_THEME_COLOR, false)) {
			colorPrimary = Color.parseColor(App.preferences.getString(PreferenceKeys.USER_DEFINED_THEME_COLOR_PRIMARY, null));
			colorAccent = Color.parseColor(App.preferences.getString(PreferenceKeys.USER_DEFINED_THEME_COLOR_ACCENT, null));
			colorBackgroundImage = Color.parseColor(App.preferences.getString(PreferenceKeys.USER_DEFINED_THEME_COLOR_BACKGROUND_IMAGE, null));
		} else {
			colorPrimary = ContextCompat.getColor(this, R.color.colorPrimary);
			colorAccent = ContextCompat.getColor(this, R.color.colorAccent);
			colorBackgroundImage = ContextCompat.getColor(this, R.color.colorBackgroundImage);
		}
		configView();
	}

	private void configView() {

		Log.v("CBF", "Developer called");

		ll_actionbar = (LinearLayout) findViewById(R.id.ll_actionbar);

		txt_company_name = (TextView) findViewById(R.id.txt_company_name);
		txt_company_name_background = (GradientDrawable) txt_company_name.getBackground();

		txt_developers_title = (TextView) findViewById(R.id.txt_developers_title);
		ll_developer_1 = (LinearLayout) findViewById(R.id.ll_developer_1);
		ll_developer_2 = (LinearLayout) findViewById(R.id.ll_developer_2);
		ll_developer_3 = (LinearLayout) findViewById(R.id.ll_developer_3);
		ll_developer_4 = (LinearLayout) findViewById(R.id.ll_developer_4);
		ll_developer_5 = (LinearLayout) findViewById(R.id.ll_developer_5);
		txt_developer_1_name = (TextView) findViewById(R.id.txt_developer_1_name);
		txt_developer_2_name = (TextView) findViewById(R.id.txt_developer_2_name);
		txt_developer_3_name = (TextView) findViewById(R.id.txt_developer_3_name);
		txt_developer_4_name = (TextView) findViewById(R.id.txt_developer_4_name);
		txt_developer_5_name = (TextView) findViewById(R.id.txt_developer_5_name);
		img_dev_1_arrow = (ImageView) findViewById(R.id.img_dev_1_arrow);
		img_dev_2_arrow = (ImageView) findViewById(R.id.img_dev_2_arrow);
		img_dev_3_arrow = (ImageView) findViewById(R.id.img_dev_3_arrow);
		img_dev_4_arrow = (ImageView) findViewById(R.id.img_dev_4_arrow);
		img_dev_5_arrow = (ImageView) findViewById(R.id.img_dev_5_arrow);
		ll_developer_1_social_links = (LinearLayout) findViewById(R.id.ll_developer_1_social_links);
		ll_developer_2_social_links = (LinearLayout) findViewById(R.id.ll_developer_2_social_links);
		ll_developer_3_social_links = (LinearLayout) findViewById(R.id.ll_developer_3_social_links);
		ll_developer_4_social_links = (LinearLayout) findViewById(R.id.ll_developer_4_social_links);
		ll_developer_5_social_links = (LinearLayout) findViewById(R.id.ll_developer_5_social_links);
		img_dev_1_social_link_1 = (ImageView) findViewById(R.id.img_dev_1_social_link_1);
		img_dev_2_social_link_1 = (ImageView) findViewById(R.id.img_dev_2_social_link_1);
		img_dev_3_social_link_1 = (ImageView) findViewById(R.id.img_dev_3_social_link_1);
		img_dev_4_social_link_1 = (ImageView) findViewById(R.id.img_dev_4_social_link_1);
		img_dev_5_social_link_1 = (ImageView) findViewById(R.id.img_dev_5_social_link_1);

		if (dev_1_active) {
			img_dev_1_arrow.setVisibility(View.VISIBLE);
		} else {
			img_dev_1_arrow.setVisibility(View.GONE);
		}
		if (dev_2_active) {
			img_dev_2_arrow.setVisibility(View.VISIBLE);
		} else {
			img_dev_2_arrow.setVisibility(View.GONE);
		}
		if (dev_3_active) {
			img_dev_3_arrow.setVisibility(View.VISIBLE);
		} else {
			img_dev_3_arrow.setVisibility(View.GONE);
		}
		if (dev_4_active) {
			img_dev_4_arrow.setVisibility(View.VISIBLE);
		} else {
			img_dev_4_arrow.setVisibility(View.GONE);
		}
		if (dev_5_active) {
			img_dev_5_arrow.setVisibility(View.VISIBLE);
		} else {
			img_dev_5_arrow.setVisibility(View.GONE);
		}

		setColorToElements();

		txt_company_name.setOnClickListener(this);

		ll_developer_1.setOnClickListener(this);
		ll_developer_2.setOnClickListener(this);
		ll_developer_3.setOnClickListener(this);
		ll_developer_4.setOnClickListener(this);
		ll_developer_5.setOnClickListener(this);

		img_dev_1_social_link_1.setOnClickListener(this);
		img_dev_2_social_link_1.setOnClickListener(this);
		img_dev_3_social_link_1.setOnClickListener(this);
		img_dev_4_social_link_1.setOnClickListener(this);
		img_dev_5_social_link_1.setOnClickListener(this);

	}

	private void setColorToElements() {

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.setStatusBarColor(colorPrimary);
		}

		ll_actionbar.setBackgroundColor(colorPrimary);

		txt_company_name.setTextColor(colorPrimary);
		txt_company_name_background.setColor(ContextCompat.getColor(this, R.color.colorWhite));
		txt_company_name_background.setStroke(3, colorPrimary);

		txt_developers_title.setTextColor(colorPrimary);
		txt_developer_1_name.setTextColor(colorPrimary);
		txt_developer_2_name.setTextColor(colorPrimary);
		txt_developer_3_name.setTextColor(colorPrimary);
		txt_developer_4_name.setTextColor(colorPrimary);
		txt_developer_5_name.setTextColor(colorPrimary);
		changeDrawableColor(img_dev_1_arrow, colorPrimary);
		changeDrawableColor(img_dev_2_arrow, colorPrimary);
		changeDrawableColor(img_dev_3_arrow, colorPrimary);
		changeDrawableColor(img_dev_4_arrow, colorPrimary);
		changeDrawableColor(img_dev_5_arrow, colorPrimary);
	}

	private void changeDrawableColor(ImageView imageView, int color) {
		Drawable d = imageView.getDrawable();
		d.mutate();
		d.setColorFilter(color, PorterDuff.Mode.MULTIPLY);
		imageView.setImageDrawable(d);
	}

	public void menuAnimation(final View v, int position, final boolean direction) {
		int titleHeight = v.getHeight() * position;
		int viewHeight = v.getHeight();
		int totalDistance;
		if (direction) {
			v.setVisibility(View.VISIBLE);
			totalDistance = 0;
		} else {
			totalDistance = -(viewHeight);
			//totalDistance = -(titleHeight + viewHeight);
		}
		v.animate().translationY(totalDistance).setListener(new AnimatorListenerAdapter() {
			@Override
			public void onAnimationEnd(Animator animation) {
				super.onAnimationEnd(animation);
				if (direction) {
					v.setVisibility(View.VISIBLE);
				} else {
					v.setVisibility(View.GONE);
				}
			}
		});
	}

	private void rotate(View v, float degree1, float degree2) {
		final RotateAnimation rotateAnim = new RotateAnimation(degree1, degree2,
				RotateAnimation.RELATIVE_TO_SELF, 0.5f,
				RotateAnimation.RELATIVE_TO_SELF, 0.5f);

		rotateAnim.setDuration(500);
		rotateAnim.setFillAfter(true);
		v.startAnimation(rotateAnim);
	}

	private boolean menuChanged(boolean b, View v1, View v2, int position) {
		int d1, d2;
		if (b) {
			d1 = 90;
			d2 = 0;
		} else {
			d1 = 0;
			d2 = 90;
		}
		b = !b;
		rotate(v2, d1, d2);
		menuAnimation(v1, position, b);
		return b;
	}

	public void changeAllMenus(int a) {
		switch (a) {
			case 1:
				dev_1_clicked = menuChanged(dev_1_clicked, ll_developer_1_social_links, img_dev_1_arrow, 1);
				if (dev_2_clicked) {
					dev_2_clicked = menuChanged(dev_2_clicked, ll_developer_2_social_links, img_dev_2_arrow, 2);
				}
				if (dev_3_clicked) {
					dev_3_clicked = menuChanged(dev_3_clicked, ll_developer_3_social_links, img_dev_3_arrow, 3);
				}
				if (dev_4_clicked) {
					dev_4_clicked = menuChanged(dev_4_clicked, ll_developer_4_social_links, img_dev_4_arrow, 4);
				}
				if (dev_5_clicked) {
					dev_5_clicked = menuChanged(dev_5_clicked, ll_developer_5_social_links, img_dev_5_arrow, 5);
				}
				break;
			case 2:
				if (dev_1_clicked) {
					dev_1_clicked = menuChanged(dev_1_clicked, ll_developer_1_social_links, img_dev_1_arrow, 1);
				}
				dev_2_clicked = menuChanged(dev_2_clicked, ll_developer_2_social_links, img_dev_2_arrow, 2);
				if (dev_3_clicked) {
					dev_3_clicked = menuChanged(dev_3_clicked, ll_developer_3_social_links, img_dev_3_arrow, 3);
				}
				if (dev_4_clicked) {
					dev_4_clicked = menuChanged(dev_4_clicked, ll_developer_4_social_links, img_dev_4_arrow, 4);
				}
				if (dev_5_clicked) {
					dev_5_clicked = menuChanged(dev_5_clicked, ll_developer_5_social_links, img_dev_5_arrow, 5);
				}
				break;
			case 3:
				if (dev_1_clicked) {
					dev_1_clicked = menuChanged(dev_1_clicked, ll_developer_1_social_links, img_dev_1_arrow, 1);
				}
				if (dev_2_clicked) {
					dev_2_clicked = menuChanged(dev_2_clicked, ll_developer_2_social_links, img_dev_2_arrow, 2);
				}
				dev_3_clicked = menuChanged(dev_3_clicked, ll_developer_3_social_links, img_dev_3_arrow, 3);
				if (dev_4_clicked) {
					dev_4_clicked = menuChanged(dev_4_clicked, ll_developer_4_social_links, img_dev_4_arrow, 4);
				}
				if (dev_5_clicked) {
					dev_5_clicked = menuChanged(dev_5_clicked, ll_developer_5_social_links, img_dev_5_arrow, 5);
				}
				break;
			case 4:
				if (dev_1_clicked) {
					dev_1_clicked = menuChanged(dev_1_clicked, ll_developer_1_social_links, img_dev_1_arrow, 1);
				}
				if (dev_2_clicked) {
					dev_2_clicked = menuChanged(dev_2_clicked, ll_developer_2_social_links, img_dev_2_arrow, 2);
				}
				if (dev_3_clicked) {
					dev_3_clicked = menuChanged(dev_3_clicked, ll_developer_3_social_links, img_dev_3_arrow, 3);
				}
				dev_4_clicked = menuChanged(dev_4_clicked, ll_developer_4_social_links, img_dev_4_arrow, 4);
				if (dev_5_clicked) {
					dev_5_clicked = menuChanged(dev_5_clicked, ll_developer_5_social_links, img_dev_5_arrow, 5);
				}
				break;
			case 5:
				if (dev_1_clicked) {
					dev_1_clicked = menuChanged(dev_1_clicked, ll_developer_1_social_links, img_dev_1_arrow, 1);
				}
				if (dev_2_clicked) {
					dev_2_clicked = menuChanged(dev_2_clicked, ll_developer_2_social_links, img_dev_2_arrow, 2);
				}
				if (dev_3_clicked) {
					dev_3_clicked = menuChanged(dev_3_clicked, ll_developer_3_social_links, img_dev_3_arrow, 3);
				}
				if (dev_4_clicked) {
					dev_4_clicked = menuChanged(dev_4_clicked, ll_developer_4_social_links, img_dev_4_arrow, 4);
				}
				dev_5_clicked = menuChanged(dev_5_clicked, ll_developer_5_social_links, img_dev_5_arrow, 5);
				break;
		}
	}

	public static Intent getOpenFacebookIntent(Context context, int developer_position) {
		try {
			context.getPackageManager()
					.getPackageInfo("com.facebook.katana", 0); //Checks if FB is even installed.
			return new Intent(Intent.ACTION_VIEW,
					Uri.parse(Developer_FB_Profile_App_Links[developer_position])); //Trys to make intent with FB's URI
		} catch (Exception e) {
			return new Intent(Intent.ACTION_VIEW,
					Uri.parse(Developer_FB_Profile_Website_Links[developer_position])); //catches and opens a url to the desired page
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

			case R.id.txt_company_name:
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(Constants.HTTP.COMPANY_URL));
				startActivity(i);
				break;

			case R.id.ll_developer_1:
				if (dev_1_active) {
					changeAllMenus(1);
				}
				break;
			case R.id.ll_developer_2:
				if (dev_2_active) {
					changeAllMenus(2);
				}
				break;
			case R.id.ll_developer_3:
				if (dev_3_active) {
					changeAllMenus(3);
				}
				break;
			case R.id.ll_developer_4:
				if (dev_4_active) {
					changeAllMenus(4);
				}
				break;
			case R.id.ll_developer_5:
				if (dev_5_active) {
					changeAllMenus(5);
				}
				break;
			case R.id.img_dev_1_social_link_1:
				if (dev_1_active) {
					facebookIntent = getOpenFacebookIntent(this, 0);
					startActivity(facebookIntent);
				}
				break;
			case R.id.img_dev_2_social_link_1:
				if (dev_2_active) {
					facebookIntent = getOpenFacebookIntent(this, 1);
					startActivity(facebookIntent);
				}
				break;
			case R.id.img_dev_3_social_link_1:
				if (dev_3_active) {
					facebookIntent = getOpenFacebookIntent(this, 2);
					startActivity(facebookIntent);
				}
				break;
			case R.id.img_dev_4_social_link_1:
				if (dev_4_active) {
					facebookIntent = getOpenFacebookIntent(this, 3);
					startActivity(facebookIntent);
				}
				break;
			case R.id.img_dev_5_social_link_1:
				if (dev_5_active) {
					facebookIntent = getOpenFacebookIntent(this, 4);
					startActivity(facebookIntent);
				}
				break;
		}
	}

	@Override
	public void onBackPressed() {
		if (dev_1_clicked) {
			changeAllMenus(1);
			return;
		} else if (dev_2_clicked) {
			changeAllMenus(2);
			return;
		} else if (dev_3_clicked) {
			changeAllMenus(3);
			return;
		} else if (dev_4_clicked) {
			changeAllMenus(4);
			return;
		} else if (dev_5_clicked) {
			changeAllMenus(5);
			return;
		}
		startActivity(new Intent(DevelopersActivity.this, AboutActivity.class));
		finish();
	}
}
