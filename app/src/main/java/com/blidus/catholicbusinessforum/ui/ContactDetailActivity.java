package com.blidus.catholicbusinessforum.ui;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blidus.catholicbusinessforum.App;
import com.blidus.catholicbusinessforum.R;
import com.blidus.catholicbusinessforum.adapters.dbAdapters.ContactDBAdapter;
import com.blidus.catholicbusinessforum.dataModels.dbDatatypes.Contact;
import com.blidus.catholicbusinessforum.global.Constants;
import com.blidus.catholicbusinessforum.global.PreferenceKeys;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;

import java.io.File;

import cz.msebera.android.httpclient.Header;

import static com.blidus.catholicbusinessforum.ui.ContactsActivity.EXTRA_CONTACT_ID;

public class ContactDetailActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String EXTRA_EDIT_CONTACT_ID = "com.blidus.catholicbusinessforum.editContactID";

    private LinearLayout ll_actionbar;
    private ImageView img_background;

    private Runnable runAfterStoragePermissionGrant;
    private Runnable runAfterCallPermissionGrant;
    private Runnable runAfterContactWritePermissionGrant;
    private String userProfilePicPath;
    private AsyncHttpClient http;

    private Intent intent;
    String contactId = null;
    private Contact contact;
    private LinearLayout activity_contact_detail, ll_contact_details, ll_btn_edit_profile, ll_btn_save_contact, ll_contact_image, ll_contact_mobile, ll_contact_email;
    private GradientDrawable ll_contact_details_background, ll_btn_edit_profile_background, ll_btn_save_contact_background, ll_contact_image_background, ll_contact_mobile_background, ll_contact_email_background;
    private ImageView img_contact;
    private TextView txt_contact_fullname, txt_contact_company_name, txt_contact_product_name, txt_contact_house_name, txt_contact_address, txt_contact_city, txt_contact_mobile, txt_contact_email, txt_contact_descreption;
    private Button btn_edit_profile, btn_save_contact, btn_call_contact, btn_sms_contact, btn_send_email;
    private GradientDrawable btn_call_contact_background, btn_sms_contact_background, btn_send_email_background;

    private int colorPrimary, colorAccent, colorBackgroundImage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_detail);
        if (App.preferences.getBoolean(PreferenceKeys.USER_DEFINED_THEME_COLOR, false)) {
            colorPrimary = Color.parseColor(App.preferences.getString(PreferenceKeys.USER_DEFINED_THEME_COLOR_PRIMARY, null));
            colorAccent = Color.parseColor(App.preferences.getString(PreferenceKeys.USER_DEFINED_THEME_COLOR_ACCENT, null));
            colorBackgroundImage = Color.parseColor(App.preferences.getString(PreferenceKeys.USER_DEFINED_THEME_COLOR_BACKGROUND_IMAGE, null));
        } else {
            colorPrimary = ContextCompat.getColor(this, R.color.colorPrimary);
            colorAccent = ContextCompat.getColor(this, R.color.colorAccent);
            colorBackgroundImage = ContextCompat.getColor(this, R.color.colorBackgroundImage);
        }
        configView();
        displayContactFromDb(Integer.parseInt(contactId));
    }

    private void configView() {
        intent = getIntent();
        contactId = intent.getStringExtra(EXTRA_CONTACT_ID);
        //Toast.makeText(this,contactId+"",Toast.LENGTH_SHORT).show();

        ll_actionbar = (LinearLayout) findViewById(R.id.ll_actionbar);
        img_background = (ImageView) findViewById(R.id.img_background);

        activity_contact_detail = (LinearLayout) findViewById(R.id.activity_contact_detail);
        ll_contact_details = (LinearLayout) findViewById(R.id.ll_contact_details);
        ll_contact_details_background = (GradientDrawable) ll_contact_details.getBackground();

        ll_btn_edit_profile = (LinearLayout) findViewById(R.id.ll_btn_edit_profile);
        ll_btn_edit_profile_background = (GradientDrawable) ll_btn_edit_profile.getBackground();
        ll_btn_save_contact = (LinearLayout) findViewById(R.id.ll_btn_save_contact);
        ll_btn_save_contact_background = (GradientDrawable) ll_btn_save_contact.getBackground();
        ll_contact_image = (LinearLayout) findViewById(R.id.ll_contact_image);
        ll_contact_image_background = (GradientDrawable) ll_contact_image.getBackground();
        img_contact = (ImageView) findViewById(R.id.img_contact);
        txt_contact_fullname = (TextView) findViewById(R.id.txt_contact_fullname);
        txt_contact_company_name = (TextView) findViewById(R.id.txt_contact_company_name);
        txt_contact_product_name = (TextView) findViewById(R.id.txt_contact_product_name);
        txt_contact_house_name = (TextView) findViewById(R.id.txt_contact_house_name);
        txt_contact_address = (TextView) findViewById(R.id.txt_contact_address);
        txt_contact_city = (TextView) findViewById(R.id.txt_contact_city);
        ll_contact_mobile = (LinearLayout) findViewById(R.id.ll_contact_mobile);
        ll_contact_mobile_background = (GradientDrawable) ll_contact_mobile.getBackground();
        txt_contact_mobile = (TextView) findViewById(R.id.txt_contact_mobile);
        ll_contact_email = (LinearLayout) findViewById(R.id.ll_contact_email);
        ll_contact_email_background = (GradientDrawable) ll_contact_email.getBackground();
        txt_contact_email = (TextView) findViewById(R.id.txt_contact_email);
        txt_contact_descreption = (TextView) findViewById(R.id.txt_contact_descreption);

        btn_edit_profile = (Button) findViewById(R.id.btn_edit_profile);
        btn_save_contact = (Button) findViewById(R.id.btn_save_contact);
        btn_call_contact = (Button) findViewById(R.id.btn_call_contact);
        btn_call_contact_background = (GradientDrawable) btn_call_contact.getBackground();
        btn_sms_contact = (Button) findViewById(R.id.btn_sms_contact);
        btn_sms_contact_background = (GradientDrawable) btn_sms_contact.getBackground();
        btn_send_email = (Button) findViewById(R.id.btn_send_email);
        btn_send_email_background = (GradientDrawable) btn_send_email.getBackground();

        http = new AsyncHttpClient();

        btn_edit_profile.setOnClickListener(this);
        btn_save_contact.setOnClickListener(this);
        btn_call_contact.setOnClickListener(this);
        btn_sms_contact.setOnClickListener(this);
        btn_send_email.setOnClickListener(this);

        setColorToElements();
    }

    private void setColorToElements() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(colorPrimary);
        }

        ll_actionbar.setBackgroundColor(colorPrimary);
        changeDrawableColor(img_background, colorBackgroundImage);
        ll_contact_details_background.setColor(ContextCompat.getColor(this, R.color.colorWhite));
        ll_contact_details_background.setStroke(3, colorPrimary);
        ll_btn_edit_profile_background.setColor(colorPrimary);
        ll_btn_edit_profile_background.setStroke(3, ContextCompat.getColor(this, R.color.colorWhite));
        ll_btn_save_contact_background.setColor(colorPrimary);
        ll_btn_save_contact_background.setStroke(3, ContextCompat.getColor(this, R.color.colorWhite));
        ll_contact_image_background.setColor(ContextCompat.getColor(this, R.color.colorWhite));
        ll_contact_image_background.setStroke(3, colorPrimary);
        ll_contact_mobile_background.setColor(ContextCompat.getColor(this, R.color.colorWhite));
        ll_contact_mobile_background.setStroke(3, colorPrimary);
        btn_call_contact_background.setColor(colorPrimary);
        btn_sms_contact.setTextColor(colorPrimary);
        btn_sms_contact_background.setColor(ContextCompat.getColor(this, R.color.colorWhite));
        btn_sms_contact_background.setStroke(3, colorPrimary);
        ll_contact_email_background.setColor(ContextCompat.getColor(this, R.color.colorWhite));
        ll_contact_email_background.setStroke(3, colorPrimary);
        btn_send_email_background.setColor(colorPrimary);
    }

    private void changeDrawableColor(ImageView imageView, int color) {
        Drawable d = imageView.getDrawable();
        d.mutate();
        d.setColorFilter(color, PorterDuff.Mode.MULTIPLY);
        imageView.setImageDrawable(d);
    }

    public void displayContactFromDb(int Id) {
        ContactDBAdapter dbAdapter = new ContactDBAdapter(this);
        dbAdapter.open();
        contact = dbAdapter.getContact(Id);
        dbAdapter.close();

        if (Integer.parseInt(contact.getPKUserID()) == App.preferences.getInt(PreferenceKeys.LOGIN_ID, 0)) {
            //ll_btn_edit_profile.setVisibility(View.VISIBLE);
            ll_btn_save_contact.setVisibility(View.GONE);
            btn_call_contact.setVisibility(View.GONE);
            btn_sms_contact.setVisibility(View.GONE);
            btn_send_email.setVisibility(View.GONE);
        }/* else {
            ll_btn_edit_profile.setVisibility(View.GONE);
        }*/
        if (App.preferences.getInt(PreferenceKeys.LOGIN_TYPE, 0) == 1){
            ll_btn_edit_profile.setVisibility(View.VISIBLE);
        }

        if (contact.getUserImage() != null) {
            requestStoragePermission(new Runnable() {
                @Override
                public void run() {
                    File contactImage = new File(Constants.FILES_APP.FOLDER_USER_IMAGE.getAbsolutePath() + "/" + contact.getUserImage());
                    if (contactImage.exists()) {
                        final Bitmap bm = BitmapFactory.decodeFile(contactImage.toString());
                        if (bm != null) runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                img_contact.setImageBitmap(bm);
                            }
                        });
                        userProfilePicPath = fetchProfilePicture(Constants.HTTP.BASE_URL + Constants.FOLDERS_ONLINE.FOLDER_USER_IMAGE + contact.getUserImage());
                        Log.d("UserImage", contact.getUserImage());
                    } else {
                        userProfilePicPath = fetchProfilePicture(Constants.HTTP.BASE_URL + Constants.FOLDERS_ONLINE.FOLDER_USER_IMAGE + contact.getUserImage());
                        Log.d("UserImage", contact.getUserImage());
                    }
                }
            });
        }
        setContactDetails(contact);
    }

    private void setContactDetails(Contact contact) {
        String Value = "Contact Name";
        if (contact.getUserFullName() != "" && !"".equals(contact.getUserFullName()) && contact.getUserFullName() != null) {
            Value = contact.getUserFullName();
        }
        txt_contact_fullname.setText(Value);
        Value = "Company Name";
        if (contact.getCompanyName() != "" && !"".equals(contact.getCompanyName()) && contact.getCompanyName() != null) {
            Value = contact.getCompanyName();
        }
        txt_contact_company_name.setText(Value);
        Value = "Product Name";
        if (contact.getProductName() != "" && !"".equals(contact.getProductName()) && contact.getProductName() != null) {
            Value = contact.getProductName();
        }
        txt_contact_product_name.setText(Value);
        Value = "House Name";
        if (contact.getUserHouseName() != "" && !"".equals(contact.getUserHouseName()) && contact.getUserHouseName() != null) {
            Value = contact.getUserHouseName();
        }
        txt_contact_house_name.setText(Value);
        Value = "Address";
        if (contact.getUserAddress() != "" && !"".equals(contact.getUserAddress()) && contact.getUserAddress() != null) {
            Value = contact.getUserAddress();
        }
        txt_contact_address.setText(Value);
        Value = "City Name";
        if (contact.getUserCity() != "" && !"".equals(contact.getUserCity()) && contact.getUserCity() != null) {
            Value = contact.getUserCity();
        }
        txt_contact_city.setText(Value);
        Value = "Mobile Number";
        if (contact.getUserMobile() != "" && !"".equals(contact.getUserMobile()) && contact.getUserMobile() != null) {
            Value = contact.getUserMobile();
        }
        txt_contact_mobile.setText(Value);
        Value = "Email Address";
        if (contact.getUserEmail() != "" && !"".equals(contact.getUserEmail()) && contact.getUserEmail() != null) {
            Value = contact.getUserEmail();
        }
        txt_contact_email.setText(Value);
        Value = "Contact Description";
        if (contact.getUserDescription() != "" && !"".equals(contact.getUserDescription()) && contact.getUserDescription() != null) {
            Value = contact.getUserDescription();
        }
        txt_contact_descreption.setText(Value);
    }

    private File makeContactImageFile() {
        Constants.FILES_APP.FOLDER_USER_IMAGE.mkdirs();
        File ContactImage = new File(Constants.FILES_APP.FOLDER_USER_IMAGE, contact.getUserImage());
        return ContactImage;
        /*File storageDir = Environment.getExternalStorageDirectory();
        File mediaDir = new File(storageDir, Constants.FOLDER_PATHS_APP.PATH_USER_IMAGE);
        mediaDir.mkdirs();
        Calendar now = Calendar.getInstance();
        File mediaFile = new File(mediaDir, contact.getUserImage());
        return mediaFile;*/
    }

    private String fetchProfilePicture(String url) {
        File ContactImage = makeContactImageFile();
        http.get(url, new FileAsyncHttpResponseHandler(ContactImage) {
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        img_contact.setImageResource(R.drawable.profile);
                        if (Integer.parseInt(contact.getPKUserID()) == App.preferences.getInt(PreferenceKeys.LOGIN_ID, 0)) {
                            ll_btn_edit_profile.setVisibility(View.VISIBLE);
                        } else {
                            if (App.preferences.getInt(PreferenceKeys.LOGIN_TYPE, 0) == 1){
                                ll_btn_edit_profile.setVisibility(View.VISIBLE);
                            } else {
                                ll_btn_edit_profile.setVisibility(View.GONE);
                            }
                        }
                    }
                });
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                final Bitmap bm = BitmapFactory.decodeFile(file.toString());
                if (bm != null) runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        img_contact.setImageBitmap(bm);
                        if (Integer.parseInt(contact.getPKUserID()) == App.preferences.getInt(PreferenceKeys.LOGIN_ID, 0)) {
                            ll_btn_edit_profile.setVisibility(View.VISIBLE);
                        } else {
                            if (App.preferences.getInt(PreferenceKeys.LOGIN_TYPE, 0) == 1){
                                ll_btn_edit_profile.setVisibility(View.VISIBLE);
                            } else {
                                ll_btn_edit_profile.setVisibility(View.GONE);
                            }
                        }
                    }
                });
            }
        });
        return ContactImage.toString();
    }

    private void requestStoragePermission(Runnable runAfterStoragePermissionGrant) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int readStoragePermissionStatus = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
            int writeStoragePermissionStatus = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (readStoragePermissionStatus == PackageManager.PERMISSION_DENIED
                    || writeStoragePermissionStatus == PackageManager.PERMISSION_DENIED) {
                this.runAfterStoragePermissionGrant = runAfterStoragePermissionGrant;
                ActivityCompat.requestPermissions(this, new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE}, Constants.PERMISSION_REQUESTS.REQUEST_FILE_PERMISSIONS);
            } else {
                runAfterStoragePermissionGrant.run();
            }
        } else {
            runAfterStoragePermissionGrant.run();
        }
    }

    private void requestCallPermission(Runnable runAfterCallPermissionGrant) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int perm = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);
            if (perm == PackageManager.PERMISSION_DENIED) {
                this.runAfterCallPermissionGrant = runAfterCallPermissionGrant;
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, Constants.PERMISSION_REQUESTS.REQUEST_CALL_PERMISSION);
            } else {
                runAfterCallPermissionGrant.run();
            }
        } else {
            runAfterCallPermissionGrant.run();
        }
    }

    private void requestContactWritePermission(Runnable runAfterContactWritePermissionGrant) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int readContactPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS);
            int writeContactPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_CONTACTS);
            if (readContactPermission == PackageManager.PERMISSION_DENIED || writeContactPermission == PackageManager.PERMISSION_DENIED) {
                this.runAfterContactWritePermissionGrant = runAfterContactWritePermissionGrant;
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_CONTACTS, Manifest.permission.READ_CONTACTS}, Constants.PERMISSION_REQUESTS.REQUEST_CONTACT_WRITE_PERMISSION);
            } else {
                runAfterContactWritePermissionGrant.run();
            }
        } else {
            runAfterContactWritePermissionGrant.run();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constants.PERMISSION_REQUESTS.REQUEST_CALL_PERMISSION: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    this.runAfterCallPermissionGrant.run();
                }
                break;
            }
            case Constants.PERMISSION_REQUESTS.REQUEST_CONTACT_WRITE_PERMISSION: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    this.runAfterContactWritePermissionGrant.run();
                }
                break;
            }
            case Constants.PERMISSION_REQUESTS.REQUEST_FILE_PERMISSIONS: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    this.runAfterStoragePermissionGrant.run();
                } else {
                    img_contact.setImageResource(R.drawable.profile);
                }
                break;
            }
        }

    }

    public void sendUserSMS(String mobileNumber, String messageText) {
        startActivity(new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:" + mobileNumber)).putExtra("sms_body", messageText));
    }

    public void sendAutoSMS(String mobileNumber, String messageText) {
        SmsManager smsmanager = SmsManager.getDefault();
        smsmanager.sendTextMessage(mobileNumber, null, messageText, null, null);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_edit_profile:
                startActivity(new Intent(this, ProfileEditActivity.class).putExtra(EXTRA_EDIT_CONTACT_ID, Integer.parseInt(contact.getPKUserID())));
                finish();
                break;
            case R.id.btn_save_contact:
                if (contact.getUserMobile() != "" && !"".equals(contact.getUserMobile()) && contact.getUserMobile() != null) {
                    requestContactWritePermission(new Runnable() {
                        @Override
                        public void run() {
                            Intent contactIntent = new Intent(ContactsContract.Intents.SHOW_OR_CREATE_CONTACT, ContactsContract.Contacts.CONTENT_URI);
                            contactIntent.setData(Uri.parse("tel:" + contact.getUserMobile()));
                            contactIntent.putExtra(ContactsContract.Intents.Insert.NAME, contact.getUserFullName());
                            if (contact.getUserEmail() != null)
                                contactIntent.putExtra(ContactsContract.Intents.Insert.EMAIL, contact.getUserEmail());
                            startActivity(contactIntent);
                        }
                    });
                } else {
                    Snackbar.make(activity_contact_detail, "Sorry..!! The User didn't provide a Mobile Number..!!", Snackbar.LENGTH_SHORT).show();
                }
                break;
            case R.id.btn_call_contact:
                if (contact.getUserMobile() != "" && !"".equals(contact.getUserMobile()) && contact.getUserMobile() != null) {
                    requestCallPermission(new Runnable() {
                        @Override
                        public void run() {
                            Intent i = new Intent(Intent.ACTION_CALL);
                            i.setData(Uri.parse("tel:" + contact.getUserMobile()));
                            //noinspection ResourceType
                            startActivity(i);
                        }
                    });
                } else {
                    Snackbar.make(activity_contact_detail, "Sorry..!! The User didn't provide a Mobile Number..!!", Snackbar.LENGTH_SHORT).show();
                }
                break;
            case R.id.btn_sms_contact:
                if (contact.getUserMobile() != "" && !"".equals(contact.getUserMobile()) && contact.getUserMobile() != null) {
                    startActivity(new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:" + contact.getUserMobile())).putExtra("sms_body", ""));
                } else {
                    Snackbar.make(activity_contact_detail, "Sorry..!! The User didn't provide a Mobile Number..!!", Snackbar.LENGTH_SHORT).show();
                }
                break;
            case R.id.btn_send_email:
                if (contact.getUserEmail() != "" && !"".equals(contact.getUserEmail()) && contact.getUserEmail() != null) {
                    Intent i = new Intent();
                    i.setData(Uri.parse("mailto:" + contact.getUserEmail()));
                    startActivity(i);
                } else {
                    Snackbar.make(activity_contact_detail, "Sorry..!! The User didn't provide an Email Address..!!", Snackbar.LENGTH_SHORT).show();
                    //Toast.makeText(this, "Sorry..!! The User did not provide an Email Address..!!", Toast.LENGTH_SHORT).show();
                }
                break;

        }
    }

    @Override
    public void onBackPressed() {
        if (Integer.parseInt(contact.getPKUserID()) == App.preferences.getInt(PreferenceKeys.LOGIN_ID, 0)) {
            startActivity(new Intent(this, MainActivity.class));
        }
        finish();
    }
}
