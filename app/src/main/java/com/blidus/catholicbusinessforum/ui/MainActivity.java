package com.blidus.catholicbusinessforum.ui;

import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.blidus.catholicbusinessforum.App;
import com.blidus.catholicbusinessforum.R;
import com.blidus.catholicbusinessforum.adapters.dbAdapters.ContactDBAdapter;
import com.blidus.catholicbusinessforum.adapters.dbAdapters.MessageDbAdapter;
import com.blidus.catholicbusinessforum.adapters.uiAdapters.AdAdapter;
import com.blidus.catholicbusinessforum.apiCallbacks.ChatMsgsGet;
import com.blidus.catholicbusinessforum.apiCallbacks.ContactsList;
import com.blidus.catholicbusinessforum.apiCallbacks.Logout;
import com.blidus.catholicbusinessforum.controllers.dbControllers.MessageDbController;
import com.blidus.catholicbusinessforum.controllers.uiControllers.AadController;
import com.blidus.catholicbusinessforum.dataModels.callbackDatatypes.ChatMsgItem;
import com.blidus.catholicbusinessforum.global.Constants;
import com.blidus.catholicbusinessforum.global.PreferenceKeys;
import com.blidus.catholicbusinessforum.helpers.DrawerToggle;
import com.blidus.catholicbusinessforum.services.nativeServices.MessageFileDownloadService;
import com.blidus.catholicbusinessforum.services.nativeServices.loadOnlineContactService;
import com.blidus.catholicbusinessforum.services.nativeServices.loadOnlineMessageService;
import com.blidus.catholicbusinessforum.services.nativeServices.loadOnlineMessageService.MessageBinder;
import com.blidus.catholicbusinessforum.services.nativeServices.loadOnlineContactService.ContactBinder;

import java.lang.ref.WeakReference;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.blidus.catholicbusinessforum.ui.ContactsActivity.EXTRA_CONTACT_ID;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static WeakReference<MainActivity> mainactvity;

    private DisplayMetrics displayMetrics;
    private float screenWidthInDp;
    private float screenHeightInDp;

    public static final String EXTRA_PRE_MAX_POS = "com.blidus.catholicbusinessforum.preMaxId";
    public static final String EXTRA_MESSAGE_TYPE = "com.blidus.catholicbusinessforum.msgType";
    public static final String EXTRA_FROM_MAIN = "com.blidus.catholicbusinessforum.fromMain";
    public static final String EXTRA_NEW_MESSAGES = "com.blidus.catholicbusinessforum.newMessages";
    public static final String EXTRA_MESSAGE_LOAD_DIRECTION = "com.blidus.catholicbusinessforum.messageLoadDirection";

    private SharedPreferences preferences;
    private static int maxID = 0, minID = 0, newMsgs = 0, lastPosition = 20;

    private DrawerLayout view_drawer_layout;
    private DrawerToggle view_drawer_toggle;
    private TextView txt_app_title;
    private ImageView btn_easy_logout;

    private LinearLayout ll_actionbar;
    private ImageView img_background;
    private LinearLayout sb_btn_profile, sb_btn_contacts, sb_btn_messages, sb_btn_settings, sb_btn_about, sb_btn_logout;
    private ImageView sb_img_profile, sb_img_contacts, sb_img_messages, sb_img_settings, sb_img_about, sb_img_logout;
    private TextView sb_txt_profile, sb_txt_contacts, sb_txt_messages, sb_txt_settings, sb_txt_about, sb_txt_logout;
    private LinearLayout ll_user_messaging_status;
    private LinearLayout ll_refresh_button;
    private ProgressBar pb_refresh;
    /*private ImageView img_pb_refresh;
    private TableLayout tl_chat_essentials;
    private TableRow tr_1, tr_2, tr_3;*/
    private LinearLayout ll_chat_essentials;
    private LinearLayout btn_new_note, btn_image, btn_video, btn_audio, btn_attach;
    private ImageView img_new_note, img_image, img_video, img_audio, img_attach;
    private TextView txt_new_note, txt_image, txt_video, txt_audio, txt_attach;

    private LinearLayout ll_main_items, btn_new_msgs, btn_contacts;
    private ImageView img_new_msgs, img_contacts;
    private TextView no_of_new_msgs, no_of_contacts, txt_new_msgs, txt_contacts;
    private ProgressBar pb_messages, pb_contacts;
    //private ImageView img_pb_messages, img_pb_contacts;

    private ImageView img_main_sponser;
    private boolean autoslide = false;
    private FrameLayout fl_aad_view;
    private ViewPager viewPager;
    private AdAdapter aadAdapter;
    private AadController aadController;

    private AlertDialog.Builder builderLogout;
    private AlertDialog alertDialogLogout;
    private Handler handler;

    /*Thread contactsLoadThread = new Thread(new Runnable() {
        @Override
        public void run() {
            cntLoadFromOnlineDb(1);
            retrieveDbCntData();
        }
    });
    Thread messagesLoadThread = new Thread(new Runnable() {
        @Override
        public void run() {
            msgLoadFromOnlineDb(1);
            retrieveDbMsgData();
        }
    });*/

    private boolean closeOnBackPress = false;

    private int colorPrimary, colorAccent, colorBackgroundImage;


    private loadOnlineMessageService messageService;
    private boolean isMessageServiceBound = false;
    private static boolean messageFunctionCall = false;

    private ServiceConnection messageServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            MessageBinder binder = (MessageBinder) service;
            messageService = binder.getService();
            isMessageServiceBound = true;
            messageFunctionCall = messageService.msgLoadFromOnlineDb(1, sb_btn_messages, ll_user_messaging_status, ll_chat_essentials, btn_new_msgs, no_of_new_msgs, pb_messages);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            isMessageServiceBound = false;
        }
    };

    private loadOnlineContactService contactService;
    private boolean isContactServiceBound = false;
    private static boolean contactFunctionCall = false;

    private ServiceConnection contactServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            ContactBinder binder = (ContactBinder) service;
            contactService = binder.getService();
            isContactServiceBound = true;
            if (App.preferences.getInt(PreferenceKeys.FIRST_TIME_MAIN_ACTIVITY, 0) == 0) {
                contactFunctionCall = contactService.cntLoadFromOnlineDb(0, no_of_contacts, pb_contacts);
                App.preferences.edit()
                        .putInt(PreferenceKeys.FIRST_TIME_MAIN_ACTIVITY, 1)
                        .apply();
            } else {
                contactFunctionCall = contactService.cntLoadFromOnlineDb(1, no_of_contacts, pb_contacts);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            isContactServiceBound = false;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mainactvity = new WeakReference<MainActivity>(this);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        if (App.preferences.getBoolean(PreferenceKeys.USER_DEFINED_THEME_COLOR, false)) {
            colorPrimary = Color.parseColor(App.preferences.getString(PreferenceKeys.USER_DEFINED_THEME_COLOR_PRIMARY, null));
            colorAccent = Color.parseColor(App.preferences.getString(PreferenceKeys.USER_DEFINED_THEME_COLOR_ACCENT, null));
            colorBackgroundImage = Color.parseColor(App.preferences.getString(PreferenceKeys.USER_DEFINED_THEME_COLOR_BACKGROUND_IMAGE, null));
        } else {
            colorPrimary = ContextCompat.getColor(this, R.color.colorPrimary);
            colorAccent = ContextCompat.getColor(this, R.color.colorAccent);
            colorBackgroundImage = ContextCompat.getColor(this, R.color.colorBackgroundImage);
        }
        configView();

        try {
            no_of_new_msgs.setText(String.valueOf(App.preferences.getInt(PreferenceKeys.NEW_MSG_COUNT, 0)));
            no_of_contacts.setText(String.valueOf(App.preferences.getInt(PreferenceKeys.TOTAL_CNT_COUNT, 0)));
        } catch (Exception e){
            e.printStackTrace();
        }


        Intent contactLoadServiceIntent = new Intent(this, loadOnlineContactService.class);
        bindService(contactLoadServiceIntent, contactServiceConnection, Context.BIND_AUTO_CREATE);
        Intent messageLoadServiceIntent = new Intent(this, loadOnlineMessageService.class);
        bindService(messageLoadServiceIntent, messageServiceConnection, Context.BIND_AUTO_CREATE);


        //contactsLoadThread.run();
        //no_of_contacts.setText("Updating");
        //pb_contacts.setVisibility(View.VISIBLE);
        configMessagingView(App.preferences.getInt(PreferenceKeys.MESSAGE_ACTIVE, PreferenceKeys.FALSE) == 1);
        //messagesLoadThread.run();
        view_drawer_layout.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                view_drawer_toggle.setDrawerSlideProgress(slideOffset);
//                ViewCompat.setTranslationX(sv_root, 120 * getResources().getDisplayMetrics().density * slideOffset);
            }

            @Override
            public void onDrawerOpened(View drawerView) {

            }

            @Override
            public void onDrawerClosed(View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        //messagesLoadThread.run();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isMessageServiceBound) {
            unbindService(messageServiceConnection);
            isMessageServiceBound = false;
        }
        if (isContactServiceBound) {
            unbindService(contactServiceConnection);
            isContactServiceBound = false;
        }
    }

    private void configView() {

        displayMetrics = getResources().getDisplayMetrics();
        screenWidthInDp = displayMetrics.widthPixels / displayMetrics.density;
        screenHeightInDp = displayMetrics.heightPixels / displayMetrics.density;

        builderLogout = new AlertDialog.Builder(this);
        builderLogout.setTitle("Logout !!");
        builderLogout.setMessage("Do you want to logout?");
        builderLogout.setCancelable(false);
        builderLogout.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String Token = App.preferences.getString(PreferenceKeys.TOKEN, null);
                App.preferences.edit()
                        .putInt(PreferenceKeys.LOGIN, PreferenceKeys.FALSE)
                        .putInt(PreferenceKeys.FIRST_TIME_MAIN_ACTIVITY, 0)
                        .putString(PreferenceKeys.TOKEN, null)
                        .putInt(PreferenceKeys.LOGIN_ID, PreferenceKeys.FALSE)
                        .putInt(PreferenceKeys.LOGIN_TYPE, PreferenceKeys.FALSE)
                        .putInt(PreferenceKeys.PROFILE_ACTIVE, PreferenceKeys.FALSE)
                        .putInt(PreferenceKeys.MESSAGE_ACTIVE, PreferenceKeys.FALSE)
                        .apply();
                App.api.logout(Token, new Callback<Logout>() {
                    @Override
                    public void success(Logout logout, Response response) {
                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                finish();
            }
        });
        builderLogout.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        handler = new Handler();

        ll_actionbar = (LinearLayout) findViewById(R.id.ll_actionbar);
        img_background = (ImageView) findViewById(R.id.img_background);

        view_drawer_layout = (DrawerLayout) findViewById(R.id.view_drawer_layout);
        view_drawer_toggle = (DrawerToggle) findViewById(R.id.view_drawer_toggle);

        txt_app_title = (TextView) findViewById(R.id.txt_app_title);
        btn_easy_logout = (ImageView) findViewById(R.id.btn_easy_logout);

        sb_btn_profile = (LinearLayout) findViewById(R.id.sb_btn_profile);
        sb_btn_contacts = (LinearLayout) findViewById(R.id.sb_btn_contacts);
        sb_btn_messages = (LinearLayout) findViewById(R.id.sb_btn_messages);
        sb_btn_settings = (LinearLayout) findViewById(R.id.sb_btn_settings);
        sb_btn_about = (LinearLayout) findViewById(R.id.sb_btn_about);
        sb_btn_logout = (LinearLayout) findViewById(R.id.sb_btn_logout);

        sb_img_profile = (ImageView) findViewById(R.id.sb_img_profile);
        sb_img_contacts = (ImageView) findViewById(R.id.sb_img_contacts);
        sb_img_messages = (ImageView) findViewById(R.id.sb_img_messages);
        sb_img_settings = (ImageView) findViewById(R.id.sb_img_settings);
        sb_img_about = (ImageView) findViewById(R.id.sb_img_about);
        sb_img_logout = (ImageView) findViewById(R.id.sb_img_logout);

        sb_txt_profile = (TextView) findViewById(R.id.sb_txt_profile);
        sb_txt_contacts = (TextView) findViewById(R.id.sb_txt_contacts);
        sb_txt_messages = (TextView) findViewById(R.id.sb_txt_messages);
        sb_txt_settings = (TextView) findViewById(R.id.sb_txt_settings);
        sb_txt_about = (TextView) findViewById(R.id.sb_txt_about);
        sb_txt_logout = (TextView) findViewById(R.id.sb_txt_logout);

        ll_user_messaging_status = (LinearLayout) findViewById(R.id.ll_user_messaging_status);
        ll_refresh_button = (LinearLayout) findViewById(R.id.ll_refresh_button);
        //img_pb_refresh = (ImageView) findViewById(R.id.img_pb_refresh);
        pb_refresh = (ProgressBar) findViewById(R.id.pb_refresh);

        ll_chat_essentials = (LinearLayout) findViewById(R.id.ll_chat_essentials);

        btn_new_note = (LinearLayout) findViewById(R.id.btn_new_note);
        btn_image = (LinearLayout) findViewById(R.id.btn_image);
        btn_video = (LinearLayout) findViewById(R.id.btn_video);
        btn_audio = (LinearLayout) findViewById(R.id.btn_audio);
        btn_attach = (LinearLayout) findViewById(R.id.btn_attach);

        img_new_note = (ImageView) findViewById(R.id.img_new_note);
        img_image = (ImageView) findViewById(R.id.img_image);
        img_video = (ImageView) findViewById(R.id.img_video);
        img_audio = (ImageView) findViewById(R.id.img_audio);
        img_attach = (ImageView) findViewById(R.id.img_attach);

        txt_new_note = (TextView) findViewById(R.id.txt_new_note);
        txt_image = (TextView) findViewById(R.id.txt_image);
        txt_video = (TextView) findViewById(R.id.txt_video);
        txt_audio = (TextView) findViewById(R.id.txt_audio);
        txt_attach = (TextView) findViewById(R.id.txt_attach);

        ll_main_items = (LinearLayout) findViewById(R.id.ll_main_items);

        btn_new_msgs = (LinearLayout) findViewById(R.id.btn_new_msgs);
        btn_contacts = (LinearLayout) findViewById(R.id.btn_contacts);

        img_new_msgs = (ImageView) findViewById(R.id.img_new_msgs);
        img_contacts = (ImageView) findViewById(R.id.img_contacts);
        no_of_new_msgs = (TextView) findViewById(R.id.no_of_new_msgs);
        no_of_contacts = (TextView) findViewById(R.id.no_of_contacts);
        txt_new_msgs = (TextView) findViewById(R.id.txt_new_msgs);
        txt_contacts = (TextView) findViewById(R.id.txt_contacts);

        /*img_pb_messages = (ImageView) findViewById(R.id.img_pb_messages);
        rotate(img_pb_messages, 0, 360);*/
        pb_messages = (ProgressBar) findViewById(R.id.pb_messages);
        /*img_pb_contacts = (ImageView) findViewById(R.id.img_pb_contacts);
        rotate(img_pb_contacts, 0, 360);*/
        pb_contacts = (ProgressBar) findViewById(R.id.pb_contacts);

        img_main_sponser = (ImageView) findViewById(R.id.img_main_sponser);

        fl_aad_view = (FrameLayout) findViewById(R.id.fl_aad_view);

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                fl_aad_view.setVisibility(View.VISIBLE);
                sliderStart();
                viewPager = (ViewPager) findViewById(R.id.vp_aad_view);
                aadAdapter = new AdAdapter(MainActivity.this);
                viewPager.setAdapter(aadAdapter);
                aadController = new AadController(MainActivity.this);
                aadController.applyViewPagerScrollSpeed(viewPager, 1000);
            }
        }, 3500);

        if (App.preferences.getInt(PreferenceKeys.MESSAGE_ACTIVE, 0) == 1) {
            configMessagingView(true);
        } else {
            configMessagingView(false);
        }

        setColorToElements();

        view_drawer_toggle.setOnClickListener(this);
        btn_easy_logout.setOnClickListener(this);

        sb_btn_profile.setOnClickListener(this);
        sb_btn_contacts.setOnClickListener(this);
        sb_btn_messages.setOnClickListener(this);
        sb_btn_settings.setOnClickListener(this);
        sb_btn_about.setOnClickListener(this);
        sb_btn_logout.setOnClickListener(this);

        ll_refresh_button.setOnClickListener(this);

        btn_new_note.setOnClickListener(this);
        btn_image.setOnClickListener(this);
        btn_video.setOnClickListener(this);
        btn_audio.setOnClickListener(this);
        btn_attach.setOnClickListener(this);

        btn_new_msgs.setOnClickListener(this);
        btn_contacts.setOnClickListener(this);

        img_main_sponser.setOnClickListener(this);

        //no_of_new_msgs.setText(App.preferences.getInt(PreferenceKeys.NEW_MSG_COUNT, 0));
    }

    private void setColorToElements() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(colorPrimary);
        }

        ll_actionbar.setBackgroundColor(colorPrimary);
        changeDrawableColor(img_background, colorBackgroundImage);
        ll_user_messaging_status.setBackgroundColor(colorPrimary);
        ll_chat_essentials.setBackgroundColor(colorPrimary);
        sb_btn_profile.setBackgroundColor(colorAccent);
        sb_btn_contacts.setBackgroundColor(colorAccent);
        sb_btn_messages.setBackgroundColor(colorAccent);
        sb_btn_settings.setBackgroundColor(colorAccent);
        sb_btn_about.setBackgroundColor(colorAccent);
        sb_btn_logout.setBackgroundColor(colorAccent);

        no_of_new_msgs.setTextColor(colorPrimary);
        no_of_contacts.setTextColor(colorPrimary);
        txt_new_msgs.setTextColor(colorPrimary);
        txt_contacts.setTextColor(colorPrimary);

        /*changeDrawableColor(img_pb_messages , colorPrimary);
        changeDrawableColor(img_pb_contacts , colorPrimary);*/

        /*changeDrawableColor(img_pb_refresh, colorPrimary);
        rotate(img_pb_refresh, 0, 360);*/

        //ll_application_color_chosen.setBackgroundColor(colorPrimary);
        changeDrawableColor(img_new_msgs, colorPrimary);
        changeDrawableColor(img_contacts, colorPrimary);

    }

    private void rotate(View v, float degree1, float degree2) {
        final RotateAnimation rotateAnim = new RotateAnimation(degree1, degree2,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f);

        rotateAnim.setDuration(1000);
        rotateAnim.setRepeatCount(Animation.INFINITE);
        rotateAnim.setFillAfter(true);
        v.startAnimation(rotateAnim);
    }

    private void changeDrawableColor(ImageView imageView, int color) {
        Drawable d = imageView.getDrawable();
        d.mutate();
        d.setColorFilter(color, PorterDuff.Mode.MULTIPLY);
        imageView.setImageDrawable(d);
    }

    private void configMessagingView(boolean msgFlag) {
        if (msgFlag) {
            sb_btn_messages.setVisibility(View.VISIBLE);
            ll_user_messaging_status.setVisibility(View.GONE);
            ll_chat_essentials.setVisibility(View.VISIBLE);
            //tl_chat_essentials.setVisibility(View.VISIBLE);
            btn_new_msgs.setVisibility(View.VISIBLE);
            //no_of_new_msgs.setText("Updating");
            //pb_messages.setVisibility(View.VISIBLE);
        } else {
            sb_btn_messages.setVisibility(View.GONE);
            ll_user_messaging_status.setVisibility(View.VISIBLE);
            ll_chat_essentials.setVisibility(View.GONE);
            //tl_chat_essentials.setVisibility(View.GONE);
            btn_new_msgs.setVisibility(View.GONE);
        }
    }

    protected void sliderStart() {
        autoslide = true;

        handler.postDelayed(new Runnable() {
            int page = 0;

            @Override
            public void run() {
                page++;
                if (page == AdAdapter.IMG_COUNT) page = 0;
                viewPager.setCurrentItem(page, true);
                if (autoslide) handler.postDelayed(this, 3000);
            }
        }, 5000);
    }

    @Override
    protected void onStop() {
        autoslide = false;
        super.onStop();
    }

    private void retrieveDbMsgData() {
        int highMsgPos = 0;
        //no_of_new_msgs.setText("Updating");
        //pb_messages.setVisibility(View.VISIBLE);
        MessageDbAdapter msgDbAdapter = new MessageDbAdapter(getBaseContext());
        msgDbAdapter.open();
        maxID = msgDbAdapter.getExtremeMsgIds(MessageDbAdapter.Extremes.MAX);
        minID = msgDbAdapter.getExtremeMsgIds(MessageDbAdapter.Extremes.MIN);
        newMsgs = msgDbAdapter.getMsgsCount(MessageDbAdapter.CountTypes.NEW);
        App.preferences.edit()
                .putInt(PreferenceKeys.NEW_MSG_COUNT, newMsgs)
                .apply();
        no_of_new_msgs.setText(String.valueOf(newMsgs));
        pb_messages.setVisibility(View.GONE);
        if (minID != 0 && maxID != 0) {
            highMsgPos = ((maxID - minID) + 1);
        }
        App.preferences.edit()
                .putInt(PreferenceKeys.LOWEST_MSG_ID, minID)
                .putInt(PreferenceKeys.HIGHEST_MSG_ID, maxID)
                .putInt(PreferenceKeys.HIGHEST_MSG_POS, highMsgPos)
                .apply();
        msgDbAdapter.close();
    }

    private void retrieveDbCntData() {
        int highCntPos = 0;
        ContactDBAdapter cntDbAdapter = new ContactDBAdapter(getBaseContext());
        cntDbAdapter.open();
        maxID = cntDbAdapter.getExtremeCntIds(ContactDBAdapter.Extremes.MAX);
        minID = cntDbAdapter.getExtremeCntIds(ContactDBAdapter.Extremes.MIN);
        if (maxID == 0) {
            //no_of_contacts.setText("Updating");
            //img_pb_contacts.setVisibility(View.VISIBLE);
            //pb_contacts.setVisibility(View.VISIBLE);
        } else {
            no_of_contacts.setText(String.valueOf(maxID));
            //img_pb_contacts.setVisibility(View.GONE);
            pb_contacts.setVisibility(View.GONE);
        }
        if (minID != 0 && maxID != 0) {
            highCntPos = ((maxID - minID) + 1);
        }
        App.preferences.edit()
                .putInt(PreferenceKeys.LOWEST_CNT_ID, minID)
                .putInt(PreferenceKeys.HIGHEST_CNT_ID, maxID)
                .putInt(PreferenceKeys.HIGHEST_CNT_POS, highCntPos)
                .apply();
        cntDbAdapter.close();
    }

    public void msgLoadFromOnlineDb(int direction) {
        App.api.chatmsgsget(App.preferences.getString(PreferenceKeys.TOKEN, null), App.preferences.getInt(PreferenceKeys.HIGHEST_MSG_ID, 0), App.preferences.getInt(PreferenceKeys.LOWEST_MSG_ID, 0), direction, new Callback<ChatMsgsGet>() {
            @Override
            public void success(final ChatMsgsGet chatMsgsGet, Response response) {
                //img_pb_refresh.setVisibility(View.GONE);
                pb_refresh.setVisibility(View.GONE);
                //img_pb_messages.setVisibility(View.GONE);
                pb_messages.setVisibility(View.GONE);
                App.preferences.edit()
                        .putInt(PreferenceKeys.MESSAGE_ACTIVE, chatMsgsGet.UserMessageFlag)
                        .apply();
                configMessagingView(chatMsgsGet.UserMessageFlag == 1);
                if (chatMsgsGet.ErrorFlag == 0) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            no_of_new_msgs.setText(String.valueOf(chatMsgsGet.MessagesCount));
                            if (chatMsgsGet.MessagesCount > 0) {
                                MessageDbAdapter dbAdapter = new MessageDbAdapter(getBaseContext());
                                dbAdapter.open();
                                dbAdapter.writeToDb(chatMsgsGet.Messages, MessageDbAdapter.InsertTypes.NEW);
                                maxID = App.preferences.getInt(PreferenceKeys.HIGHEST_MSG_ID, 0);
                                minID = App.preferences.getInt(PreferenceKeys.LOWEST_MSG_ID, 0);
                                if (minID != 0 && maxID != 0) {
                                    App.preferences.edit()
                                            .putInt(PreferenceKeys.HIGHEST_MSG_POS, ((maxID - minID) + 1))
                                            .putInt(PreferenceKeys.NEW_MSG_COUNT, chatMsgsGet.MessagesCount)
                                            .apply();
                                }
                                dbAdapter.close();
                                /*handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent downloadServiceIntent = new Intent(getApplicationContext(),MessageFileDownloadService.class).putExtra(EXTRA_NEW_MESSAGES, true);
                                        startService(downloadServiceIntent);
                                    }
                                });*/
                            }
                        }
                    });
                }
                if (chatMsgsGet.ErrorFlag == 1) {
                    if (chatMsgsGet.TokenFlag != 1) {
                        if (chatMsgsGet.TokenFlag == 2) {
                            Toast.makeText(MainActivity.this, "Invalid Login! Please Login Again!", Toast.LENGTH_SHORT).show();
                        } else if (chatMsgsGet.TokenFlag == 0) {
                            Toast.makeText(MainActivity.this, "Logged Out due to inactivity! Please Login Again!", Toast.LENGTH_SHORT).show();
                        }
                        App.preferences.edit()
                                .putString(PreferenceKeys.TOKEN, "")
                                .putInt(PreferenceKeys.LOGIN, PreferenceKeys.FALSE)
                                .putInt(PreferenceKeys.LOGIN_TYPE, PreferenceKeys.FALSE)
                                .apply();
                        startActivity(new Intent(MainActivity.this, LoginActivity.class));
                        finish();
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                //img_pb_refresh.setVisibility(View.GONE);
                pb_refresh.setVisibility(View.GONE);
                //img_pb_messages.setVisibility(View.GONE);
                pb_messages.setVisibility(View.GONE);
                Toast.makeText(MainActivity.this, "Can't connect to Server! Please check your Network Connection!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void cntLoadFromOnlineDb(int create) {
        if (create == 1) {
            create = App.preferences.getInt(PreferenceKeys.HIGHEST_CNT_ID, 0);
        }
        App.api.contactsget(App.preferences.getString(PreferenceKeys.TOKEN, null), create, new Callback<ContactsList>() {
            @Override
            public void success(final ContactsList contactsList, Response response) {
                if (contactsList.ErrorFlag == 0) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //img_pb_contacts.setVisibility(View.GONE);
                            pb_contacts.setVisibility(View.GONE);
                            if (contactsList.ContactsCount > 0) {
                                ContactDBAdapter dbAdapter = new ContactDBAdapter(getBaseContext());
                                dbAdapter.open();
                                dbAdapter.writeToDb(contactsList.Contacts);
                                maxID = App.preferences.getInt(PreferenceKeys.HIGHEST_CNT_ID, 0);
                                minID = App.preferences.getInt(PreferenceKeys.LOWEST_CNT_ID, 0);
                                if (minID != 0 && maxID != 0) {
                                    App.preferences.edit()
                                            .putInt(PreferenceKeys.HIGHEST_CNT_POS, ((maxID - minID) + 1))
                                            .putInt(PreferenceKeys.CURRENT_CNT_POS, 0)
                                            .apply();
                                }
                                dbAdapter.close();
                                no_of_contacts.setText(String.valueOf(maxID));
                            }
                        }
                    });
                }
                if (contactsList.ErrorFlag == 1) {
                    if (contactsList.TokenFlag != 1) {
                        if (contactsList.TokenFlag == 2) {
                            Toast.makeText(MainActivity.this, "Invalid Login! Please Login Again!", Toast.LENGTH_SHORT).show();
                        } else if (contactsList.TokenFlag == 0) {
                            Toast.makeText(MainActivity.this, "Logged Out due to inactivity! Please Login Again!", Toast.LENGTH_SHORT).show();
                        }
                        App.preferences.edit()
                                .putString(PreferenceKeys.TOKEN, "")
                                .putInt(PreferenceKeys.LOGIN, PreferenceKeys.FALSE)
                                .putInt(PreferenceKeys.LOGIN_TYPE, PreferenceKeys.FALSE)
                                .apply();
                        startActivity(new Intent(MainActivity.this, LoginActivity.class));
                        finish();
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(MainActivity.this, "Can't connect to Server! Please check your Network Connection!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void downloadMedia(ChatMsgItem[] messages) {
        MessageDbController vc = new MessageDbController();
        for (int i = 0; i < messages.length; i++) {
            vc.message = messages[i];
            int messageType = Integer.parseInt(vc.message.getMessageType());
            String messageFile = vc.message.getMessageFile();
            Intent downloadServiceIntent = new Intent(getApplicationContext(), MessageFileDownloadService.class)
                    .putExtra("MessageType", messageType)
                    .putExtra("MessageType", messageFile);
            startService(downloadServiceIntent);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.view_drawer_toggle:
                if (view_drawer_layout.isDrawerOpen(GravityCompat.START))
                    view_drawer_layout.closeDrawer(GravityCompat.START);
                else
                    view_drawer_layout.openDrawer(GravityCompat.START);
                break;
            case R.id.btn_easy_logout:
                view_drawer_layout.closeDrawer(GravityCompat.START);
                alertDialogLogout = builderLogout.create();
                alertDialogLogout.show();
                break;

            case R.id.sb_btn_profile:
                startActivity(new Intent(this, ContactDetailActivity.class).putExtra(EXTRA_CONTACT_ID, String.valueOf(App.preferences.getInt(PreferenceKeys.LOGIN_ID, 0))));
                view_drawer_layout.closeDrawer(GravityCompat.START);
                finish();
                break;
            case R.id.sb_btn_contacts:
                startActivity(new Intent(MainActivity.this, ContactsActivity.class));
                view_drawer_layout.closeDrawer(GravityCompat.START);
                finish();
                break;
            case R.id.sb_btn_messages:
                startActivity(new Intent(this, ChatActivity.class).putExtra(EXTRA_MESSAGE_TYPE, 0).putExtra(EXTRA_FROM_MAIN, true));
                view_drawer_layout.closeDrawer(GravityCompat.START);
                finish();
                break;
            case R.id.sb_btn_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                view_drawer_layout.closeDrawer(GravityCompat.START);
                finish();
                break;
            case R.id.sb_btn_about:
                startActivity(new Intent(this, AboutActivity.class));
                view_drawer_layout.closeDrawer(GravityCompat.START);
                finish();
                break;
            case R.id.sb_btn_logout:
                view_drawer_layout.closeDrawer(GravityCompat.START);
                String Token = App.preferences.getString(PreferenceKeys.TOKEN, null);
                App.preferences.edit()
                        .putInt(PreferenceKeys.LOGIN, PreferenceKeys.FALSE)
                        .putInt(PreferenceKeys.FIRST_TIME_MAIN_ACTIVITY, 0)
                        .putString(PreferenceKeys.TOKEN, null)
                        .putInt(PreferenceKeys.LOGIN_ID, PreferenceKeys.FALSE)
                        .putInt(PreferenceKeys.LOGIN_TYPE, PreferenceKeys.FALSE)
                        .putInt(PreferenceKeys.PROFILE_ACTIVE, PreferenceKeys.FALSE)
                        .putInt(PreferenceKeys.MESSAGE_ACTIVE, PreferenceKeys.FALSE)
                        .apply();
                App.api.logout(Token, new Callback<Logout>() {
                    @Override
                    public void success(Logout logout, Response response) {
                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                finish();
                break;

            case R.id.ll_refresh_button:
                //img_pb_refresh.setVisibility(View.VISIBLE);
                pb_refresh.setVisibility(View.VISIBLE);
                //msgLoadFromOnlineDb(1);
                try {
                    messageService.msgLoadFromOnlineDb(1, sb_btn_messages, ll_user_messaging_status, ll_chat_essentials, btn_new_msgs, no_of_new_msgs, pb_messages);
                } catch (Exception e){
                    e.printStackTrace();
                }
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        pb_refresh.setVisibility(View.GONE);
                    }
                }, 500);
                break;

            case R.id.btn_new_note:
                startActivity(new Intent(this, ChatActivity.class).putExtra(EXTRA_MESSAGE_TYPE, 1).putExtra(EXTRA_FROM_MAIN, true));
                finish();
                break;
            case R.id.btn_image:
                startActivity(new Intent(this, ChatActivity.class).putExtra(EXTRA_MESSAGE_TYPE, 2).putExtra(EXTRA_FROM_MAIN, true));
                finish();
                break;
            case R.id.btn_video:
                startActivity(new Intent(this, ChatActivity.class).putExtra(EXTRA_MESSAGE_TYPE, 3).putExtra(EXTRA_FROM_MAIN, true));
                finish();
                break;
            case R.id.btn_audio:
                startActivity(new Intent(this, ChatActivity.class).putExtra(EXTRA_MESSAGE_TYPE, 4).putExtra(EXTRA_FROM_MAIN, true));
                finish();
                break;
            case R.id.btn_attach:
                startActivity(new Intent(this, ChatActivity.class).putExtra(EXTRA_MESSAGE_TYPE, 5).putExtra(EXTRA_FROM_MAIN, true));
                finish();
                break;

            case R.id.btn_new_msgs:
                startActivity(new Intent(this, ChatActivity.class).putExtra(EXTRA_FROM_MAIN, true));
                finish();
                break;
            case R.id.btn_contacts:
                startActivity(new Intent(MainActivity.this, ContactsActivity.class));
                finish();
                break;

            case R.id.img_main_sponser:
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(Constants.ADS.AD_SPONSOR));
                startActivity(i);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (!closeOnBackPress) {
            closeOnBackPress = true;
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    closeOnBackPress = false;
                }
            }, 3000);
            Toast.makeText(MainActivity.this, "Press back button again to close", Toast.LENGTH_SHORT).show();
        } else {
            App.preferences.edit()
                    .putInt(PreferenceKeys.FIRST_TIME_MAIN_ACTIVITY, 0)
                    .apply();
            finish();
        }
    }
}
