package com.blidus.catholicbusinessforum.ui;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.PorterDuff;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.media.MediaPlayer;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.blidus.catholicbusinessforum.App;
import com.blidus.catholicbusinessforum.BuildConfig;
import com.blidus.catholicbusinessforum.R;
import com.blidus.catholicbusinessforum.adapters.dbAdapters.MessageDbAdapter;
import com.blidus.catholicbusinessforum.adapters.uiAdapters.MessageAdapter;
import com.blidus.catholicbusinessforum.apiCallbacks.ChatMsgSend;
import com.blidus.catholicbusinessforum.apiCallbacks.ChatMsgsGet;
import com.blidus.catholicbusinessforum.dataModels.dbDatatypes.Message;
import com.blidus.catholicbusinessforum.global.Constants;
import com.blidus.catholicbusinessforum.global.PreferenceKeys;
import com.blidus.catholicbusinessforum.helpers.EndlessRecyclerOnScrollListener;
import com.blidus.catholicbusinessforum.services.nativeServices.MessageFileDownloadService;
import com.blidus.catholicbusinessforum.utils.messageFileUtils;
import com.blidus.catholicbusinessforum.utils.messageNotificationUtils;
import com.google.firebase.messaging.FirebaseMessaging;
import com.loopj.android.http.AsyncHttpClient;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringEscapeUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Calendar;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;

import static com.blidus.catholicbusinessforum.ui.MainActivity.EXTRA_FROM_MAIN;
import static com.blidus.catholicbusinessforum.ui.MainActivity.EXTRA_MESSAGE_TYPE;
import static com.blidus.catholicbusinessforum.ui.MainActivity.EXTRA_NEW_MESSAGES;

public class ChatActivity extends AppCompatActivity implements View.OnClickListener {

    private DisplayMetrics displayMetrics;
    private float screenWidthInDp;
    private float screenHeightInDp;

    private AsyncHttpClient http;

    private static final int ACTIVITY_CAPTURE_IMAGE = 1;
    private static final int ACTIVITY_CHOOSE_IMAGE = 2;
    private static final int ACTIVITY_CAPTURE_VIDEO = 3;
    private static final int ACTIVITY_CHOOSE_VIDEO = 4;
    private static final int ACTIVITY_CAPTURE_AUDIO = 5;
    private static final int ACTIVITY_CHOOSE_AUDIO = 6;
    private static final int ACTIVITY_CHOOSE_FILE = 7;

    private static final String TAG = "IMAGE DETAILS";

    public static final int MAX_IMAGE_DIMENSION = 720;

    private boolean isFileSelected = false;

    private Uri fileUri = null;
    private String filePath = null;
    private String fileName = null;
    private String fileType = null;
    private float fileSize = 0;

    public static WeakReference<ChatActivity> chatactvity;

    private boolean cameraPermissionsSet = false;
    private boolean microphonePermissionsSet = false;
    private boolean filePermissionsSet = false;
    private Runnable runAfterPermissionGrant;
    private File source, cache, destination;

    private Intent intent;

    int finalpmaxpos = 0;
    int temppos = 0;
    int minMsgIdInDb = 0;
    int finalpmaxid = 0;
    int finalpminid = 0;

    private Handler handler;

    private LinearLayout activity_chat;
    private ImageView img_background;
    public RecyclerView messageRecyclerView;
    private ArrayList<Message> messages;
    private ArrayList<Message> loadedmessages;
    private MessageAdapter messageAdapter;

    private boolean fromMain = false;
    private int msgType = 0;

    private LinearLayout ll_actionbar, ll_all_messages, ll_all_messages_blur, ll_message_types, ll_message_form, ll_new_message_entry_box;
    private GradientDrawable ll_message_types_background;
    private FrameLayout fl_file_selector;
    private GradientDrawable fl_file_selector_background;
    private LinearLayout ll_image_chooser, ll_audio_chooser, ll_video_chooser, ll_file_chooser;
    private LinearLayout ll_image_picker, ll_btn_close_image, ll_video_picker, ll_btn_close_video, ll_audio_picker, ll_btn_close_audio, ll_file_picker, ll_btn_close_file;
    private ImageView img_rotate_left, img_rotate_right, img_btn_close_image, img_btn_close_video, img_btn_close_audio, img_btn_close_file;
    private FrameLayout fl_image_preview, fl_video_preview, fl_audio_preview, fl_file_preview;
    private ImageView btn_new_note, btn_new_image, btn_new_video, btn_new_audio, btn_new_attcah;
    private GradientDrawable btn_new_note_background, btn_new_image_background, btn_new_video_background, btn_new_audio_background, btn_new_attcah_background;
    private ImageView img_camera, img_image_gallery, img_camcoder, img_video_gallery, img_microphone, img_audio_gallery, img_file_picker;
    private ImageView img_image_preview, img_audio_preview, img_file_preview;
    private Bitmap msgImageBitmap, scaledBm;
    private FileOutputStream finalOutput = null;
    private VideoView vid_video_preview;
    private MediaController mediaController, messageMediaController;

    private TextView txt_image_preview, txt_video_preview, txt_audio_preview, txt_file_preview;

    private FrameLayout btn_send_message;
    private ImageView img_btn_send_message;
    private RotateAnimation anim;
    private EditText edttxt_new_message;
    private GradientDrawable edttxt_new_message_background;

    private LinearLayout ll_loading_msg;
    private TextView txt_loading_msgs;
    private ProgressBar pb_loading_msgs;

    private InputMethodManager imm;

    private int colorPrimary, colorAccent, colorBackgroundImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        chatactvity = new WeakReference<ChatActivity>(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        if (App.preferences.getBoolean(PreferenceKeys.USER_DEFINED_THEME_COLOR, false)) {
            colorPrimary = Color.parseColor(App.preferences.getString(PreferenceKeys.USER_DEFINED_THEME_COLOR_PRIMARY, null));
            colorAccent = Color.parseColor(App.preferences.getString(PreferenceKeys.USER_DEFINED_THEME_COLOR_ACCENT, null));
            colorBackgroundImage = Color.parseColor(App.preferences.getString(PreferenceKeys.USER_DEFINED_THEME_COLOR_BACKGROUND_IMAGE, null));
        } else {
            colorPrimary = ContextCompat.getColor(this, R.color.colorPrimary);
            colorAccent = ContextCompat.getColor(this, R.color.colorAccent);
            colorBackgroundImage = ContextCompat.getColor(this, R.color.colorBackgroundImage);
        }

        configView();
        msgLoadMoreFromOnlineDb(1);
        uiMsgLoader();

    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseMessaging.getInstance().subscribeToTopic(Constants.FIREBASE.TOPIC);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        uiMsgLoader();
    }

    private void configView() {
        displayMetrics = getResources().getDisplayMetrics();
        screenWidthInDp = displayMetrics.widthPixels / displayMetrics.density;
        screenHeightInDp = displayMetrics.heightPixels / displayMetrics.density;

        minMsgIdInDb = App.preferences.getInt(PreferenceKeys.LOWEST_MSG_ID, PreferenceKeys.FALSE);
        intent = getIntent();
        msgType = intent.getIntExtra(EXTRA_MESSAGE_TYPE, 0);
        fromMain = intent.getBooleanExtra(EXTRA_FROM_MAIN, false);

        activity_chat = (LinearLayout) findViewById(R.id.activity_chat);
        img_background = (ImageView) findViewById(R.id.img_background);

        handler = new Handler();

        http = new AsyncHttpClient();
        mediaController = new MediaController(this);
        messageMediaController = new MediaController(this);
        messageMediaController.setAnchorView(ll_all_messages);

        ll_actionbar = (LinearLayout) findViewById(R.id.ll_actionbar);

        ll_all_messages = (LinearLayout) findViewById(R.id.ll_all_messages);
        ll_all_messages_blur = (LinearLayout) findViewById(R.id.ll_all_messages_blur);
        ll_loading_msg = (LinearLayout) findViewById(R.id.ll_loading_msg);
        txt_loading_msgs = (TextView) findViewById(R.id.txt_loading_msgs);
        pb_loading_msgs = (ProgressBar) findViewById(R.id.pb_loading_msgs);


        ll_message_types = (LinearLayout) findViewById(R.id.ll_message_types);
        ll_message_types_background = (GradientDrawable) ll_message_types.getBackground();
        btn_new_note = (ImageView) findViewById(R.id.btn_new_note);
        btn_new_note_background = (GradientDrawable) btn_new_note.getBackground();
        btn_new_image = (ImageView) findViewById(R.id.btn_new_image);
        btn_new_image_background = (GradientDrawable) btn_new_image.getBackground();
        btn_new_video = (ImageView) findViewById(R.id.btn_new_video);
        btn_new_video_background = (GradientDrawable) btn_new_video.getBackground();
        btn_new_audio = (ImageView) findViewById(R.id.btn_new_audio);
        btn_new_audio_background = (GradientDrawable) btn_new_audio.getBackground();
        btn_new_attcah = (ImageView) findViewById(R.id.btn_new_attcah);
        btn_new_attcah_background = (GradientDrawable) btn_new_attcah.getBackground();

        ll_message_form = (LinearLayout) findViewById(R.id.ll_message_form);

        fl_file_selector = (FrameLayout) findViewById(R.id.fl_file_selector);
        fl_file_selector_background = (GradientDrawable) fl_file_selector.getBackground();

        /*------------------Image Section Starts----------------------*/
        ll_image_chooser = (LinearLayout) findViewById(R.id.ll_image_chooser);
        ll_image_picker = (LinearLayout) findViewById(R.id.ll_image_picker);
        img_camera = (ImageView) findViewById(R.id.img_camera);
        img_image_gallery = (ImageView) findViewById(R.id.img_image_gallery);
        fl_image_preview = (FrameLayout) findViewById(R.id.fl_image_preview);
        ll_btn_close_image = (LinearLayout) findViewById(R.id.ll_btn_close_image);
        img_rotate_left = (ImageView) findViewById(R.id.img_rotate_left);
        img_rotate_right = (ImageView) findViewById(R.id.img_rotate_right);
        img_btn_close_image = (ImageView) findViewById(R.id.img_btn_close_image);
        img_image_preview = (ImageView) findViewById(R.id.img_image_preview);
        txt_image_preview = (TextView) findViewById(R.id.txt_image_preview);
        /*------------------Image Section Ends----------------------*/

        /*------------------Video Section Starts----------------------*/
        ll_video_chooser = (LinearLayout) findViewById(R.id.ll_video_chooser);
        ll_video_picker = (LinearLayout) findViewById(R.id.ll_video_picker);
        img_camcoder = (ImageView) findViewById(R.id.img_camcoder);
        img_video_gallery = (ImageView) findViewById(R.id.img_video_gallery);
        fl_video_preview = (FrameLayout) findViewById(R.id.fl_video_preview);
        ll_btn_close_video = (LinearLayout) findViewById(R.id.ll_btn_close_video);
        img_btn_close_video = (ImageView) findViewById(R.id.img_btn_close_video);
        vid_video_preview = (VideoView) findViewById(R.id.vid_video_preview);
        txt_video_preview = (TextView) findViewById(R.id.txt_video_preview);
        /*------------------Video Section Ends----------------------*/

        /*------------------Audio Section Starts----------------------*/
        ll_audio_chooser = (LinearLayout) findViewById(R.id.ll_audio_chooser);
        ll_audio_picker = (LinearLayout) findViewById(R.id.ll_audio_picker);
        img_microphone = (ImageView) findViewById(R.id.img_microphone);
        img_audio_gallery = (ImageView) findViewById(R.id.img_audio_gallery);
        fl_audio_preview = (FrameLayout) findViewById(R.id.fl_audio_preview);
        ll_btn_close_audio = (LinearLayout) findViewById(R.id.ll_btn_close_audio);
        img_btn_close_audio = (ImageView) findViewById(R.id.img_btn_close_audio);
        img_audio_preview = (ImageView) findViewById(R.id.img_audio_preview);
        txt_audio_preview = (TextView) findViewById(R.id.txt_audio_preview);
        /*------------------Audio Section Ends----------------------*/

        /*------------------File Section Starts----------------------*/
        ll_file_chooser = (LinearLayout) findViewById(R.id.ll_file_chooser);
        ll_file_picker = (LinearLayout) findViewById(R.id.ll_file_picker);
        img_file_picker = (ImageView) findViewById(R.id.img_file_picker);
        fl_file_preview = (FrameLayout) findViewById(R.id.fl_file_preview);
        ll_btn_close_file = (LinearLayout) findViewById(R.id.ll_btn_close_file);
        img_btn_close_file = (ImageView) findViewById(R.id.img_btn_close_file);
        img_file_preview = (ImageView) findViewById(R.id.img_file_preview);
        txt_file_preview = (TextView) findViewById(R.id.txt_file_preview);
        /*------------------File Section Ends----------------------*/

        ll_new_message_entry_box = (LinearLayout) findViewById(R.id.ll_new_message_entry_box);
        edttxt_new_message = (EditText) findViewById(R.id.edttxt_new_message);
        edttxt_new_message_background = (GradientDrawable) edttxt_new_message.getBackground();
        btn_send_message = (FrameLayout) findViewById(R.id.btn_send_message);
        img_btn_send_message = (ImageView) findViewById(R.id.img_btn_send_message);


        setMessagingLayouts(msgType);


        btn_new_note.setOnClickListener(this);

        btn_new_image.setOnClickListener(this);
        img_camera.setOnClickListener(this);
        img_image_gallery.setOnClickListener(this);
        ll_btn_close_image.setOnClickListener(this);
        img_rotate_left.setOnClickListener(this);
        img_rotate_right.setOnClickListener(this);

        btn_new_video.setOnClickListener(this);
        img_camcoder.setOnClickListener(this);
        img_video_gallery.setOnClickListener(this);
        ll_btn_close_video.setOnClickListener(this);

        vid_video_preview.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                return true;
            }
        });

        btn_new_audio.setOnClickListener(this);
        img_microphone.setOnClickListener(this);
        img_audio_gallery.setOnClickListener(this);
        ll_btn_close_audio.setOnClickListener(this);

        btn_new_attcah.setOnClickListener(this);
        img_file_picker.setOnClickListener(this);
        ll_btn_close_file.setOnClickListener(this);

        btn_send_message.setOnClickListener(this);

        setColorToElements();
    }

    private void setColorToElements() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(colorPrimary);
        }

        ll_actionbar.setBackgroundColor(colorPrimary);
        changeDrawableColor(img_background, colorBackgroundImage);
        txt_loading_msgs.setTextColor(colorPrimary);

        ll_message_types_background.setColor(ContextCompat.getColor(this, R.color.colorWhite));
        ll_message_types_background.setStroke(3, colorPrimary);
        btn_new_note_background.setColor(colorPrimary);
        btn_new_image_background.setColor(colorPrimary);
        btn_new_video_background.setColor(colorPrimary);
        btn_new_audio_background.setColor(colorPrimary);
        btn_new_attcah_background.setColor(colorPrimary);

        fl_file_selector_background.setColor(ContextCompat.getColor(this, R.color.colorWhite));
        fl_file_selector_background.setStroke(3, colorPrimary);

        ll_image_picker.setBackgroundColor(colorPrimary);
        changeDrawableColor(img_rotate_left, colorPrimary);
        changeDrawableColor(img_rotate_right, colorPrimary);
        changeDrawableColor(img_btn_close_image, colorPrimary);

        ll_video_picker.setBackgroundColor(colorPrimary);
        changeDrawableColor(img_btn_close_video, colorPrimary);

        ll_audio_picker.setBackgroundColor(colorPrimary);
        changeDrawableColor(img_btn_close_audio, colorPrimary);

        ll_file_picker.setBackgroundColor(colorPrimary);
        changeDrawableColor(img_btn_close_file, colorPrimary);

        ll_new_message_entry_box.setBackgroundColor(colorPrimary);
        edttxt_new_message.setTextColor(colorPrimary);
        edttxt_new_message.setHintTextColor(colorAccent);
        edttxt_new_message_background.setColor(ContextCompat.getColor(this, R.color.colorWhite));
        edttxt_new_message_background.setStroke(3, colorPrimary);
        changeDrawableColor(img_btn_send_message, colorPrimary);

        // Define a shape with rounded corners
        final float[] roundedCorners = new float[]{5, 5, 5, 5, 5, 5, 5, 5};
        ShapeDrawable pgDrawable = new ShapeDrawable(new RoundRectShape(roundedCorners, null, null));

        // Sets the progressBar color
        pgDrawable.getPaint().setColor(colorPrimary);

        // Adds the drawable to your progressBar
        ClipDrawable progress = new ClipDrawable(pgDrawable, Gravity.LEFT, ClipDrawable.HORIZONTAL);
        pb_loading_msgs.setProgressDrawable(progress);
    }

    private void changeDrawableColor(ImageView imageView, int color) {
        Drawable d = imageView.getDrawable();
        d.mutate();
        d.setColorFilter(color, PorterDuff.Mode.MULTIPLY);
        imageView.setImageDrawable(d);
    }

    private void imageParameters(ImageView layout, int height, int width) {
        ViewGroup.LayoutParams params = layout.getLayoutParams();
        float hdp = 0;
        float wdp = 0;
        if (height == 0) {
            hdp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, screenHeightInDp, displayMetrics);
        } else {
            hdp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, height, displayMetrics);
        }
        if (width == 0) {
            wdp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, screenWidthInDp, displayMetrics);
        } else {
            wdp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, width, displayMetrics);
        }
        params.height = (int) hdp;
        params.width = (int) wdp;
        layout.setLayoutParams(params);
    }

    private void videoParameters(VideoView layout, int height, int width) {
        ViewGroup.LayoutParams params = layout.getLayoutParams();
        float hdp = 0;
        float wdp = 0;
        if (height == 0) {
            hdp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, screenHeightInDp, displayMetrics);
        } else {
            hdp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, height, displayMetrics);
        }
        if (width == 0) {
            wdp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, screenWidthInDp, displayMetrics);
        } else {
            wdp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, width, displayMetrics);
        }
        params.height = (int) hdp;
        params.width = (int) wdp;
        layout.setLayoutParams(params);
    }

    private void uiMsgLoader() {

        MessageDbAdapter dbAdapter = new MessageDbAdapter(this);
        dbAdapter.open();
        messages = dbAdapter.getAllMessages();
        dbAdapter.close();
        messageAdapter = new MessageAdapter(this, messages);

        temppos = finalpmaxpos = (App.preferences.getInt(PreferenceKeys.HIGHEST_MSG_POS, 0) - 1);


        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        messageRecyclerView = (RecyclerView) this.findViewById(R.id.rv_all_messages);
        messageRecyclerView.setHasFixedSize(true);
        messageRecyclerView.setRecycledViewPool(new RecyclerView.RecycledViewPool());
        messageRecyclerView.setLayoutManager(linearLayoutManager);
        messageRecyclerView.setAdapter(messageAdapter);
        messageRecyclerView.scrollToPosition(temppos);

        messageRecyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                //int itemPosition = ((LinearLayoutManager) messageRecyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition();
                //((LinearLayoutManager) messageRecyclerView.getLayoutManager()).scrollToPosition(itemPosition);
                if (minMsgIdInDb != 1) {
                    ll_loading_msg.setVisibility(View.VISIBLE);
                    msgLoadMoreFromOnlineDb(0);
                } else {
                    txt_loading_msgs.setText(R.string.txt_no_more_msgs);
                    pb_loading_msgs.setVisibility(View.GONE);
                    ll_loading_msg.setVisibility(View.VISIBLE);
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ll_loading_msg.setVisibility(View.GONE);
                        }
                    }, 2000);
                }
            }
        });
    }

    public void msgLoadMoreFromOnlineDb(final int direction) {
        txt_loading_msgs.setText(R.string.txt_loading_msgs);
        pb_loading_msgs.setVisibility(View.VISIBLE);
        App.api.chatmsgsget(App.preferences.getString(PreferenceKeys.TOKEN, null), App.preferences.getInt(PreferenceKeys.HIGHEST_MSG_ID, 0), App.preferences.getInt(PreferenceKeys.LOWEST_MSG_ID, 0), direction, new Callback<ChatMsgsGet>() {
            @Override
            public void success(final ChatMsgsGet chatMsgsGet, Response response) {
                try {
                    if (chatMsgsGet.UserMessageFlag == 1) {
                        if (chatMsgsGet.ErrorFlag == 0 && chatMsgsGet.MessagesCount > 0) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    MessageDbAdapter dbAdapter = new MessageDbAdapter(getBaseContext());
                                    dbAdapter.open();
                                    loadedmessages = dbAdapter.writeToDb(chatMsgsGet.Messages, MessageDbAdapter.InsertTypes.OLD);
                                    dbAdapter.close();
                                    int i, j;
                                    for (i = 0, j = (loadedmessages.size() - 1); j >= 0; i++, j--) {
                                        messages.add(i, loadedmessages.get(j));
                                    }
                                    temppos = i;
                                    finalpmaxpos += i;

                                    handler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            Intent downloadServiceIntent = new Intent(getApplicationContext(), MessageFileDownloadService.class).putExtra(EXTRA_NEW_MESSAGES, false);
                                            startService(downloadServiceIntent);
                                        }
                                    });

                                    messageAdapter = new MessageAdapter(getBaseContext(), messages);
                                    messageRecyclerView.setAdapter(messageAdapter);
                                    messageRecyclerView.scrollToPosition(temppos);
                                    ll_loading_msg.setVisibility(View.GONE);
                                }
                            });
                            if (direction == 0) {
                                minMsgIdInDb = App.preferences.getInt(PreferenceKeys.LOWEST_MSG_ID, 0);
                            }
                        }
                        if (chatMsgsGet.ErrorFlag == 1) {
                            if (chatMsgsGet.TokenFlag == 2) {
                                Toast.makeText(ChatActivity.this, "Invalid Login! Please Login Again!", Toast.LENGTH_SHORT).show();
                            } else if (chatMsgsGet.TokenFlag == 0) {
                                Toast.makeText(ChatActivity.this, "Logged Out due to inactivity! Please Login Again!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        App.preferences.edit().putInt(PreferenceKeys.MESSAGE_ACTIVE, chatMsgsGet.UserMessageFlag).apply();
                        startActivity(new Intent(ChatActivity.this, MainActivity.class));
                        finish();
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(ChatActivity.this, "Can't connect to Server! Please check your Network Connection!", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void setMessagingLayouts(int type) {
        switch (type) {
            case 0:
                ll_message_types.setVisibility(View.VISIBLE);
                ll_message_form.setVisibility(View.GONE);
                fl_file_selector.setVisibility(View.GONE);
                ll_new_message_entry_box.setVisibility(View.GONE);
                ll_image_chooser.setVisibility(View.GONE);
                ll_audio_chooser.setVisibility(View.GONE);
                ll_video_chooser.setVisibility(View.GONE);
                ll_file_chooser.setVisibility(View.GONE);
                break;
            case 1:
                ll_message_types.setVisibility(View.GONE);
                ll_all_messages_blur.setVisibility(View.VISIBLE);
                ll_message_form.setVisibility(View.VISIBLE);
                ll_new_message_entry_box.setVisibility(View.VISIBLE);
                break;
            case 2:
                ll_message_types.setVisibility(View.GONE);
                ll_all_messages_blur.setVisibility(View.VISIBLE);
                ll_message_form.setVisibility(View.VISIBLE);
                fl_file_selector.setVisibility(View.VISIBLE);
                ll_new_message_entry_box.setVisibility(View.VISIBLE);
                ll_image_chooser.setVisibility(View.VISIBLE);
                break;
            case 3:
                ll_message_types.setVisibility(View.GONE);
                ll_all_messages_blur.setVisibility(View.VISIBLE);
                ll_message_form.setVisibility(View.VISIBLE);
                fl_file_selector.setVisibility(View.VISIBLE);
                ll_new_message_entry_box.setVisibility(View.VISIBLE);
                ll_video_chooser.setVisibility(View.VISIBLE);
                break;
            case 4:
                ll_message_types.setVisibility(View.GONE);
                ll_all_messages_blur.setVisibility(View.VISIBLE);
                ll_message_form.setVisibility(View.VISIBLE);
                fl_file_selector.setVisibility(View.VISIBLE);
                ll_new_message_entry_box.setVisibility(View.VISIBLE);
                ll_audio_chooser.setVisibility(View.VISIBLE);
                break;
            case 5:
                ll_message_types.setVisibility(View.GONE);
                ll_all_messages_blur.setVisibility(View.VISIBLE);
                ll_message_form.setVisibility(View.VISIBLE);
                fl_file_selector.setVisibility(View.VISIBLE);
                ll_new_message_entry_box.setVisibility(View.VISIBLE);
                ll_file_chooser.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void showFileChooser(int msgType, int subType) {
        Calendar cal = Calendar.getInstance();
        switch (msgType) {
            case 2:
                switch (subType) {
                    case 1:
                        try {
                            fileName = String.format("IMG_%d_%d_%d_%d_%d_%d.jpg", cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.YEAR), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));
                            //fileName = "IMAGE.jpg";
                            Constants.FILES_APP.FOLDER_CBF_CACHE.mkdirs();
                            cache = new File(Constants.FILES_APP.FOLDER_CBF_CACHE, fileName);
                            filePath = cache.getAbsolutePath();
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                                intent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(ChatActivity.this, BuildConfig.APPLICATION_ID + ".provider", cache));
                            } else {
                                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(cache));
                            }
                            startActivityForResult(intent, ACTIVITY_CAPTURE_IMAGE);
                        } catch (Exception e) {
                            Toast.makeText(this, "Please install a suitable Camera application..!!", Toast.LENGTH_SHORT).show();
                            Log.d("Exception", e.getMessage());
                        }
                        break;
                    case 2:
                        try {
                            Toast.makeText(this, "Select IMAGE files", Toast.LENGTH_LONG).show();
                            Intent mRequestImageIntent = new Intent(Intent.ACTION_PICK);
                            mRequestImageIntent.setType(Constants.FILE_TYPES.FILE_IMAGE);
                            startActivityForResult(Intent.createChooser(mRequestImageIntent, "Select an Image to upload"), ACTIVITY_CHOOSE_IMAGE);
                        } catch (android.content.ActivityNotFoundException e) {
                            Toast.makeText(this, "Please install a suitable File Manager application..!!", Toast.LENGTH_SHORT).show();
                        }
                        break;
                }
                break;
            case 3:
                switch (subType) {
                    case 1:
                        try {
                            fileName = String.format("VID_%d_%d_%d_%d_%d_%d.mp4", cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.YEAR), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));
                            //fileName = "VIDEO.mp4";
                            Constants.FILES_APP.FOLDER_CBF_CACHE.mkdirs();
                            cache = new File(Constants.FILES_APP.FOLDER_CBF_CACHE, fileName);
                            Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                            intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0);
                            intent.putExtra(MediaStore.EXTRA_SIZE_LIMIT, (Constants.FILE_SIZES.ONE_MB * Constants.FILE_SIZES.FILE_VIDEO_SIZE));
                            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                                intent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(ChatActivity.this, BuildConfig.APPLICATION_ID + ".provider", cache));
                            } else {
                                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(cache));
                            }
                            startActivityForResult(intent, ACTIVITY_CAPTURE_VIDEO);
                        } catch (Exception e) {
                            Toast.makeText(this, "Please install a suitable Video Recorder application..!!", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                        break;
                    case 2:
                        try {
                            Toast.makeText(this, "Select VIDEO files", Toast.LENGTH_LONG).show();
                            Intent mRequestVideoIntent = new Intent(Intent.ACTION_PICK);
                            mRequestVideoIntent.setType(Constants.FILE_TYPES.FILE_VIDEO);
                            mRequestVideoIntent.putExtra(MediaStore.EXTRA_SIZE_LIMIT, (Constants.FILE_SIZES.ONE_MB * Constants.FILE_SIZES.FILE_VIDEO_SIZE));
                            startActivityForResult(Intent.createChooser(mRequestVideoIntent, "Select a video to upload"), ACTIVITY_CHOOSE_VIDEO);
                        } catch (android.content.ActivityNotFoundException e) {
                            Toast.makeText(this, "Please install a suitable File Manager application..!!", Toast.LENGTH_SHORT).show();
                        }
                        break;
                }
                break;
            case 4:
                switch (subType) {
                    case 1:
                        try {
                            fileName = String.format("AUD_%d_%d_%d_%d_%d_%d.amr", cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.YEAR), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));
                            //fileName = "AUDIO.mp3";
                            Constants.FILES_APP.FOLDER_CBF_CACHE.mkdirs();
                            cache = new File(Constants.FILES_APP.FOLDER_CBF_CACHE, fileName);
                            Intent intent = new Intent(MediaStore.Audio.Media.RECORD_SOUND_ACTION);
                            //intent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(ChatActivity.this, BuildConfig.APPLICATION_ID + ".provider", cache));
                            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                                intent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(ChatActivity.this, BuildConfig.APPLICATION_ID + ".provider", cache));
                            } else {
                                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(cache));
                            }
                            startActivityForResult(intent, ACTIVITY_CAPTURE_AUDIO);
                        } catch (Exception e) {
                            Toast.makeText(this, "Please install a suitable Audio Recorder application..!!", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                        break;
                    case 2:
                        try {
                            Toast.makeText(this, "Select AUDIO files", Toast.LENGTH_LONG).show();
                            Intent mRequestAudioIntent = new Intent(Intent.ACTION_PICK);
                            mRequestAudioIntent.setType(Constants.FILE_TYPES.FILE_AUDIO);
                            mRequestAudioIntent.setAction(Intent.ACTION_GET_CONTENT);
                            startActivityForResult(Intent.createChooser(mRequestAudioIntent, "Select an audio to upload"), ACTIVITY_CHOOSE_AUDIO);
                        } catch (android.content.ActivityNotFoundException e) {
                            Toast.makeText(this, "Please install a suitable File Manager application..!!", Toast.LENGTH_SHORT).show();
                        }
                        break;
                }
                break;
            case 5:
                try {
                    Toast.makeText(this, "Select PDF or MICROSOFT OFFICE files", Toast.LENGTH_LONG).show();
                    Intent galleryFileIntent = new Intent(Intent.ACTION_GET_CONTENT);
                    galleryFileIntent.setType(Constants.FILE_TYPES.FILE_PDF + "|" + Constants.FILE_TYPES.FILE_DOC + "|" + Constants.FILE_TYPES.FILE_XLS + "|" + Constants.FILE_TYPES.FILE_PPT);
                    galleryFileIntent.addCategory(Intent.CATEGORY_OPENABLE);
                    startActivityForResult(Intent.createChooser(galleryFileIntent, "Select a File to upload"), ACTIVITY_CHOOSE_FILE);
                } catch (android.content.ActivityNotFoundException e) {
                    Toast.makeText(this, "Please install a suitable File Manager application..!!", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void sendMessage(int type) {
        if (type == 1) {
            String MsgText = StringEscapeUtils.escapeJava(edttxt_new_message.getText().toString());
            if (MsgText.length() == 0) {
                Toast.makeText(ChatActivity.this, "Empty Message Content", Toast.LENGTH_SHORT).show();
                btn_send_message.clearAnimation();
                return;
            }
            imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(edttxt_new_message.getWindowToken(), 0);
            App.api.chatmsgsend(App.preferences.getString(PreferenceKeys.TOKEN, null), msgType, MsgText, new Callback<ChatMsgSend>() {
                @Override
                public void success(final ChatMsgSend textMsgSendresponse, Response response) {
                    sendMsgCallbackFunctions(textMsgSendresponse);
                }

                @Override
                public void failure(RetrofitError error) {
                    btn_send_message.clearAnimation();
                    Toast.makeText(ChatActivity.this, "Can't connect to Server! Please check your Network Connection!", Toast.LENGTH_LONG).show();
                }
            });
        } else {
            switch (type) {
                case 2:
                    if (isFileSelected) {
                        try {
                            String MessageText = edttxt_new_message.getText().toString();
                            imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(edttxt_new_message.getWindowToken(), 0);
                            String filename = cache.getName();
                            Log.d("File Name", filename);
                            getMimeType(filename);
                            App.api.chatfilesend(
                                    new TypedString(App.preferences.getString(PreferenceKeys.TOKEN, null)),
                                    new TypedString(String.valueOf(msgType)),
                                    new TypedString(MessageText),
                                    new TypedFile(fileType, cache),
                                    new Callback<ChatMsgSend>() {
                                        @Override
                                        public void success(ChatMsgSend attachMsgSendresponse, Response response) {
                                            sendMsgCallbackFunctions(attachMsgSendresponse);
                                        }

                                        @Override
                                        public void failure(RetrofitError error) {
                                            btn_send_message.clearAnimation();
                                            Toast.makeText(ChatActivity.this, "Can't connect to Server! Please check your Network Connection!", Toast.LENGTH_LONG).show();
                                        }
                                    });
                        } catch (Exception e) {
                            btn_send_message.clearAnimation();
                            e.printStackTrace();
                        }
                    } else {
                        btn_send_message.clearAnimation();
                        Snackbar.make(activity_chat, "Please select an attachment..!!", Snackbar.LENGTH_SHORT).show();
                    }
                    break;
                case 3:
                    if (isFileSelected) {
                        try {
                            final String MessageText = edttxt_new_message.getText().toString();
                            imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(edttxt_new_message.getWindowToken(), 0);
                            Log.d("Token", App.preferences.getString(PreferenceKeys.TOKEN, null));
                            Log.d("MessageType", String.valueOf(msgType));
                            Log.d("MessageText", MessageText);
                            Log.d("MessageFileName", cache.getName());
                            //Log.d("MessageFileType", fileType);
                            getMimeType(fileName);
                            App.api.chatfilesend(
                                    new TypedString(App.preferences.getString(PreferenceKeys.TOKEN, null)),
                                    new TypedString(String.valueOf(msgType)),
                                    new TypedString(MessageText),
                                    new TypedFile(fileType, cache),
                                    new Callback<ChatMsgSend>() {
                                        @Override
                                        public void success(ChatMsgSend attachMsgSendresponse, Response response) {
                                            sendMsgCallbackFunctions(attachMsgSendresponse);
                                        }

                                        @Override
                                        public void failure(RetrofitError error) {
                                            btn_send_message.clearAnimation();
                                            Toast.makeText(ChatActivity.this, "Can't connect to Server! Please check your Network Connection!", Toast.LENGTH_LONG).show();
                                        }
                                    });
                        } catch (Exception e) {
                            btn_send_message.clearAnimation();
                            e.printStackTrace();
                        }
                    } else {
                        btn_send_message.clearAnimation();
                        Snackbar.make(activity_chat, "Please select an attachment..!!", Snackbar.LENGTH_SHORT).show();
                    }
                    break;
                case 4:
                    if (isFileSelected) {
                        try {
                            String MessageText = edttxt_new_message.getText().toString();
                            imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(edttxt_new_message.getWindowToken(), 0);
                            App.api.chatfilesend(
                                    new TypedString(App.preferences.getString(PreferenceKeys.TOKEN, null)),
                                    new TypedString(String.valueOf(msgType)),
                                    new TypedString(MessageText),
                                    new TypedFile(fileType, cache),
                                    new Callback<ChatMsgSend>() {
                                        @Override
                                        public void success(ChatMsgSend attachMsgSendresponse, Response response) {
                                            sendMsgCallbackFunctions(attachMsgSendresponse);
                                        }

                                        @Override
                                        public void failure(RetrofitError error) {
                                            btn_send_message.clearAnimation();
                                            Toast.makeText(ChatActivity.this, "Can't connect to Server! Please check your Network Connection!", Toast.LENGTH_LONG).show();
                                        }
                                    });
                        } catch (Exception e) {
                            btn_send_message.clearAnimation();
                            e.printStackTrace();
                        }
                    } else {
                        btn_send_message.clearAnimation();
                        Snackbar.make(activity_chat, "Please select an attachment..!!", Snackbar.LENGTH_SHORT).show();
                    }
                    break;
                case 5:
                    if (isFileSelected) {
                        try {
                            String MessageText = edttxt_new_message.getText().toString();
                            imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(edttxt_new_message.getWindowToken(), 0);
                            App.api.chatfilesend(
                                    new TypedString(App.preferences.getString(PreferenceKeys.TOKEN, null)),
                                    new TypedString(String.valueOf(msgType)),
                                    new TypedString(MessageText),
                                    new TypedFile(fileType, cache),
                                    new Callback<ChatMsgSend>() {
                                        @Override
                                        public void success(ChatMsgSend attachMsgSendresponse, Response response) {
                                            sendMsgCallbackFunctions(attachMsgSendresponse);
                                        }

                                        @Override
                                        public void failure(RetrofitError error) {
                                            btn_send_message.clearAnimation();
                                            Toast.makeText(ChatActivity.this, "Can't connect to Server! Please check your Network Connection!", Toast.LENGTH_LONG).show();
                                        }
                                    });
                        } catch (Exception e) {
                            btn_send_message.clearAnimation();
                            e.printStackTrace();
                        }
                    } else {
                        btn_send_message.clearAnimation();
                        Snackbar.make(activity_chat, "Please select an attachment..!!", Snackbar.LENGTH_SHORT).show();
                    }
                    break;
            }
        }
    }

    private void sendMsgCallbackFunctions(ChatMsgSend chatMsgSend) {
        try {
            if (chatMsgSend.UserMessageFlag == 1) {
                if (chatMsgSend.MsgSendFlag == 1) {
                    btn_send_message.clearAnimation();
                    ll_all_messages_blur.setVisibility(View.GONE);
                    edttxt_new_message.setText("");
                    messageNotificationUtils.sendMessage(Integer.parseInt(chatMsgSend.UserId), chatMsgSend.UserFullName, chatMsgSend.MessageID, chatMsgSend.MessageText);
                    if (chatMsgSend.MessageID == (App.preferences.getInt(PreferenceKeys.HIGHEST_MSG_ID, 0) + 1)) {
                        MessageDbAdapter dbAdapter = new MessageDbAdapter(getBaseContext());
                        dbAdapter.open();
                        Message newMessage = dbAdapter.createMessages(1, chatMsgSend.MessageID, chatMsgSend.UserId, chatMsgSend.UserFullName, chatMsgSend.UserMobile, chatMsgSend.UserEmail, chatMsgSend.UserCompanyName, chatMsgSend.MessageText, chatMsgSend.MessageFile, chatMsgSend.MessageType, chatMsgSend.MsgDateTime, 1, chatMsgSend.MessageDelFlag);
                        messages.add(newMessage);
                        messageAdapter = new MessageAdapter(getBaseContext(), messages);
                        messageRecyclerView.setAdapter(messageAdapter);
                        finalpmaxpos++;
                        messageRecyclerView.scrollToPosition(finalpmaxpos);
                    } else {
                        msgLoadMoreFromOnlineDb(1);
                    }
                    if (chatMsgSend.MessageType != 1) {
                        switch (chatMsgSend.MessageType) {
                            case 2:
                                ll_image_picker.setVisibility(View.VISIBLE);
                                fl_image_preview.setVisibility(View.GONE);
                                txt_image_preview.setText("");
                                destination = new File(Constants.FILES_APP.FOLDER_MESSAGE_IMAGE, chatMsgSend.MessageFile);
                                break;
                            case 3:
                                ll_video_picker.setVisibility(View.VISIBLE);
                                fl_video_preview.setVisibility(View.GONE);
                                txt_video_preview.setText("");
                                vid_video_preview.stopPlayback();
                                vid_video_preview.suspend();
                                try {
                                    String fileExtension = MimeTypeMap.getFileExtensionFromUrl(chatMsgSend.MessageFile);
                                    Log.d("Thumbnail Extension", fileExtension);
                                    String thumbName = chatMsgSend.MessageFile.replace(fileExtension, "jpg");
                                    Log.d("Thumbnail Name", thumbName);
                                    File msgThumb = new File(Constants.FILES_APP.FOLDER_MESSAGE_VIDEO_THUMBNAILS, thumbName);
                                    Bitmap thumbnail = ThumbnailUtils.createVideoThumbnail(cache.getAbsolutePath(), MediaStore.Images.Thumbnails.MINI_KIND);
                                    Bitmap scaledBm = resizeBitmap(thumbnail);
                                    FileOutputStream fos = new FileOutputStream(msgThumb.getAbsolutePath());
                                    scaledBm.compress(Bitmap.CompressFormat.JPEG, 90, fos);
                                    fos.close();
                                } catch (Exception e) {

                                }
                                destination = new File(Constants.FILES_APP.FOLDER_MESSAGE_VIDEO, chatMsgSend.MessageFile);
                                break;
                            case 4:
                                ll_audio_picker.setVisibility(View.VISIBLE);
                                fl_audio_preview.setVisibility(View.GONE);
                                txt_audio_preview.setText("");
                                destination = new File(Constants.FILES_APP.FOLDER_MESSAGE_AUDIO, chatMsgSend.MessageFile);
                                break;
                            case 5:
                                ll_file_picker.setVisibility(View.VISIBLE);
                                fl_file_preview.setVisibility(View.GONE);
                                txt_file_preview.setText("");
                                destination = new File(Constants.FILES_APP.FOLDER_MESSAGE_DOCUMENT, chatMsgSend.MessageFile);
                                break;
                        }
                        moveFile(cache, destination, true);
                    }
                    //Toast.makeText(ChatActivity.this, "Message Send", Toast.LENGTH_SHORT).show();
                }
                if (chatMsgSend.ErrorFlag == 1) {
                    if (chatMsgSend.TokenFlag == 2) {
                        Toast.makeText(ChatActivity.this, "Invalid Login! Please Login Again!", Toast.LENGTH_SHORT).show();
                    } else if (chatMsgSend.TokenFlag == 0) {
                        Toast.makeText(ChatActivity.this, "Logged Out due to inactivity! Please Login Again!", Toast.LENGTH_SHORT).show();
                    }
                }
            } else {
                App.preferences.edit().putInt(PreferenceKeys.MESSAGE_ACTIVE, chatMsgSend.UserMessageFlag).apply();
                startActivity(new Intent(ChatActivity.this, MainActivity.class));
                finish();
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private void copyFile(String sourcePath, File destination, boolean delete) throws IOException {
        source = new File(sourcePath);
        moveFile(source, destination, delete);
        isFileSelected = true;
    }

    private void moveFile(File sourceFile, File destinationFile, boolean delete) {
        try {
            FileUtils.copyFile(sourceFile, destinationFile);
            if (delete) {
                sourceFile.delete();
                isFileSelected = false;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void removeFile(int type) {
        if (type == 2) {
            ll_image_picker.setVisibility(View.VISIBLE);
            fl_image_preview.setVisibility(View.GONE);
            txt_image_preview.setText("");
        }
        if (type == 3) {
            ll_video_picker.setVisibility(View.VISIBLE);
            fl_video_preview.setVisibility(View.GONE);
            txt_video_preview.setText("");
            vid_video_preview.setVisibility(View.GONE);
            vid_video_preview.stopPlayback();
            vid_video_preview.refreshDrawableState();
        }
        if (type == 4) {
            ll_audio_picker.setVisibility(View.VISIBLE);
            fl_audio_preview.setVisibility(View.GONE);
            txt_audio_preview.setText("");
        }
        if (type == 5) {
            ll_file_picker.setVisibility(View.VISIBLE);
            fl_file_preview.setVisibility(View.GONE);
            txt_file_preview.setText("");
        }
        if (cache.delete()) {
            isFileSelected = false;
        }
    }

    public void getMimeType(String Filename) {
        Filename = Filename.toLowerCase();
        //Filename = Filename.replace(" ", "");
        Filename = Filename.replaceAll("[^a-zA-Z0-9.-]", "_");
        //String extension = Filename.substring(Filename.lastIndexOf("."));
        String extension = MimeTypeMap.getFileExtensionFromUrl(Filename);
        Log.d("File Extension", extension);
        if (extension != null) {
            fileType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
    }

    public void getFileSize(String passedPath) {
        source = new File(passedPath);
        fileSize = (Float.parseFloat(String.valueOf(source.length() / Constants.FILE_SIZES.ONE_MB)));
    }

    private Bitmap resizeBitmap(Bitmap bm) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        //Log.d(TAG, "resizeBitmap w: " + width + " h: " + height);
        int targetWidth;
        int targetHeight;
        float scaleFactor;
        if (width > height) {
            scaleFactor = ((float) MAX_IMAGE_DIMENSION) / ((float) width);
            targetWidth = MAX_IMAGE_DIMENSION;
            targetHeight = (int) (height * scaleFactor);
        } else {
            scaleFactor = ((float) MAX_IMAGE_DIMENSION) / ((float) height);
            targetHeight = MAX_IMAGE_DIMENSION;
            targetWidth = (int) (width * scaleFactor);
        }

        //Log.d(TAG, "resizeBitmap scaled to w: " + targetWidth + " h: " + targetHeight);
        return Bitmap.createScaledBitmap(bm, targetWidth, targetHeight, true);
    }

    private Bitmap getBitmap(String path) {

        ContentResolver mContentResolver = getBaseContext().getContentResolver();
        //Uri uri = getImageUri(path);
        Uri uri = Uri.parse(new File(path).toString());
        InputStream in = null;
        try {
            final int IMAGE_MAX_SIZE = 1200000; // 1.2MP
            in = mContentResolver.openInputStream(uri);

            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, o);
            in.close();

            int scale = 1;
            while ((o.outWidth * o.outHeight) * (1 / Math.pow(scale, 2)) >
                    IMAGE_MAX_SIZE) {
                scale++;
            }
            Log.d(TAG, "scale = " + scale + ", orig-width: " + o.outWidth + ", orig-height: " + o.outHeight);

            Bitmap bitmap = null;
            in = mContentResolver.openInputStream(uri);
            if (scale > 1) {
                scale--;
                // scale to max possible inSampleSize that still yields an image
                // larger than target
                o = new BitmapFactory.Options();
                o.inSampleSize = scale;
                bitmap = BitmapFactory.decodeStream(in, null, o);

                // resize to desired dimensions
                int height = bitmap.getHeight();
                int width = bitmap.getWidth();
                Log.d(TAG, "1th scale operation dimenions - width: " + width + ", height: " + height);

                double y = Math.sqrt(IMAGE_MAX_SIZE
                        / (((double) width) / height));
                double x = (y / height) * width;

                Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, (int) x,
                        (int) y, true);
                bitmap.recycle();
                bitmap = scaledBitmap;

                System.gc();
            } else {
                bitmap = BitmapFactory.decodeStream(in);
            }
            in.close();

            Log.d(TAG, "bitmap size - width: " + bitmap.getWidth() + ", height: " + bitmap.getHeight());
            return bitmap;
        } catch (IOException e) {
            Log.e(TAG, e.getMessage(), e);
            return null;
        }
    }

    private void showImagePreview(final Bitmap bm) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    finalOutput = new FileOutputStream(cache.getAbsolutePath());
                    bm.compress(Bitmap.CompressFormat.PNG, 100, finalOutput);
                    finalOutput.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                int width = bm.getWidth();
                int height = bm.getHeight();
                int targetWidth;
                int targetHeight;
                float scaleFactor;
                if (width > height) {
                    scaleFactor = ((float) 250) / ((float) width);
                    targetWidth = 250;
                    targetHeight = (int) (height * scaleFactor);
                } else {
                    scaleFactor = ((float) 250) / ((float) height);
                    targetHeight = 250;
                    targetWidth = (int) (width * scaleFactor);
                }
                imageParameters(img_image_preview, targetHeight, targetWidth);
                ll_image_picker.setVisibility(View.GONE);
                fl_image_preview.setVisibility(View.VISIBLE);
                img_image_preview.setImageBitmap(bm);
            }
        });
    }

    private Bitmap rotateBitmap(Bitmap bitmap, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        return rotatedBitmap;
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void requestPermissions(Runnable ifAllSetDoWhat, int requestCode) {
        runAfterPermissionGrant = ifAllSetDoWhat;
        int cameraAccessPermissionStatus = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int microphoneAccessPermissionStatus = ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO);
        int readStoragePermissionStatus = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int writeStoragePermissionStatus = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        switch (requestCode) {
            case Constants.PERMISSION_REQUESTS.REQUEST_CAMERA_PERMISSIONS:
                if (cameraAccessPermissionStatus == PackageManager.PERMISSION_DENIED) {
                    ActivityCompat.requestPermissions(this, new String[]{
                            Manifest.permission.CAMERA}, Constants.PERMISSION_REQUESTS.REQUEST_CAMERA_PERMISSIONS);
                } else {
                    cameraPermissionsSet = true;
                    if (cameraPermissionsSet) {
                        if (runAfterPermissionGrant != null) {
                            runAfterPermissionGrant.run();
                        }
                    }
                }
                break;
            case Constants.PERMISSION_REQUESTS.REQUEST_MICROPHONE_PERMISSIONS:
                if (microphoneAccessPermissionStatus == PackageManager.PERMISSION_DENIED) {
                    ActivityCompat.requestPermissions(this, new String[]{
                            Manifest.permission.RECORD_AUDIO}, Constants.PERMISSION_REQUESTS.REQUEST_MICROPHONE_PERMISSIONS);
                } else {
                    microphonePermissionsSet = true;
                    if (microphonePermissionsSet) {
                        if (runAfterPermissionGrant != null) {
                            runAfterPermissionGrant.run();
                        }
                    }
                }
                break;
            case Constants.PERMISSION_REQUESTS.REQUEST_FILE_PERMISSIONS:
                if (readStoragePermissionStatus == PackageManager.PERMISSION_DENIED
                        || writeStoragePermissionStatus == PackageManager.PERMISSION_DENIED) {
                    ActivityCompat.requestPermissions(this, new String[]{
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE}, Constants.PERMISSION_REQUESTS.REQUEST_FILE_PERMISSIONS);
                } else {
                    filePermissionsSet = true;
                    if (filePermissionsSet) {
                        if (runAfterPermissionGrant != null) {
                            runAfterPermissionGrant.run();
                        }
                    }
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constants.PERMISSION_REQUESTS.REQUEST_CAMERA_PERMISSIONS:
                for (int result : grantResults)
                    if (result == PackageManager.PERMISSION_DENIED) {
                        cameraPermissionsSet = false;
                        return;
                    }
                cameraPermissionsSet = true;
                App.preferences.edit().putBoolean(PreferenceKeys.PERMISSION_CAM, true).apply();
                if (cameraPermissionsSet) {
                    if (runAfterPermissionGrant != null) {
                        runAfterPermissionGrant.run();
                    }
                }
                break;
            case Constants.PERMISSION_REQUESTS.REQUEST_MICROPHONE_PERMISSIONS:
                for (int result : grantResults)
                    if (result == PackageManager.PERMISSION_DENIED) {
                        microphonePermissionsSet = false;
                        return;
                    }
                microphonePermissionsSet = true;
                App.preferences.edit().putBoolean(PreferenceKeys.PERMISSION_MIC, true).apply();
                if (microphonePermissionsSet) {
                    if (runAfterPermissionGrant != null) {
                        runAfterPermissionGrant.run();
                    }
                }
                break;
            case Constants.PERMISSION_REQUESTS.REQUEST_FILE_PERMISSIONS:
                for (int result : grantResults)
                    if (result == PackageManager.PERMISSION_DENIED) {
                        filePermissionsSet = false;
                        return;
                    }
                filePermissionsSet = true;
                App.preferences.edit().putBoolean(PreferenceKeys.PERMISSION_STR, true).apply();
                if (filePermissionsSet) {
                    if (runAfterPermissionGrant != null) {
                        runAfterPermissionGrant.run();
                    }
                }
                break;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case ACTIVITY_CAPTURE_IMAGE:
                try {
                    if (cache.exists()) {
                        Log.d("File Path", cache.getAbsolutePath());
                        msgImageBitmap = BitmapFactory.decodeFile(cache.getAbsolutePath());
                        scaledBm = resizeBitmap(msgImageBitmap);
                        //Bitmap scaledBm = getBitmap(cache.getAbsolutePath());
                        txt_image_preview.setText(fileName);
                        //showImagePreview(rotateBitmap(scaledBm, -90));
                        showImagePreview(scaledBm);
                        isFileSelected = true;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case ACTIVITY_CHOOSE_IMAGE:
                if (resultCode == RESULT_OK) {
                    // Get the Uri of the selected file
                    fileUri = data.getData();
                    Log.d("Content Uri", fileUri.toString());
                    if (data.getData().getScheme().equals("content")) {
                        Cursor cursor = getBaseContext().getContentResolver().query(fileUri, new String[]{android.provider.MediaStore.Images.ImageColumns.DATA}, null, null, null);
                        if (cursor.moveToFirst()) {
                            fileUri = Uri.parse("file://" + Uri.parse(cursor.getString(0)).getPath());
                        }
                        cursor.close();
                    }
                    Log.d("File Uri", "" + fileUri.toString());
                    // Get the path of the selected file
                    try {
                        filePath = messageFileUtils.getPath(this, fileUri);
                        Log.d("File Path", "" + filePath);
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                    // Get the name of the selected file
                    fileName = fileUri.getLastPathSegment();
                    Log.d("File Name", fileName);
                    // Get the type of the selected file
                    getMimeType(fileName);
                    Log.d("File Mime", fileType);
                    if (fileType != Constants.FILE_TYPES.FILE_IMAGE_JPEG && fileType != Constants.FILE_TYPES.FILE_IMAGE_JPG && fileType != Constants.FILE_TYPES.FILE_IMAGE_PNG) {
                        Snackbar.make(activity_chat, "Sorry..!! Wrong file type chosen..!!", Snackbar.LENGTH_LONG).show();
                    } else {
                        // Get the size of the selected file
                        getFileSize(filePath);
                        Log.d("File Size", fileSize + "");
                        if (fileSize < Constants.FILE_SIZES.FILE_IMAGE_SIZE) {
                            ll_image_picker.setVisibility(View.GONE);
                            fl_image_preview.setVisibility(View.VISIBLE);
                            txt_image_preview.setText(fileName);
                            cache = new File(Constants.FILES_APP.FOLDER_CBF_CACHE, fileName);
                            try {
                                copyFile(filePath, cache, false);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            if (cache.exists()) {
                                msgImageBitmap = BitmapFactory.decodeFile(cache.getAbsolutePath());
                                scaledBm = resizeBitmap(msgImageBitmap);
                                showImagePreview(scaledBm);
                            }
                        } else {
                            Snackbar.make(activity_chat, "Sorry..!! The file size exceeds " + Constants.FILE_SIZES.FILE_IMAGE_SIZE + "MB..!!", Snackbar.LENGTH_LONG).show();
                        }
                    }
                }
                break;
            case ACTIVITY_CAPTURE_VIDEO:
                try {
                    if (cache.exists()) {
                        ll_video_picker.setVisibility(View.GONE);
                        fl_video_preview.setVisibility(View.VISIBLE);
                        txt_video_preview.setText(fileName);
                        isFileSelected = true;
                        //getMimeType(fileName);
                        filePath = cache.getAbsolutePath();
                        Log.d("File Path", filePath);
                    }
                    if (isFileSelected) {
                        MediaController controller = new MediaController(this);
                        controller.setAnchorView(vid_video_preview);
                        controller.setMediaPlayer(vid_video_preview);
                        Bitmap thumbnail = ThumbnailUtils.createVideoThumbnail(filePath, MediaStore.Images.Thumbnails.MINI_KIND);
                        int width = thumbnail.getWidth();
                        int height = thumbnail.getHeight();
                        int targetWidth;
                        int targetHeight;
                        float scaleFactor;
                        if (width > height) {
                            scaleFactor = ((float) 250) / ((float) width);
                            targetWidth = 250;
                            targetHeight = (int) (height * scaleFactor);
                        } else {
                            scaleFactor = ((float) 250) / ((float) height);
                            targetHeight = 250;
                            targetWidth = (int) (width * scaleFactor);
                        }
                        videoParameters(vid_video_preview, targetHeight, targetWidth);
                        vid_video_preview.setMediaController(controller);
                        vid_video_preview.setVideoPath(filePath);
                        vid_video_preview.start();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case ACTIVITY_CHOOSE_VIDEO:
                if (resultCode == RESULT_OK) {
                    boolean isContentType = true;
                    // Get the Uri of the selected file
                    fileUri = data.getData();
                    Uri tempFileUri = data.getData();
                    Log.d("Content Uri", fileUri.toString());
                    if (data.getData().getScheme().equals("content")) {
                        isContentType = true;
                        ContentResolver cR = this.getContentResolver();
                        fileType = cR.getType(fileUri);
                        Cursor cursor = getBaseContext().getContentResolver().query(fileUri, new String[]{android.provider.MediaStore.Images.ImageColumns.DATA}, null, null, null);
                        if (cursor.moveToFirst()) {
                            tempFileUri = Uri.parse("file://" + Uri.parse(cursor.getString(0)).getPath());
                        }
                        cursor.close();
                    } else {
                        isContentType = false;
                    }
                    Log.d("File Uri", "" + tempFileUri.toString());
                    // Get the path of the selected file
                    try {
                        filePath = messageFileUtils.getPath(this, fileUri);
                        Log.d("File Path", "" + filePath);
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                    // Get the name of the selected file
                    fileName = tempFileUri.getLastPathSegment();
                    //fileName = fileName.replace(" ", "");
                    Log.d("File Name", fileName);
                    // Get the type of the selected file
                    if (!isContentType) {
                        getMimeType(fileName);
                    }
                    Log.d("File Mime", fileType);
                    /*if (fileType != Constants.FILE_TYPES.FILE_VIDEO_MP4 && fileType != Constants.FILE_TYPES.FILE_VIDEO_3GP && fileType != Constants.FILE_TYPES.FILE_VIDEO_3GPP && fileType != Constants.FILE_TYPES.FILE_VIDEO_AVI) {
                        Snackbar.make(activity_chat, "Sorry..!! Wrong video type chosen..!!", Snackbar.LENGTH_LONG).show();
                    } else {*/
                    // Get the size of the selected file
                    getFileSize(filePath);
                    Log.d("File Size", fileSize + "");
                    if (fileSize < Constants.FILE_SIZES.FILE_VIDEO_SIZE) {
                        ll_video_picker.setVisibility(View.GONE);
                        fl_video_preview.setVisibility(View.VISIBLE);
                        txt_video_preview.setText(fileName);
                        cache = new File(Constants.FILES_APP.FOLDER_CBF_CACHE, "VIDEO." + fileType.replace("video/", ""));
                        try {
                            copyFile(filePath, cache, false);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        if (isFileSelected) {
                            MediaController controller = new MediaController(this);
                            controller.setAnchorView(vid_video_preview);
                            controller.setMediaPlayer(vid_video_preview);
                            Bitmap thumbnail = ThumbnailUtils.createVideoThumbnail(filePath, MediaStore.Images.Thumbnails.MINI_KIND);
                            int width = thumbnail.getWidth();
                            int height = thumbnail.getHeight();
                            int targetWidth;
                            int targetHeight;
                            float scaleFactor;
                            if (width > height) {
                                scaleFactor = ((float) 250) / ((float) width);
                                targetWidth = 250;
                                targetHeight = (int) (height * scaleFactor);
                            } else {
                                scaleFactor = ((float) 250) / ((float) height);
                                targetHeight = 250;
                                targetWidth = (int) (width * scaleFactor);
                            }
                            videoParameters(vid_video_preview, targetHeight, targetWidth);
                            vid_video_preview.setMediaController(controller);
                            vid_video_preview.setVideoPath(filePath);
                            vid_video_preview.start();
                        }
                    } else {
                        Snackbar.make(activity_chat, "Sorry..!! The file size exceeds " + Constants.FILE_SIZES.FILE_VIDEO_SIZE + "MB..!!", Snackbar.LENGTH_LONG).show();
                        vid_video_preview.setVisibility(View.GONE);
                    }
                    //}
                }
                break;
            case ACTIVITY_CAPTURE_AUDIO:
                if (resultCode == RESULT_OK) {
                    // Get the Uri of the selected file
                    fileUri = data.getData();
                    Log.d("Content Uri", fileUri.toString());
                    if (data.getData().getScheme().equals("content")) {
                        ContentResolver cR = this.getContentResolver();
                        fileType = cR.getType(fileUri);
                        Cursor cursor = getBaseContext().getContentResolver().query(fileUri, new String[]{android.provider.MediaStore.Images.ImageColumns.DATA}, null, null, null);
                        if (cursor.moveToFirst()) {
                            fileUri = Uri.parse("file://" + Uri.parse(cursor.getString(0)).getPath());
                        }
                        cursor.close();
                    }
                    Log.d("File Uri", "" + fileUri.toString());
                    // Get the path of the selected file
                    try {
                        filePath = messageFileUtils.getPath(this, fileUri);
                        Log.d("File Path", "" + filePath);
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                    // Get the name of the selected file
                    fileName = fileUri.getLastPathSegment();
                    Log.d("File Name", fileName);
                    // Get the type of the selected file
                    getMimeType(fileName.replace(" ", ""));
                    Log.d("File Mime", fileType);
                    if (fileType != Constants.FILE_TYPES.FILE_AUDIO_MPEG && fileType != Constants.FILE_TYPES.FILE_AUDIO_3GP && fileType != Constants.FILE_TYPES.FILE_AUDIO_AMR && fileType != Constants.FILE_TYPES.FILE_AUDIO_WAV && fileType != Constants.FILE_TYPES.FILE_AUDIO_OGG) {
                        Snackbar.make(activity_chat, "Sorry..!! Wrong file type chosen..!!", Snackbar.LENGTH_LONG).show();
                    } else {
                        // Get the size of the selected file
                        getFileSize(filePath);
                        Log.d("File Size", fileSize + "");
                        if (fileSize < Constants.FILE_SIZES.FILE_AUDIO_SIZE) {
                            ll_audio_picker.setVisibility(View.GONE);
                            fl_audio_preview.setVisibility(View.VISIBLE);
                            txt_audio_preview.setText(fileName);
                            cache = new File(Constants.FILES_APP.FOLDER_CBF_CACHE, fileName);
                            try {
                                copyFile(filePath, cache, true);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Snackbar.make(activity_chat, "Sorry..!! The file size exceeds " + Constants.FILE_SIZES.FILE_ATTACH_SIZE + "MB..!!", Snackbar.LENGTH_LONG).show();
                        }
                    }
                }
                break;
            case ACTIVITY_CHOOSE_AUDIO:
                if (resultCode == RESULT_OK) {
                    try {
                        // Get the Uri of the selected file
                        fileUri = data.getData();
                        Log.d("Content Uri", fileUri.toString());
                        if (data.getData().getScheme().equals("content")) {
                            ContentResolver cR = this.getContentResolver();
                            fileType = cR.getType(fileUri);
                            try {
                                Cursor cursor = getBaseContext().getContentResolver().query(fileUri, new String[]{android.provider.MediaStore.Images.ImageColumns.DATA}, null, null, null);
                                if (cursor.moveToFirst()) {
                                    fileUri = Uri.parse("file://" + Uri.parse(cursor.getString(0)).getPath());
                                }
                                cursor.close();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        Log.d("File Uri", "" + fileUri.toString());
                        // Get the path of the selected file
                        filePath = messageFileUtils.getPath(this, fileUri);
                        Log.d("File Path", "" + filePath);
                        // Get the name of the selected file
                        fileName = fileUri.getLastPathSegment();
                        Log.d("File Name", fileName);
                        // Get the type of the selected file
                        getMimeType(fileName);
                        Log.d("File Mime", fileType);
                        if (fileType != Constants.FILE_TYPES.FILE_AUDIO_MPEG && fileType != Constants.FILE_TYPES.FILE_AUDIO_3GP && fileType != Constants.FILE_TYPES.FILE_AUDIO_AMR && fileType != Constants.FILE_TYPES.FILE_AUDIO_WAV && fileType != Constants.FILE_TYPES.FILE_AUDIO_OGG) {
                            Snackbar.make(activity_chat, "Sorry..!! Wrong file type chosen..!!", Snackbar.LENGTH_LONG).show();
                        } else {
                            // Get the size of the selected file
                            getFileSize(filePath);
                            Log.d("File Size", fileSize + "");
                            if (fileSize < Constants.FILE_SIZES.FILE_AUDIO_SIZE) {
                                ll_audio_picker.setVisibility(View.GONE);
                                fl_audio_preview.setVisibility(View.VISIBLE);
                                txt_audio_preview.setText(fileName);
                                cache = new File(Constants.FILES_APP.FOLDER_CBF_CACHE, fileName);
                                try {
                                    copyFile(filePath, cache, false);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Snackbar.make(activity_chat, "Sorry..!! The file size exceeds " + Constants.FILE_SIZES.FILE_ATTACH_SIZE + "MB..!!", Snackbar.LENGTH_LONG).show();
                            }
                        }
                    } catch (Exception e) {
                        Toast.makeText(this,"Please select the audio with a suitable file manager..!!",Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                }
                break;
            case ACTIVITY_CHOOSE_FILE:
                if (resultCode == RESULT_OK) {
                    // Get the Uri of the selected file
                    fileUri = data.getData();
                    Log.d("Content Uri", fileUri.toString());
                    if (data.getData().getScheme().equals("content")) {
                        Cursor cursor = getBaseContext().getContentResolver().query(fileUri, new String[]{android.provider.MediaStore.Images.ImageColumns.DATA}, null, null, null);
                        if (cursor.moveToFirst()) {
                            fileUri = Uri.parse("file://" + Uri.parse(cursor.getString(0)).getPath());
                        }
                        cursor.close();
                    }
                    Log.d("File Uri", "" + fileUri.toString());
                    // Get the path of the selected file
                    try {
                        filePath = messageFileUtils.getPath(this, fileUri);
                        Log.d("File Path", "" + filePath);
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                    // Get the name of the selected file
                    fileName = fileUri.getLastPathSegment();
                    Log.d("File Name", fileName);
                    // Get the type of the selected file
                    getMimeType(fileName);
                    Log.d("File Mime", fileType);
                    if (fileType != Constants.FILE_TYPES.FILE_PDF && fileType != Constants.FILE_TYPES.FILE_DOC && fileType != Constants.FILE_TYPES.FILE_XLS && fileType != Constants.FILE_TYPES.FILE_PPT) {
                        Snackbar.make(activity_chat, "Sorry..!! Wrong file type chosen..!!", Snackbar.LENGTH_LONG).show();
                    } else {
                        // Get the size of the selected file
                        getFileSize(filePath);
                        Log.d("File Size", fileSize + "");
                        if (fileSize < Constants.FILE_SIZES.FILE_ATTACH_SIZE) {
                            ll_file_picker.setVisibility(View.GONE);
                            fl_file_preview.setVisibility(View.VISIBLE);
                            txt_file_preview.setText(fileName);
                            cache = new File(Constants.FILES_APP.FOLDER_CBF_CACHE, fileName);
                            try {
                                copyFile(filePath, cache, false);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Snackbar.make(activity_chat, "Sorry..!! The file size exceeds " + Constants.FILE_SIZES.FILE_ATTACH_SIZE + "MB..!!", Snackbar.LENGTH_LONG).show();
                        }
                    }
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btn_new_note:
                msgType = 1;
                setMessagingLayouts(msgType);
                break;


            case R.id.btn_new_image:
                msgType = 2;
                setMessagingLayouts(msgType);
                break;
            case R.id.img_camera:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new Runnable() {
                        @Override
                        public void run() {
                            showFileChooser(msgType, 1);
                        }
                    }, Constants.PERMISSION_REQUESTS.REQUEST_CAMERA_PERMISSIONS);
                } else {
                    showFileChooser(msgType, 1);
                }
                break;
            case R.id.img_image_gallery:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new Runnable() {
                        @Override
                        public void run() {
                            showFileChooser(msgType, 2);
                        }
                    }, Constants.PERMISSION_REQUESTS.REQUEST_FILE_PERMISSIONS);
                } else {
                    showFileChooser(msgType, 2);
                }
                break;
            case R.id.ll_btn_close_image:
                removeFile(msgType);
                break;
            case R.id.img_rotate_left:
                scaledBm = rotateBitmap(scaledBm, -90);
                showImagePreview(scaledBm);
                break;
            case R.id.img_rotate_right:
                scaledBm = rotateBitmap(scaledBm, 90);
                showImagePreview(scaledBm);
                break;


            case R.id.btn_new_video:
                msgType = 3;
                setMessagingLayouts(msgType);
                break;
            case R.id.img_camcoder:
                vid_video_preview.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new Runnable() {
                        @Override
                        public void run() {
                            showFileChooser(msgType, 1);
                        }
                    }, Constants.PERMISSION_REQUESTS.REQUEST_CAMERA_PERMISSIONS);
                } else {
                    showFileChooser(msgType, 1);
                }
                break;
            case R.id.img_video_gallery:
                vid_video_preview.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new Runnable() {
                        @Override
                        public void run() {
                            showFileChooser(msgType, 2);
                        }
                    }, Constants.PERMISSION_REQUESTS.REQUEST_FILE_PERMISSIONS);
                } else {
                    showFileChooser(msgType, 2);
                }
                break;
            case R.id.ll_btn_close_video:
                removeFile(msgType);
                break;


            case R.id.btn_new_audio:
                msgType = 4;
                setMessagingLayouts(msgType);
                break;
            case R.id.img_microphone:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new Runnable() {
                        @Override
                        public void run() {
                            showFileChooser(msgType, 1);
                        }
                    }, Constants.PERMISSION_REQUESTS.REQUEST_CAMERA_PERMISSIONS);
                } else {
                    showFileChooser(msgType, 1);
                }
                break;
            case R.id.img_audio_gallery:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new Runnable() {
                        @Override
                        public void run() {
                            showFileChooser(msgType, 2);
                        }
                    }, Constants.PERMISSION_REQUESTS.REQUEST_FILE_PERMISSIONS);
                } else {
                    showFileChooser(msgType, 2);
                }
                break;
            case R.id.ll_btn_close_audio:
                removeFile(msgType);
                break;


            case R.id.btn_new_attcah:
                msgType = 5;
                setMessagingLayouts(msgType);
                break;
            case R.id.img_file_picker:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new Runnable() {
                        @Override
                        public void run() {
                            showFileChooser(msgType, 0);
                        }
                    }, Constants.PERMISSION_REQUESTS.REQUEST_FILE_PERMISSIONS);
                } else {
                    showFileChooser(msgType, 0);
                }
                break;
            case R.id.ll_btn_close_file:
                removeFile(msgType);
                break;


            case R.id.btn_send_message:
                float SBH = btn_send_message.getHeight();
                float SBW = btn_send_message.getWidth();
                anim = new RotateAnimation(0f, 360f, (SBH / 2), (SBW / 2));
                anim.setInterpolator(new LinearInterpolator());
                anim.setRepeatCount(Animation.INFINITE);
                anim.setDuration(1500);
                btn_send_message.startAnimation(anim);
                sendMessage(msgType);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (msgType != 0) {
            if (isFileSelected) {
                removeFile(msgType);
            }
            msgType = 0;
            ll_all_messages_blur.setVisibility(View.GONE);
            setMessagingLayouts(msgType);
            return;
        }
        if (fromMain) {
            startActivity(new Intent(ChatActivity.this, MainActivity.class));
        }
        finish();
    }
}
