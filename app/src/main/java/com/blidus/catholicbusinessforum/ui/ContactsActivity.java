package com.blidus.catholicbusinessforum.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blidus.catholicbusinessforum.App;
import com.blidus.catholicbusinessforum.R;
import com.blidus.catholicbusinessforum.adapters.dbAdapters.ContactDBAdapter;
import com.blidus.catholicbusinessforum.adapters.uiAdapters.ContactAdapter;
import com.blidus.catholicbusinessforum.apiCallbacks.ContactsList;
import com.blidus.catholicbusinessforum.dataModels.dbDatatypes.Contact;
import com.blidus.catholicbusinessforum.global.PreferenceKeys;
import com.blidus.catholicbusinessforum.helpers.EndlessRecyclerOnScrollListener;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ContactsActivity extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener {

    private DisplayMetrics displayMetrics;
    private static int maxID = 0, minID = 0;

    public static final String EXTRA_CONTACT_ID = "com.blidus.catholicbusinessforum.contactId";
    int finalpmaxpos = 0;
    int temppos = 0;
    int finalpminpos = 0;
    int finalpmaxid = 0;
    int finalpminid = 0;
    private boolean searched = false;
    private boolean searchBtnPressed = false;
    private boolean contactDetailview = false;

    private FrameLayout fl_actionbar, fl_all_contacts;
    private ImageView img_background;

    private LinearLayout ll_search, ll_search_form_outer, ll_search_form_title;
    private GradientDrawable search_bg, btn_contact_search_background, ll_search_form_outer_background;
    private ImageView img_search, img_add_contact;

    private TableLayout tl_search_form;
    private GradientDrawable tl_search_form_background, btn_contact_cancel_background;

    private ImageView img_search_shortcut;
    private EditText edt_contact_name, edt_company_name, edt_product_name;
    private Button btn_contact_search, btn_contact_cancel;
    private TextView txt_contact_empty_result;
    private InputMethodManager imm;
    private IBinder windowToken;

    private RecyclerView contactRecyclerView;
    private ArrayList<Contact> contacts;
    private ArrayList<Contact> searchedcontacts;
    private ContactAdapter contactAdapter;

    private int colorPrimary, colorAccent, colorBackgroundImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);
        if (App.preferences.getBoolean(PreferenceKeys.USER_DEFINED_THEME_COLOR, false)) {
            colorPrimary = Color.parseColor(App.preferences.getString(PreferenceKeys.USER_DEFINED_THEME_COLOR_PRIMARY, null));
            colorAccent = Color.parseColor(App.preferences.getString(PreferenceKeys.USER_DEFINED_THEME_COLOR_ACCENT, null));
            colorBackgroundImage = Color.parseColor(App.preferences.getString(PreferenceKeys.USER_DEFINED_THEME_COLOR_BACKGROUND_IMAGE, null));
        } else {
            colorPrimary = ContextCompat.getColor(this, R.color.colorPrimary);
            colorAccent = ContextCompat.getColor(this, R.color.colorAccent);
            colorBackgroundImage = ContextCompat.getColor(this, R.color.colorBackgroundImage);
        }
        //cntLoadFromOnlineDb(0);
        configView();
        displayContactsFromDb();
    }

    private void configView() {

        displayMetrics = getResources().getDisplayMetrics();

        fl_actionbar = (FrameLayout) findViewById(R.id.fl_actionbar);
        img_background = (ImageView) findViewById(R.id.img_background);

        img_add_contact = (ImageView) findViewById(R.id.img_add_contact);
        if (App.preferences.getInt(PreferenceKeys.LOGIN_TYPE, 0) == 1){
            img_add_contact.setVisibility(View.VISIBLE);
        } else {
            img_add_contact.setVisibility(View.GONE);
        }
        img_search = (ImageView) findViewById(R.id.img_search);
        ll_search = (LinearLayout) findViewById(R.id.ll_search);
        ll_search_form_outer = (LinearLayout) findViewById(R.id.ll_search_form_outer);
        ll_search_form_outer_background = (GradientDrawable) ll_search_form_outer.getBackground();
        ll_search_form_title = (LinearLayout) findViewById(R.id.ll_search_form_title);
        tl_search_form = (TableLayout) findViewById(R.id.tl_search_form);
        tl_search_form_background = (GradientDrawable) tl_search_form.getBackground();

        fl_all_contacts = (FrameLayout) findViewById(R.id.fl_all_contacts);

        img_search_shortcut = (ImageView) findViewById(R.id.img_search_shortcut);
        edt_contact_name = (EditText) findViewById(R.id.edt_contact_name);
        edt_company_name = (EditText) findViewById(R.id.edt_company_name);
        edt_product_name = (EditText) findViewById(R.id.edt_product_name);

        btn_contact_search = (Button) findViewById(R.id.btn_contact_search);
        btn_contact_search_background = (GradientDrawable) btn_contact_search.getBackground();
        btn_contact_cancel = (Button) findViewById(R.id.btn_contact_cancel);
        btn_contact_cancel_background = (GradientDrawable) btn_contact_cancel.getBackground();

        txt_contact_empty_result = (TextView) findViewById(R.id.txt_contact_empty_result);

        img_add_contact.setOnClickListener(this);
        img_search.setOnClickListener(this);
        btn_contact_search.setOnClickListener(this);
        btn_contact_cancel.setOnClickListener(this);

        img_search_shortcut.setOnClickListener(this);
        edt_contact_name.setOnTouchListener(this);
        edt_company_name.setOnTouchListener(this);
        edt_product_name.setOnTouchListener(this);

        setColorToElements();
    }

    private void setColorToElements() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(colorPrimary);
        }

        fl_actionbar.setBackgroundColor(colorPrimary);
        changeDrawableColor(img_background , colorBackgroundImage);
        ll_search.setBackgroundColor(colorAccent);
        ll_search_form_title.setBackgroundColor(colorPrimary);

        ll_search_form_outer_background.setColor(ContextCompat.getColor(this, R.color.colorWhite));
        ll_search_form_outer_background.setStroke(3, colorPrimary);
        tl_search_form_background.setColor(ContextCompat.getColor(this, R.color.colorWhite));
        tl_search_form_background.setStroke(3, colorPrimary);
        btn_contact_search_background.setColor(colorPrimary);
        btn_contact_cancel.setTextColor(colorPrimary);
        btn_contact_cancel_background.setColor(ContextCompat.getColor(this, R.color.colorWhite));
        btn_contact_cancel_background.setStroke(3, colorPrimary);
        edt_contact_name.setTextColor(colorPrimary);
        edt_contact_name.setHintTextColor(colorAccent);
        edt_company_name.setTextColor(colorPrimary);
        edt_company_name.setHintTextColor(colorAccent);
        edt_product_name.setTextColor(colorPrimary);
        edt_product_name.setHintTextColor(colorAccent);
    }

    private void changeDrawableColor(ImageView imageView, int color) {
        Drawable d = imageView.getDrawable();
        d.mutate();
        d.setColorFilter(color, PorterDuff.Mode.MULTIPLY);
        imageView.setImageDrawable(d);
    }

    public void cntLoadFromOnlineDb(int create) {
        if (create == 1) {
            create = App.preferences.getInt(PreferenceKeys.HIGHEST_CNT_ID, 0);
        }
        App.api.contactsget(App.preferences.getString(PreferenceKeys.TOKEN, null), create, new Callback<ContactsList>() {
            @Override
            public void success(final ContactsList contactsList, Response response) {
                if (contactsList.ErrorFlag == 0) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //img_pb_contacts.setVisibility(View.GONE);
                            if (contactsList.ContactsCount > 0) {
                                ContactDBAdapter dbAdapter = new ContactDBAdapter(getBaseContext());
                                dbAdapter.open();
                                dbAdapter.writeToDb(contactsList.Contacts);
                                maxID = App.preferences.getInt(PreferenceKeys.HIGHEST_CNT_ID, 0);
                                minID = App.preferences.getInt(PreferenceKeys.LOWEST_CNT_ID, 0);
                                if (minID != 0 && maxID != 0) {
                                    App.preferences.edit()
                                            .putInt(PreferenceKeys.HIGHEST_CNT_POS, ((maxID - minID) + 1))
                                            .putInt(PreferenceKeys.CURRENT_CNT_POS, 0)
                                            .apply();
                                }
                                dbAdapter.close();
                            }
                        }
                    });
                }
                if (contactsList.ErrorFlag == 1) {
                    if (contactsList.TokenFlag != 1) {
                        if (contactsList.TokenFlag == 2) {
                            Toast.makeText(ContactsActivity.this, "Invalid Login! Please Login Again!", Toast.LENGTH_SHORT).show();
                        } else if (contactsList.TokenFlag == 0) {
                            Toast.makeText(ContactsActivity.this, "Logged Out due to inactivity! Please Login Again!", Toast.LENGTH_SHORT).show();
                        }
                        App.preferences.edit()
                                .putString(PreferenceKeys.TOKEN, "")
                                .putInt(PreferenceKeys.LOGIN, PreferenceKeys.FALSE)
                                .putInt(PreferenceKeys.LOGIN_TYPE, PreferenceKeys.FALSE)
                                .apply();
                        startActivity(new Intent(ContactsActivity.this, LoginActivity.class));
                        finish();
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(ContactsActivity.this, "Can't connect to Server! Please check your Network Connection!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void displayContactsFromDb() {
        ContactDBAdapter dbAdapter = new ContactDBAdapter(this);
        dbAdapter.open();
        contacts = dbAdapter.getAllContacts();
        dbAdapter.close();
        contactAdapter = new ContactAdapter(this, contacts);

        if (contacts.size() == 0) {
            txt_contact_empty_result.setVisibility(View.VISIBLE);
        } else {
            txt_contact_empty_result.setVisibility(View.GONE);
        }

        finalpmaxpos = (App.preferences.getInt(PreferenceKeys.HIGHEST_CNT_POS, 0) - 1);
        temppos = App.preferences.getInt(PreferenceKeys.CURRENT_CNT_POS, 0);

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        contactRecyclerView = (RecyclerView) this.findViewById(R.id.rv_all_contacts);
        contactRecyclerView.setHasFixedSize(true);
        contactRecyclerView.setRecycledViewPool(new RecyclerView.RecycledViewPool());
        contactRecyclerView.setLayoutManager(linearLayoutManager);
        contactRecyclerView.setAdapter(contactAdapter);
        contactRecyclerView.scrollToPosition(temppos);
        App.preferences.edit().putInt(PreferenceKeys.CURRENT_CNT_POS, 0).apply();

        contactRecyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                //int itemPosition = ((LinearLayoutManager) messageRecyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition();
                //((LinearLayoutManager) messageRecyclerView.getLayoutManager()).scrollToPosition(itemPosition);
                if (App.preferences.getInt(PreferenceKeys.LOWEST_CNT_ID, PreferenceKeys.FALSE) != 1) {
                    //msgLoadMoreFromOnlineDb(0);
                }
            }
        });
    }

    public void searchedContactsFromDb() {

        try {
            searched = true;
            img_search.setImageResource(R.drawable.search);
            ll_search.setBackgroundColor(colorPrimary);
            /*img_search.setImageResource(R.drawable.searched);
            search_bg.setColor(ContextCompat.getColor(this, R.color.colorWhite));
            search_bg.setStroke(2, ContextCompat.getColor(this, R.color.colorPrimary));*/

            String Name = edt_contact_name.getText().toString();
            String Company = edt_company_name.getText().toString();
            String Product = edt_product_name.getText().toString();

            ContactDBAdapter dbAdapter = new ContactDBAdapter(this);
            dbAdapter.open();
            searchedcontacts = dbAdapter.getSearchedContacts(Name, Company, Product);
            dbAdapter.close();
            contactAdapter = new ContactAdapter(this, searchedcontacts);

            if (searchedcontacts.size() == 0) {
                txt_contact_empty_result.setVisibility(View.VISIBLE);
            } else {
                txt_contact_empty_result.setVisibility(View.GONE);
            }

            final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

            contactRecyclerView = (RecyclerView) this.findViewById(R.id.rv_all_contacts);
            contactRecyclerView.setHasFixedSize(true);
            contactRecyclerView.setRecycledViewPool(new RecyclerView.RecycledViewPool());
            contactRecyclerView.setLayoutManager(linearLayoutManager);
            contactRecyclerView.setAdapter(contactAdapter);
            contactRecyclerView.scrollToPosition(temppos);

            ll_search_form_outer.setVisibility(View.GONE);
            ll_search.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            Toast.makeText(this, "Unsupported characters !!", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        switch (v.getId()) {
            case R.id.img_add_contact:
                startActivity(new Intent(ContactsActivity.this, RegisterActivity.class));
                break;
            case R.id.img_search:
                searchBtnPressed = true;
                fl_all_contacts.setPadding(0, 0, 0, 0);
                ll_search_form_outer.setVisibility(View.VISIBLE);
                ll_search.setVisibility(View.GONE);
                break;
            case R.id.btn_contact_cancel:
                imm.hideSoftInputFromWindow(windowToken, 0);
                searchBtnPressed = false;
                fl_all_contacts.setPadding(0, 0, 0, ll_search.getHeight());
                ll_search_form_outer.setVisibility(View.GONE);
                ll_search.setVisibility(View.VISIBLE);
                break;
            case R.id.btn_contact_search:
                imm.hideSoftInputFromWindow(windowToken, 0);
                searchBtnPressed = false;
                fl_all_contacts.setPadding(0, 0, 0, ll_search.getHeight());
                searchedContactsFromDb();
                break;
            case R.id.img_search_shortcut:
                imm.hideSoftInputFromWindow(windowToken, 0);
                searchBtnPressed = false;
                fl_all_contacts.setPadding(0, 0, 0, ll_search.getHeight());
                searchedContactsFromDb();
                break;
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId()) {//edt_contact_name, edt_company_name, edt_product_name
            case R.id.edt_contact_name:
                windowToken = edt_contact_name.getWindowToken();
                break;
            case R.id.edt_company_name:
                windowToken = edt_company_name.getWindowToken();
                break;
            case R.id.edt_product_name:
                windowToken = edt_product_name.getWindowToken();
                break;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        if (searchBtnPressed) {
            searchBtnPressed = false;
            fl_all_contacts.setPadding(0, 0, 0, ll_search.getHeight());
            edt_contact_name.setText("");
            edt_company_name.setText("");
            edt_product_name.setText("");
            ll_search_form_outer.setVisibility(View.GONE);
            ll_search.setVisibility(View.VISIBLE);
            return;
        }
        if (searched) {
            displayContactsFromDb();
            searched = false;
            img_search.setImageResource(R.drawable.search);
            ll_search.setBackgroundColor(colorAccent);
            /*search_bg.setColor(ContextCompat.getColor(this, R.color.colorPrimary));
            search_bg.setStroke(2, ContextCompat.getColor(this, R.color.colorWhite));*/
            edt_contact_name.setText("");
            edt_company_name.setText("");
            edt_product_name.setText("");
            return;
        }
        startActivity(new Intent(ContactsActivity.this, MainActivity.class));
        finish();
    }
}
