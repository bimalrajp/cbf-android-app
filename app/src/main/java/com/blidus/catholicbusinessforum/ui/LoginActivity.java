package com.blidus.catholicbusinessforum.ui;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blidus.catholicbusinessforum.App;
import com.blidus.catholicbusinessforum.R;
import com.blidus.catholicbusinessforum.apiCallbacks.Login;
import com.blidus.catholicbusinessforum.global.Constants;
import com.blidus.catholicbusinessforum.global.PreferenceKeys;
import com.blidus.catholicbusinessforum.interfaces.API;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btn_login;
    private GradientDrawable btn_login_background;

    private Handler handler;
    private LinearLayout activity_login, ll_login_divider, ll_register_link;
    private TextView txt_register_link_text;
    private ImageView img_register_link_arrow1, img_register_link_arrow2, img_register_link_arrow3;
    private TextView txt_app_name_login;
    private EditText loginUsername, loginPassword;

    private boolean closeOnBackPress = false;

    private int colorPrimary, colorAccent, colorBackgroundImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        switch (App.preferences.getInt(PreferenceKeys.LOGIN, PreferenceKeys.FALSE)) {

            case PreferenceKeys.TRUE:
                startActivity(new Intent(this, MainActivity.class));
                finish();
                return;
        }
        if (App.preferences.getBoolean(PreferenceKeys.USER_DEFINED_THEME_COLOR, false)) {
            colorPrimary = Color.parseColor(App.preferences.getString(PreferenceKeys.USER_DEFINED_THEME_COLOR_PRIMARY, null));
            colorAccent = Color.parseColor(App.preferences.getString(PreferenceKeys.USER_DEFINED_THEME_COLOR_ACCENT, null));
            colorBackgroundImage = Color.parseColor(App.preferences.getString(PreferenceKeys.USER_DEFINED_THEME_COLOR_BACKGROUND_IMAGE, null));
        } else {
            colorPrimary = ContextCompat.getColor(this, R.color.colorPrimary);
            colorAccent = ContextCompat.getColor(this, R.color.colorAccent);
            colorBackgroundImage = ContextCompat.getColor(this, R.color.colorBackgroundImage);
        }
        setContentView(R.layout.activity_login);
        configView();
        if (!App.preferences.getBoolean(PreferenceKeys.FIRST_PERMISSION_REQUEST, false)) {
            App.preferences.edit().putBoolean(PreferenceKeys.MEDIA_AUTO_DOWNLOAD_IMAGE, true).apply();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                new AlertDialog.Builder(LoginActivity.this)
                        .setTitle("APPLICATION PERMISSIONS")
                        .setMessage("The application needs certain permissions for its proper functionality. Grant the permissions?")
                        .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                requestAllPermissions();
                                App.preferences.edit().putBoolean(PreferenceKeys.FIRST_PERMISSION_REQUEST, true).apply();
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                App.preferences.edit().putBoolean(PreferenceKeys.FIRST_PERMISSION_REQUEST, true).apply();
                                Snackbar.make(activity_login, "PERMISSION DENIAL MAY CAUSE ERRORS..!!", Snackbar.LENGTH_LONG).show();
                            }
                        })
                        .show();
            } else {
                App.preferences.edit()
                        .putBoolean(PreferenceKeys.PERMISSION_CAM, true)
                        .putBoolean(PreferenceKeys.PERMISSION_MIC, true)
                        .putBoolean(PreferenceKeys.PERMISSION_STR, true)
                        .putBoolean(PreferenceKeys.PERMISSION_CNT, true)
                        .putBoolean(PreferenceKeys.PERMISSION_CAL, true)
                        .putBoolean(PreferenceKeys.PERMISSION_SMS, true)
                        .putBoolean(PreferenceKeys.FIRST_PERMISSION_REQUEST, true)
                        .apply();
            }
        }
    }

    private void configView() {

        activity_login = (LinearLayout) findViewById(R.id.activity_login);

        ll_register_link = (LinearLayout) findViewById(R.id.ll_register_link);
        txt_register_link_text = (TextView) findViewById(R.id.txt_register_link_text);
        img_register_link_arrow1 = (ImageView) findViewById(R.id.img_register_link_arrow1);
        img_register_link_arrow2 = (ImageView) findViewById(R.id.img_register_link_arrow2);
        img_register_link_arrow3 = (ImageView) findViewById(R.id.img_register_link_arrow3);

        txt_app_name_login = (TextView) findViewById(R.id.txt_app_name_login);
        loginUsername = (EditText) findViewById(R.id.login_username);
        ll_login_divider = (LinearLayout) findViewById(R.id.ll_login_divider);
        loginPassword = (EditText) findViewById(R.id.login_password);
        btn_login = (Button) findViewById(R.id.btn_login);
        btn_login_background = (GradientDrawable) btn_login.getBackground();

        handler = new Handler();

        btn_login.setOnClickListener(this);
        ll_register_link.setOnClickListener(this);

        setColorToElements();
    }

    private void setColorToElements(){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(colorPrimary);
        }

        changeDrawableColor(img_register_link_arrow1, colorPrimary);
        changeDrawableColor(img_register_link_arrow2, colorPrimary);
        changeDrawableColor(img_register_link_arrow3, colorPrimary);
        txt_register_link_text.setTextColor(colorPrimary);

        txt_app_name_login.setTextColor(colorPrimary);

        btn_login_background.setColor(colorPrimary);
        ll_login_divider.setBackgroundColor(colorPrimary);
        loginUsername.setTextColor(colorPrimary);
        loginUsername.setHintTextColor(colorAccent);
        loginPassword.setTextColor(colorPrimary);
        loginPassword.setHintTextColor(colorAccent);

    }

    private void changeDrawableColor(ImageView imageView, int color) {
        Drawable d = imageView.getDrawable();
        d.mutate();
        d.setColorFilter(color, PorterDuff.Mode.MULTIPLY);
        imageView.setImageDrawable(d);
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void requestAllPermissions() {
        int cameraAccessPermissionStatus = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int microphoneAccessPermissionStatus = ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO);
        int readStoragePermissionStatus = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int writeStoragePermissionStatus = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int readContactsPermissionStatus = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS);
        int writeContactsPermissionStatus = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_CONTACTS);
        int callPhonePermissionStatus = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);
        int sendSmsPermissionStatus = ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS);

        if (cameraAccessPermissionStatus == PackageManager.PERMISSION_DENIED
                || microphoneAccessPermissionStatus == PackageManager.PERMISSION_DENIED
                || readStoragePermissionStatus == PackageManager.PERMISSION_DENIED
                || writeStoragePermissionStatus == PackageManager.PERMISSION_DENIED
                || readContactsPermissionStatus == PackageManager.PERMISSION_DENIED
                || writeContactsPermissionStatus == PackageManager.PERMISSION_DENIED
                || callPhonePermissionStatus == PackageManager.PERMISSION_DENIED
                || sendSmsPermissionStatus == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(this, new String[]{
                            Manifest.permission.CAMERA,
                            Manifest.permission.RECORD_AUDIO,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_CONTACTS,
                            Manifest.permission.WRITE_CONTACTS,
                            Manifest.permission.CALL_PHONE,
                            Manifest.permission.SEND_SMS},
                    Constants.PERMISSION_REQUESTS.REQUEST_ALL_PERMISSION);
        } else {
            App.preferences.edit()
                    .putBoolean(PreferenceKeys.PERMISSION_CAM, true)
                    .putBoolean(PreferenceKeys.PERMISSION_MIC, true)
                    .putBoolean(PreferenceKeys.PERMISSION_STR, true)
                    .putBoolean(PreferenceKeys.PERMISSION_CNT, true)
                    .putBoolean(PreferenceKeys.PERMISSION_CAL, true)
                    .putBoolean(PreferenceKeys.PERMISSION_SMS, true)
                    .apply();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Constants.PERMISSION_REQUESTS.REQUEST_ALL_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                App.preferences.edit().putBoolean(PreferenceKeys.PERMISSION_CAM, true).apply();
            } else {
                App.preferences.edit().putBoolean(PreferenceKeys.PERMISSION_CAM, false).apply();
            }
            if (grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                App.preferences.edit().putBoolean(PreferenceKeys.PERMISSION_MIC, true).apply();
            } else {
                App.preferences.edit().putBoolean(PreferenceKeys.PERMISSION_MIC, false).apply();
            }
            if (grantResults[2] == PackageManager.PERMISSION_GRANTED && grantResults[3] == PackageManager.PERMISSION_GRANTED) {
                App.preferences.edit().putBoolean(PreferenceKeys.PERMISSION_STR, true).apply();
            } else {
                App.preferences.edit().putBoolean(PreferenceKeys.PERMISSION_STR, false).apply();
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_login:
                /*startActivity(new Intent(LoginActivity.this,MainActivity.class));
                finish();*/
                String userName = loginUsername.getText().toString();
                String passWord = loginPassword.getText().toString();
                if (userName.length() == 0) {
                    Toast.makeText(LoginActivity.this, "Please provide your Username", Toast.LENGTH_SHORT).show();
                    break;
                }
                if (passWord.length() == 0) {
                    Toast.makeText(LoginActivity.this, "Please provide your Password", Toast.LENGTH_SHORT).show();
                    break;
                }
                App.api.login(userName, passWord, new Callback<Login>() {
                    @Override
                    public void success(final Login login, Response response) {
                        switch (login.LoginFlag) {
                            case API.LOGIN_FAILED:
                                break;
                            case API.LOGIN_SUCCESS:
                                switch (login.Status) {
                                    case API.STATUS_USER_ACTIVE:
                                        App.preferences.edit()
                                                .putInt(PreferenceKeys.LOGIN, PreferenceKeys.TRUE)
                                                .putString(PreferenceKeys.TOKEN, login.Token)
                                                .putInt(PreferenceKeys.LOGIN_ID, login.PKUserID)
                                                .putInt(PreferenceKeys.LOGIN_TYPE, login.LoginType)
                                                .putInt(PreferenceKeys.PROFILE_ACTIVE, login.Status)
                                                .putInt(PreferenceKeys.MESSAGE_ACTIVE, login.MessageFlag)
                                                /*.putInt(PreferenceKeys.LOWEST_MSG_ID, PreferenceKeys.FALSE)
                                                .putInt(PreferenceKeys.HIGHEST_MSG_ID, PreferenceKeys.FALSE)
                                                .putInt(PreferenceKeys.HIGHEST_MSG_POS, PreferenceKeys.FALSE)
                                                .putInt(PreferenceKeys.CURRENT_MSG_POS, PreferenceKeys.FALSE)
                                                .putInt(PreferenceKeys.LOWEST_CNT_ID, PreferenceKeys.FALSE)
                                                .putInt(PreferenceKeys.HIGHEST_CNT_ID, PreferenceKeys.FALSE)*/
                                                .apply();
                                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                        finish();
                                        break;
                                    case API.STATUS_USER_INACTIVE:
                                        Toast.makeText(LoginActivity.this, "Your account is in TEMPORARY INACTIVE STATE!", Toast.LENGTH_SHORT).show();
                                        App.preferences.edit()
                                                .putInt(PreferenceKeys.LOGIN, PreferenceKeys.FALSE)
                                                .apply();
                                        break;
                                }
                                break;
                            case API.LOGIN_INCORRECT_USER_CREDENTIALS:
                                Toast.makeText(LoginActivity.this, "Your Credentials are INVALID!", Toast.LENGTH_SHORT).show();
                                break;
                            case API.LOGIN_INCORRECT_PASSWORD:
                                Toast.makeText(LoginActivity.this, "You have entered INCORRECT PASSWORD!", Toast.LENGTH_SHORT).show();
                                break;
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Toast.makeText(LoginActivity.this, "Can't connect to Server! Please check your Network Connection!", Toast.LENGTH_SHORT).show();
                    }
                });
                break;
            case R.id.ll_register_link:
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
                finish();
                this.overridePendingTransition(R.anim.login_to_register_1, R.anim.login_to_register_2);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (!closeOnBackPress) {
            closeOnBackPress = true;
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    closeOnBackPress = false;
                }
            }, 3000);
            Toast.makeText(LoginActivity.this, "Press back button again to close", Toast.LENGTH_SHORT).show();
        } else {
            finish();
        }
    }
}
