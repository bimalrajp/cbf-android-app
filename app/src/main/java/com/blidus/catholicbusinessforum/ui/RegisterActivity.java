package com.blidus.catholicbusinessforum.ui;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.blidus.catholicbusinessforum.App;
import com.blidus.catholicbusinessforum.BuildConfig;
import com.blidus.catholicbusinessforum.R;
import com.blidus.catholicbusinessforum.adapters.uiAdapters.SpinnerAdapter;
import com.blidus.catholicbusinessforum.apiCallbacks.Login;
import com.blidus.catholicbusinessforum.global.Constants;
import com.blidus.catholicbusinessforum.global.PreferenceKeys;
import com.blidus.catholicbusinessforum.interfaces.API;
import com.blidus.catholicbusinessforum.utils.messageFileUtils;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int ACTIVITY_CAPTURE_IMAGE = 1;
    private static final int ACTIVITY_CHOOSE_IMAGE = 2;

    private boolean isFileSelected = false;
    private File source, cache, destination;
    private Bitmap msgImageBitmap, scaledBm;
    private FileOutputStream finalOutput = null;
    public static final int MAX_IMAGE_DIMENSION = 720;

    private Uri fileUri = null;
    private String filePath = null;
    private String fileName = null;
    private String fileType = null;
    private float fileSize = 0;

    private boolean allPermissionsSet = false;
    private Runnable runAfterPermissionGrant;
    private boolean imageFormOpen = false;

    private LinearLayout activity_register;

    private LinearLayout ll_login_link;
    private TextView txt_login_link_text;
    private ImageView img_login_link_arrow1, img_login_link_arrow2, img_login_link_arrow3;

    private LinearLayout ll_register;
    private ImageView img_app_logo_register;
    private TextView txt_app_name_register;
    private ImageView register_image;
    private LinearLayout ll_register_divider_1, ll_register_divider_2, ll_register_divider_3, ll_register_divider_4, ll_register_divider_5, ll_register_divider_6, ll_register_divider_7, ll_register_divider_8;
    private EditText register_name, register_mobile, register_email, register_company, register_product, register_descreption, register_username, register_password;
    private Spinner register_gender;
    private String[] gender_items;
    private Button btn_register;
    private GradientDrawable btn_register_background;

    private LinearLayout ll_camera_dialog, ll_camera_choices, ll_image_divider;
    private GradientDrawable ll_camera_choices_background;
    private ImageView img_reg_camera, img_reg_gallery;

    private int colorPrimary, colorAccent, colorBackgroundImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (App.preferences.getBoolean(PreferenceKeys.USER_DEFINED_THEME_COLOR, false)) {
            colorPrimary = Color.parseColor(App.preferences.getString(PreferenceKeys.USER_DEFINED_THEME_COLOR_PRIMARY, null));
            colorAccent = Color.parseColor(App.preferences.getString(PreferenceKeys.USER_DEFINED_THEME_COLOR_ACCENT, null));
            colorBackgroundImage = Color.parseColor(App.preferences.getString(PreferenceKeys.USER_DEFINED_THEME_COLOR_BACKGROUND_IMAGE, null));
        } else {
            colorPrimary = ContextCompat.getColor(this, R.color.colorPrimary);
            colorAccent = ContextCompat.getColor(this, R.color.colorAccent);
            colorBackgroundImage = ContextCompat.getColor(this, R.color.colorBackgroundImage);
        }
        setContentView(R.layout.activity_register);
        gender_items = new String[]{this.getResources().getString(R.string.prompt_register_gender), this.getResources().getString(R.string.prompt_register_gender_male), this.getResources().getString(R.string.prompt_register_gender_female)};
        configView();
    }

    private void configView() {

        activity_register = (LinearLayout) findViewById(R.id.activity_register);

        ll_login_link = (LinearLayout) findViewById(R.id.ll_login_link);
        txt_login_link_text = (TextView) findViewById(R.id.txt_login_link_text);
        img_login_link_arrow1 = (ImageView) findViewById(R.id.img_login_link_arrow1);
        img_login_link_arrow2 = (ImageView) findViewById(R.id.img_login_link_arrow2);
        img_login_link_arrow3 = (ImageView) findViewById(R.id.img_login_link_arrow3);

        img_app_logo_register = (ImageView) findViewById(R.id.img_app_logo_register);
        txt_app_name_register = (TextView) findViewById(R.id.txt_app_name_register);

        if (App.preferences.getString(PreferenceKeys.TOKEN, null) != null){
            if (App.preferences.getInt(PreferenceKeys.LOGIN_TYPE, 0) == 1){
                ll_login_link.setVisibility(View.GONE);
                img_app_logo_register.setVisibility(View.GONE);
                txt_app_name_register.setVisibility(View.GONE);
            }
        } else {
            ll_login_link.setVisibility(View.VISIBLE);
            img_app_logo_register.setVisibility(View.VISIBLE);
            txt_app_name_register.setVisibility(View.VISIBLE);
        }
        ll_register_divider_1 = (LinearLayout) findViewById(R.id.ll_register_divider_1);
        ll_register_divider_2 = (LinearLayout) findViewById(R.id.ll_register_divider_2);
        ll_register_divider_3 = (LinearLayout) findViewById(R.id.ll_register_divider_3);
        ll_register_divider_4 = (LinearLayout) findViewById(R.id.ll_register_divider_4);
        ll_register_divider_5 = (LinearLayout) findViewById(R.id.ll_register_divider_5);
        ll_register_divider_6 = (LinearLayout) findViewById(R.id.ll_register_divider_6);
        ll_register_divider_7 = (LinearLayout) findViewById(R.id.ll_register_divider_7);
        ll_register_divider_8 = (LinearLayout) findViewById(R.id.ll_register_divider_8);

        ll_register = (LinearLayout) findViewById(R.id.ll_register);
        register_image = (ImageView) findViewById(R.id.register_image);
        register_name = (EditText) findViewById(R.id.register_name);
        register_gender = (Spinner) findViewById(R.id.register_gender);
        register_mobile = (EditText) findViewById(R.id.register_mobile);
        register_email = (EditText) findViewById(R.id.register_email);
        register_company = (EditText) findViewById(R.id.register_company);
        register_product = (EditText) findViewById(R.id.register_product);
        register_descreption = (EditText) findViewById(R.id.register_descreption);
        register_username = (EditText) findViewById(R.id.register_username);
        register_password = (EditText) findViewById(R.id.register_password);

        btn_register = (Button) findViewById(R.id.btn_register);
        btn_register_background = (GradientDrawable) btn_register.getBackground();

        ArrayAdapter adapter = new SpinnerAdapter(this, gender_items);
        register_gender.setAdapter(adapter);

        ll_camera_dialog = (LinearLayout) findViewById(R.id.ll_camera_dialog);
        ll_camera_choices = (LinearLayout) findViewById(R.id.ll_camera_choices);
        ll_camera_choices_background = (GradientDrawable) ll_camera_choices.getBackground();
        ll_image_divider = (LinearLayout) findViewById(R.id.ll_image_divider);
        img_reg_camera = (ImageView) findViewById(R.id.img_reg_camera);
        img_reg_gallery = (ImageView) findViewById(R.id.img_reg_gallery);

        if (App.preferences.getString(PreferenceKeys.TOKEN, null) != null){
            if (App.preferences.getInt(PreferenceKeys.LOGIN_TYPE, 0) == 1){
                btn_register.setText(this.getResources().getString(R.string.button_add_contact));
            }
        } else {
            btn_register.setText(this.getResources().getString(R.string.button_register));
        }

        ll_login_link.setOnClickListener(this);
        register_image.setOnClickListener(this);
        img_reg_camera.setOnClickListener(this);
        img_reg_gallery.setOnClickListener(this);
        btn_register.setOnClickListener(this);

        setColorToElements();
    }

    private void setColorToElements() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(colorPrimary);
        }

        changeDrawableColor(img_login_link_arrow1, colorPrimary);
        changeDrawableColor(img_login_link_arrow2, colorPrimary);
        changeDrawableColor(img_login_link_arrow3, colorPrimary);
        txt_login_link_text.setTextColor(colorPrimary);

        txt_app_name_register.setTextColor(colorPrimary);
        ll_register_divider_1.setBackgroundColor(colorPrimary);
        ll_register_divider_2.setBackgroundColor(colorPrimary);
        ll_register_divider_3.setBackgroundColor(colorPrimary);
        ll_register_divider_4.setBackgroundColor(colorPrimary);
        ll_register_divider_5.setBackgroundColor(colorPrimary);
        ll_register_divider_6.setBackgroundColor(colorPrimary);
        ll_register_divider_7.setBackgroundColor(colorPrimary);
        ll_register_divider_8.setBackgroundColor(colorPrimary);

        changeDrawableColor(register_image, colorPrimary);
        register_name.setTextColor(colorPrimary);
        register_name.setHintTextColor(colorAccent);
        register_mobile.setTextColor(colorPrimary);
        register_mobile.setHintTextColor(colorAccent);
        register_email.setTextColor(colorPrimary);
        register_email.setHintTextColor(colorAccent);
        register_company.setTextColor(colorPrimary);
        register_company.setHintTextColor(colorAccent);
        register_product.setTextColor(colorPrimary);
        register_product.setHintTextColor(colorAccent);
        register_descreption.setTextColor(colorPrimary);
        register_descreption.setHintTextColor(colorAccent);
        register_username.setTextColor(colorPrimary);
        register_username.setHintTextColor(colorAccent);
        register_password.setTextColor(colorPrimary);
        register_password.setHintTextColor(colorAccent);
        btn_register_background.setColor(colorPrimary);

        ll_camera_choices_background.setStroke(3, colorPrimary);
        ll_image_divider.setBackgroundColor(colorPrimary);
        changeDrawableColor(img_reg_camera, colorPrimary);
        changeDrawableColor(img_reg_gallery, colorPrimary);
    }

    private void changeDrawableColor(ImageView imageView, int color) {
        Drawable d = imageView.getDrawable();
        d.mutate();
        d.setColorFilter(color, PorterDuff.Mode.MULTIPLY);
        imageView.setImageDrawable(d);
    }

    private void showImagePicker(boolean img) {
        if (!img) {
            imageFormOpen = true;
            ll_camera_dialog.setVisibility(View.VISIBLE);
        } else {
            imageFormOpen = false;
            ll_camera_dialog.setVisibility(View.GONE);
        }
    }

    private void showFileChooser(int imgType) {
        fileName = "PRO_IMG.jpg";
        switch (imgType) {
            case 1:
                try {
                    /*Calendar cal = Calendar.getInstance();
                    fileName = String.format("PRF_IMG_%d_%d_%d_%d_%d_%d.jpg", cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.YEAR), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));*/
                    Constants.FILES_APP.FOLDER_CBF_CACHE.mkdirs();
                    cache = new File(Constants.FILES_APP.FOLDER_CBF_CACHE, fileName);
                    filePath = cache.getAbsolutePath();
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(RegisterActivity.this, BuildConfig.APPLICATION_ID + ".provider", cache));
                    } else {
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(cache));
                    }
                    startActivityForResult(intent, ACTIVITY_CAPTURE_IMAGE);
                } catch (Exception e) {
                    Toast.makeText(this, "Please install a suitable Camera application..!!", Toast.LENGTH_SHORT).show();
                    Log.d("Exception", e.getMessage());
                }
                break;
            case 2:
                try {
                    Toast.makeText(this, "Select IMAGE files", Toast.LENGTH_LONG).show();
                    Intent mRequestImageIntent = new Intent(Intent.ACTION_PICK);
                    mRequestImageIntent.setType(Constants.FILE_TYPES.FILE_IMAGE);
                    startActivityForResult(Intent.createChooser(mRequestImageIntent, "Select an Image to upload"), ACTIVITY_CHOOSE_IMAGE);
                } catch (android.content.ActivityNotFoundException e) {
                    Toast.makeText(this, "Please install a suitable File Manager application..!!", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void copyFile(String sourcePath, File destination, boolean delete) throws IOException {
        try {
            source = new File(sourcePath);
            moveFile(source, destination, delete);
            isFileSelected = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void moveFile(File sourceFile, File destinationFile, boolean delete) {
        try {
            FileUtils.copyFile(sourceFile, destinationFile);
            if (delete) {
                sourceFile.delete();
                isFileSelected = false;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void getMimeType(String Filename) {
        try {
            Filename = Filename.toLowerCase();
            //Filename = Filename.replace(" ", "");
            Filename = Filename.replaceAll("[^a-zA-Z0-9.-]", "_");
            //String extension = Filename.substring(Filename.lastIndexOf("."));
            String extension = MimeTypeMap.getFileExtensionFromUrl(Filename);
            Log.d("File Extension", extension);
            if (extension != null) {
                fileType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getFileSize(String passedPath) {
        try {
            source = new File(passedPath);
            fileSize = (Float.parseFloat(String.valueOf(source.length() / Constants.FILE_SIZES.ONE_MB)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Bitmap resizeBitmap(Bitmap bm) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        //Log.d(TAG, "resizeBitmap w: " + width + " h: " + height);
        int targetWidth;
        int targetHeight;
        float scaleFactor;
        if (width > height) {
            scaleFactor = ((float) MAX_IMAGE_DIMENSION) / ((float) width);
            targetWidth = MAX_IMAGE_DIMENSION;
            targetHeight = (int) (height * scaleFactor);
        } else {
            scaleFactor = ((float) MAX_IMAGE_DIMENSION) / ((float) height);
            targetHeight = MAX_IMAGE_DIMENSION;
            targetWidth = (int) (width * scaleFactor);
        }
        return Bitmap.createScaledBitmap(bm, targetWidth, targetHeight, true);
    }

    private void showImagePreview(final Bitmap bm) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    finalOutput = new FileOutputStream(cache.getAbsolutePath());
                    bm.compress(Bitmap.CompressFormat.PNG, 100, finalOutput);
                    finalOutput.close();
                    register_image.setImageBitmap(bm);
                    showImagePicker(imageFormOpen);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void requestAllPermissions(Runnable ifAllSetDoWhat) {
        runAfterPermissionGrant = ifAllSetDoWhat;
        int cameraAccessPermissionStatus = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int readStoragePermissionStatus = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int writeStoragePermissionStatus = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (cameraAccessPermissionStatus == PackageManager.PERMISSION_DENIED
                || readStoragePermissionStatus == PackageManager.PERMISSION_DENIED
                || writeStoragePermissionStatus == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(this, new String[]{
                            Manifest.permission.CAMERA,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    Constants.PERMISSION_REQUESTS.REQUEST_ALL_PERMISSION);
        } else {
            App.preferences.edit()
                    .putBoolean(PreferenceKeys.PERMISSION_CAM, true)
                    .putBoolean(PreferenceKeys.PERMISSION_STR, true)
                    .apply();
            allPermissionsSet = true;
        }
        if (allPermissionsSet) {
            if (runAfterPermissionGrant != null) {
                runAfterPermissionGrant.run();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Constants.PERMISSION_REQUESTS.REQUEST_ALL_PERMISSION) {
            boolean camera = false;
            boolean storage = false;
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                App.preferences.edit().putBoolean(PreferenceKeys.PERMISSION_CAM, true).apply();
                camera = true;
            } else {
                App.preferences.edit().putBoolean(PreferenceKeys.PERMISSION_CAM, false).apply();
                camera = false;
            }
            if (grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                App.preferences.edit().putBoolean(PreferenceKeys.PERMISSION_STR, true).apply();
                storage = true;
            } else {
                App.preferences.edit().putBoolean(PreferenceKeys.PERMISSION_STR, false).apply();
                storage = false;
            }
            if (camera && storage) {
                allPermissionsSet = true;
            }
            if (allPermissionsSet) {
                if (runAfterPermissionGrant != null) {
                    runAfterPermissionGrant.run();
                }
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case ACTIVITY_CAPTURE_IMAGE:
                try {
                    if (cache.exists()) {
                        Log.d("File Path", cache.getAbsolutePath());
                        msgImageBitmap = BitmapFactory.decodeFile(cache.getAbsolutePath());
                        scaledBm = resizeBitmap(msgImageBitmap);
                        showImagePreview(scaledBm);
                        getMimeType(cache.getName());
                        isFileSelected = true;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case ACTIVITY_CHOOSE_IMAGE:
                try {
                    if (resultCode == RESULT_OK) {
                        // Get the Uri of the selected file
                        fileUri = data.getData();
                        Log.d("Content Uri", fileUri.toString());
                        if (data.getData().getScheme().equals("content")) {
                            Cursor cursor = getBaseContext().getContentResolver().query(fileUri, new String[]{android.provider.MediaStore.Images.ImageColumns.DATA}, null, null, null);
                            if (cursor.moveToFirst()) {
                                fileUri = Uri.parse("file://" + Uri.parse(cursor.getString(0)).getPath());
                            }
                            cursor.close();
                        }
                        Log.d("File Uri", "" + fileUri.toString());
                        // Get the path of the selected file
                        try {
                            filePath = messageFileUtils.getPath(this, fileUri);
                            Log.d("File Path", "" + filePath);
                        } catch (URISyntaxException e) {
                            e.printStackTrace();
                        }
                        // Get the name of the selected file
                        String tempfileName = fileUri.getLastPathSegment();
                        Log.d("File Name", tempfileName);
                        // Get the type of the selected file
                        getMimeType(tempfileName);
                        Log.d("File Mime", fileType);
                        if (fileType != Constants.FILE_TYPES.FILE_IMAGE_JPEG && fileType != Constants.FILE_TYPES.FILE_IMAGE_JPG && fileType != Constants.FILE_TYPES.FILE_IMAGE_PNG) {
                            Snackbar.make(activity_register, "Sorry..!! Wrong file type chosen..!!", Snackbar.LENGTH_LONG).show();
                        } else {
                            // Get the size of the selected file
                            getFileSize(filePath);
                            Log.d("File Size", fileSize + "");
                            if (fileSize < Constants.FILE_SIZES.FILE_IMAGE_SIZE) {
                                cache = new File(Constants.FILES_APP.FOLDER_CBF_CACHE, fileName);
                                try {
                                    copyFile(filePath, cache, false);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                if (cache.exists()) {
                                    msgImageBitmap = BitmapFactory.decodeFile(cache.getAbsolutePath());
                                    scaledBm = resizeBitmap(msgImageBitmap);
                                    showImagePreview(scaledBm);
                                }
                            } else {
                                Snackbar.make(activity_register, "Sorry..!! The file size exceeds " + Constants.FILE_SIZES.FILE_IMAGE_SIZE + "MB..!!", Snackbar.LENGTH_LONG).show();
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public static boolean isEmailValid(String Email) {
        boolean isValid = false;
        try {
            String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
            CharSequence inputStr = Email;

            Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(inputStr);
            if (matcher.matches()) {
                isValid = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isValid;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_login_link:
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                if (cache != null) {
                    cache.delete();
                }
                finish();
                this.overridePendingTransition(R.anim.register_to_login_1, R.anim.register_to_login_2);
                break;
            case R.id.register_image:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestAllPermissions(new Runnable() {
                        @Override
                        public void run() {
                            showImagePicker(imageFormOpen);
                        }
                    });
                } else {
                    showImagePicker(imageFormOpen);
                }
                break;
            case R.id.img_reg_camera:
                showFileChooser(1);
                break;
            case R.id.img_reg_gallery:
                showFileChooser(2);
                break;
            case R.id.btn_register:
                boolean allFieldsOK = true;
                if (cache == null) {
                    Toast.makeText(this, "Provide a User Image..!!", Toast.LENGTH_SHORT).show();
                    allFieldsOK = false;
                    break;
                }
                String Name = null;
                Name = register_name.getText().toString();
                if (Name.length() == 0) {
                    Toast.makeText(this, "Provide a valid Name..!!", Toast.LENGTH_SHORT).show();
                    allFieldsOK = false;
                    break;
                }
                String Gender = null;
                Gender = register_gender.getSelectedItem().toString().toLowerCase();
                if (Gender.equals("gender")) {
                    Toast.makeText(this, "Provide a valid Gender..!!", Toast.LENGTH_SHORT).show();
                    allFieldsOK = false;
                    break;
                }
                String Mobile = null;
                Mobile = register_mobile.getText().toString();
                if (Mobile.length() == 0 || !(Mobile.matches("[0-9]{10}"))) {
                    Toast.makeText(this, "Provide a valid Mobile Number..!!", Toast.LENGTH_SHORT).show();
                    allFieldsOK = false;
                    break;
                }
                String Email = null;
                Email = register_email.getText().toString();
                boolean validEmail = isEmailValid(Email);
                if (Email.length() == 0 || !validEmail) {
                    Toast.makeText(this, "Provide a valid Email Address..!!", Toast.LENGTH_SHORT).show();
                    allFieldsOK = false;
                    break;
                }
                String CompanyName = null;
                CompanyName = register_company.getText().toString();
                if (CompanyName.length() == 0) {
                    Toast.makeText(this, "Provide a valid Company Name..!!", Toast.LENGTH_SHORT).show();
                    allFieldsOK = false;
                    break;
                }
                String ProductName = null;
                ProductName = register_product.getText().toString();
                if (ProductName.length() == 0) {
                    Toast.makeText(this, "Provide a valid Product Name..!!", Toast.LENGTH_SHORT).show();
                    allFieldsOK = false;
                    break;
                 }
                String UserDescription = null;
                UserDescription = register_descreption.getText().toString();
                if (UserDescription.length() == 0) {
                    Toast.makeText(this, "Provide a valid User Description..!!", Toast.LENGTH_SHORT).show();
                    allFieldsOK = false;
                    break;
                }
                String RUsername = null;
                RUsername = register_username.getText().toString();
                if (RUsername.length() == 0) {
                    Toast.makeText(this, "Provide a valid Username..!!", Toast.LENGTH_SHORT).show();
                    allFieldsOK = false;
                    break;
                }
                String RPassword = null;
                RPassword = register_password.getText().toString();
                if (RPassword.length() == 0) {
                    Toast.makeText(this, "Provide a valid Password..!!", Toast.LENGTH_SHORT).show();
                    allFieldsOK = false;
                    break;
                }
                if (allFieldsOK) {
                    App.api.register(
                            new TypedString(Name),
                            new TypedString(Gender),
                            new TypedString(Mobile),
                            new TypedString(Email),
                            new TypedString(CompanyName),
                            new TypedString(ProductName),
                            new TypedString(UserDescription),
                            new TypedString(RUsername),
                            new TypedString(RPassword),
                            new TypedFile(fileType, cache),
                            new Callback<Login>() {
                                @Override
                                public void success(Login login, Response response) {
                                    try {
                                        if (login.RegisterFlag == 1) {
                                            if (App.preferences.getString(PreferenceKeys.TOKEN, null) != null){
                                                if (App.preferences.getInt(PreferenceKeys.LOGIN_TYPE, 0) == 1){
                                                    startActivity(new Intent(RegisterActivity.this, ContactsActivity.class));
                                                    if (cache != null) {
                                                        cache.delete();
                                                    }
                                                    finish();
                                                }
                                            } else {
                                                switch (login.LoginFlag) {
                                                    case API.LOGIN_FAILED:
                                                        break;
                                                    case API.LOGIN_SUCCESS:
                                                        switch (login.Status) {
                                                            case API.STATUS_USER_ACTIVE:
                                                                App.preferences.edit()
                                                                        .putInt(PreferenceKeys.LOGIN, PreferenceKeys.TRUE)
                                                                        .putString(PreferenceKeys.TOKEN, login.Token)
                                                                        .putInt(PreferenceKeys.LOGIN_ID, login.PKUserID)
                                                                        .putInt(PreferenceKeys.LOGIN_TYPE, login.LoginType)
                                                                        .putInt(PreferenceKeys.PROFILE_ACTIVE, login.Status)
                                                                        .putInt(PreferenceKeys.MESSAGE_ACTIVE, login.MessageFlag)
                                                                        .apply();
                                                                startActivity(new Intent(RegisterActivity.this, MainActivity.class));
                                                                finish();
                                                                break;
                                                            case API.STATUS_USER_INACTIVE:
                                                                Toast.makeText(RegisterActivity.this, "Your account is in TEMPORARY INACTIVE STATE!", Toast.LENGTH_SHORT).show();
                                                                App.preferences.edit()
                                                                        .putInt(PreferenceKeys.LOGIN, PreferenceKeys.FALSE)
                                                                        .apply();
                                                                break;
                                                        }
                                                        break;
                                                    case API.LOGIN_INCORRECT_USER_CREDENTIALS:
                                                        Toast.makeText(RegisterActivity.this, "Your Credentials are INVALID!", Toast.LENGTH_SHORT).show();
                                                        break;
                                                    case API.LOGIN_INCORRECT_PASSWORD:
                                                        Toast.makeText(RegisterActivity.this, "You have entered INCORRECT PASSWORD!", Toast.LENGTH_SHORT).show();
                                                        break;
                                                }
                                            }
                                        } else {
                                            Toast.makeText(RegisterActivity.this, "Sorry..!! Unable to register now. Please try after a short while..!!", Toast.LENGTH_SHORT).show();
                                        }
                                        if (cache != null) {
                                            cache.delete();
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }

                                @Override
                                public void failure(RetrofitError error) {
                                    try {
                                        Toast.makeText(RegisterActivity.this, "Can't connect to Server! Please check your Network Connection!", Toast.LENGTH_SHORT).show();
                                        if (cache != null) {
                                            cache.delete();
                                        }
                                    } catch (Exception e){
                                        e.printStackTrace();
                                    }
                                }
                            });
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (imageFormOpen) {
            showImagePicker(imageFormOpen);
            return;
        } else {
            if (App.preferences.getString(PreferenceKeys.TOKEN, null) != null){
                if (App.preferences.getInt(PreferenceKeys.LOGIN_TYPE, 0) == 1){
                    startActivity(new Intent(RegisterActivity.this, ContactsActivity.class));
                    if (cache != null) {
                        cache.delete();
                    }
                    finish();
                }
            } else {
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                if (cache != null) {
                    cache.delete();
                }
                finish();
                this.overridePendingTransition(R.anim.register_to_login_1, R.anim.register_to_login_2);
            }
        }
    }
}