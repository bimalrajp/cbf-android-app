package com.blidus.catholicbusinessforum.ui;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.annotation.RequiresApi;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.webkit.MimeTypeMap;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.blidus.catholicbusinessforum.App;
import com.blidus.catholicbusinessforum.BuildConfig;
import com.blidus.catholicbusinessforum.R;
import com.blidus.catholicbusinessforum.adapters.dbAdapters.ContactDBAdapter;
import com.blidus.catholicbusinessforum.adapters.uiAdapters.SpinnerAdapter;
import com.blidus.catholicbusinessforum.apiCallbacks.ProfileUpdate;
import com.blidus.catholicbusinessforum.dataModels.dbDatatypes.Contact;
import com.blidus.catholicbusinessforum.global.Constants;
import com.blidus.catholicbusinessforum.global.PreferenceKeys;
import com.blidus.catholicbusinessforum.utils.messageFileUtils;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.msebera.android.httpclient.Header;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;

import static com.blidus.catholicbusinessforum.ui.ContactDetailActivity.EXTRA_EDIT_CONTACT_ID;
import static com.blidus.catholicbusinessforum.ui.ContactsActivity.EXTRA_CONTACT_ID;

public class ProfileEditActivity extends AppCompatActivity implements View.OnClickListener {

    private int colorPrimary, colorAccent, colorBackgroundImage;

    private static final int ACTIVITY_CAPTURE_IMAGE = 1;
    private static final int ACTIVITY_CHOOSE_IMAGE = 2;

    private boolean updatePressed = false;

    private boolean isFileSelected = false;
    private File source, cache, destination;
    private Bitmap msgImageBitmap, scaledBm;
    private FileOutputStream finalOutput = null;
    public static final int MAX_IMAGE_DIMENSION = 720;

    private Uri fileUri = null;
    private String filePath = null;
    private String fileName = null;
    private String fileType = null;
    private float fileSize = 0;

    private LinearLayout activity_profile_edit;

    private LinearLayout ll_actionbar;

    private ImageView img_contact;
    private EditText edttxt_contact_fullname, edttxt_contact_company_name, edttxt_contact_product_name, edttxt_contact_house_name, edttxt_contact_address, edttxt_contact_city, edttxt_contact_mobile, edttxt_contact_email, edttxt_contact_descreption;
    private Spinner spnr_contact_gender;
    private String[] gender_items;
    private LinearLayout ll_save_profile;
    private GradientDrawable ll_save_profile_background;

    private LinearLayout ll_camera_dialog, ll_image_divider, ll_camera_choices;
    private GradientDrawable ll_camera_choices_background;
    private ImageView img_pro_edit_camera, img_pro_edit_gallery;

    private LinearLayout ll_profile_update_animation;
    private ImageView img_profile_update_logo;
    private TextView txt_profile_update_text;

    private Contact contact;
    private Runnable runAfterStoragePermissionGrant;
    private Runnable runAfterPermissionGrant;
    private boolean allPermissionsSet = false;
    private boolean imageFormOpen = false;
    private String userProfilePicPath;
    private AsyncHttpClient http;

    private Intent intent;
    private Integer userID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_edit);
        if (App.preferences.getBoolean(PreferenceKeys.USER_DEFINED_THEME_COLOR, false)) {
            colorPrimary = Color.parseColor(App.preferences.getString(PreferenceKeys.USER_DEFINED_THEME_COLOR_PRIMARY, null));
            colorAccent = Color.parseColor(App.preferences.getString(PreferenceKeys.USER_DEFINED_THEME_COLOR_ACCENT, null));
            colorBackgroundImage = Color.parseColor(App.preferences.getString(PreferenceKeys.USER_DEFINED_THEME_COLOR_BACKGROUND_IMAGE, null));
        } else {
            colorPrimary = ContextCompat.getColor(this, R.color.colorPrimary);
            colorAccent = ContextCompat.getColor(this, R.color.colorAccent);
            colorBackgroundImage = ContextCompat.getColor(this, R.color.colorBackgroundImage);
        }
        gender_items = new String[]{this.getResources().getString(R.string.prompt_register_gender), this.getResources().getString(R.string.prompt_register_gender_male), this.getResources().getString(R.string.prompt_register_gender_female)};
        configView();
        intent = getIntent();
        userID = intent.getIntExtra(EXTRA_EDIT_CONTACT_ID, App.preferences.getInt(PreferenceKeys.LOGIN_ID, 0));
        displayContactFromDb(userID);
        //displayContactFromDb(App.preferences.getInt(PreferenceKeys.LOGIN_ID, 0));
    }

    private void configView() {

        activity_profile_edit = (LinearLayout) findViewById(R.id.activity_profile_edit);

        ll_actionbar = (LinearLayout) findViewById(R.id.ll_actionbar);

        img_contact = (ImageView) findViewById(R.id.img_contact);
        edttxt_contact_fullname = (EditText) findViewById(R.id.edttxt_contact_fullname);
        spnr_contact_gender = (Spinner) findViewById(R.id.spnr_contact_gender);
        edttxt_contact_company_name = (EditText) findViewById(R.id.edttxt_contact_company_name);
        edttxt_contact_product_name = (EditText) findViewById(R.id.edttxt_contact_product_name);
        edttxt_contact_house_name = (EditText) findViewById(R.id.edttxt_contact_house_name);
        edttxt_contact_address = (EditText) findViewById(R.id.edttxt_contact_address);
        edttxt_contact_city = (EditText) findViewById(R.id.edttxt_contact_city);
        edttxt_contact_mobile = (EditText) findViewById(R.id.edttxt_contact_mobile);
        edttxt_contact_email = (EditText) findViewById(R.id.edttxt_contact_email);
        edttxt_contact_descreption = (EditText) findViewById(R.id.edttxt_contact_descreption);

        ArrayAdapter adapter = new SpinnerAdapter(this, gender_items);
        spnr_contact_gender.setAdapter(adapter);

        ll_camera_dialog = (LinearLayout) findViewById(R.id.ll_camera_dialog);
        ll_camera_choices = (LinearLayout) findViewById(R.id.ll_camera_choices);
        ll_camera_choices_background = (GradientDrawable) ll_camera_choices.getBackground();
        ll_image_divider = (LinearLayout) findViewById(R.id.ll_image_divider);
        img_pro_edit_camera = (ImageView) findViewById(R.id.img_pro_edit_camera);
        img_pro_edit_gallery = (ImageView) findViewById(R.id.img_pro_edit_gallery);

        ll_save_profile = (LinearLayout) findViewById(R.id.ll_save_profile);
        ll_save_profile_background = (GradientDrawable) ll_save_profile.getBackground();

        ll_profile_update_animation = (LinearLayout) findViewById(R.id.ll_profile_update_animation);
        img_profile_update_logo = (ImageView) findViewById(R.id.img_profile_update_logo);
        txt_profile_update_text = (TextView) findViewById(R.id.txt_profile_update_text);

        rotate360(img_profile_update_logo, 2500);

        http = new AsyncHttpClient();

        setColorToElements();

        img_contact.setOnClickListener(this);
        img_pro_edit_camera.setOnClickListener(this);
        img_pro_edit_gallery.setOnClickListener(this);
        ll_save_profile.setOnClickListener(this);
    }

    private void setColorToElements() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(colorPrimary);
        }

        ll_actionbar.setBackgroundColor(colorPrimary);

        edttxt_contact_fullname.setTextColor(colorPrimary);
        edttxt_contact_fullname.setHintTextColor(colorAccent);
        edttxt_contact_company_name.setTextColor(colorPrimary);
        edttxt_contact_company_name.setHintTextColor(colorAccent);
        edttxt_contact_product_name.setTextColor(colorPrimary);
        edttxt_contact_product_name.setHintTextColor(colorAccent);
        edttxt_contact_house_name.setTextColor(colorPrimary);
        edttxt_contact_house_name.setHintTextColor(colorAccent);
        edttxt_contact_address.setTextColor(colorPrimary);
        edttxt_contact_address.setHintTextColor(colorAccent);
        edttxt_contact_city.setTextColor(colorPrimary);
        edttxt_contact_city.setHintTextColor(colorAccent);
        edttxt_contact_mobile.setTextColor(colorPrimary);
        edttxt_contact_mobile.setHintTextColor(colorAccent);
        edttxt_contact_email.setTextColor(colorPrimary);
        edttxt_contact_email.setHintTextColor(colorAccent);
        edttxt_contact_descreption.setTextColor(colorPrimary);
        edttxt_contact_descreption.setHintTextColor(colorAccent);

        ll_save_profile_background.setColor(colorPrimary);

        ll_camera_choices_background.setColor(ContextCompat.getColor(this, R.color.colorWhite));
        ll_camera_choices_background.setStroke(3, colorPrimary);
        ll_image_divider.setBackgroundColor(colorPrimary);
        changeDrawableColor(img_pro_edit_camera, colorPrimary);
        changeDrawableColor(img_pro_edit_gallery, colorPrimary);

        changeDrawableToGrayscale(img_profile_update_logo);
    }

    private void changeDrawableColor(ImageView imageView, int color) {
        Drawable drawable = imageView.getDrawable();
        drawable.mutate();
        drawable.setColorFilter(color, PorterDuff.Mode.MULTIPLY);
        imageView.setImageDrawable(drawable);
    }

    protected void changeDrawableToGrayscale(ImageView imageView) {
        Drawable drawable = imageView.getDrawable();
        drawable.mutate();
        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(0);
        ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
        drawable.setColorFilter(filter);
        imageView.setImageDrawable(drawable);
    }

    private void rotate360(View v, long duration) {
        final RotateAnimation rotateAnim = new RotateAnimation(0, 1800,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f);

        rotateAnim.setDuration(duration);
        rotateAnim.setRepeatMode(Animation.INFINITE);
        rotateAnim.setRepeatCount(Animation.INFINITE);
        rotateAnim.setFillAfter(true);
        v.startAnimation(rotateAnim);
    }

    private void showImagePicker(boolean img) {
        if (!img) {
            imageFormOpen = true;
            ll_camera_dialog.setVisibility(View.VISIBLE);
        } else {
            imageFormOpen = false;
            ll_camera_dialog.setVisibility(View.GONE);
        }
    }

    private void showFileChooser(int imgType) {
        fileName = "PRO_IMG.jpg";
        switch (imgType) {
            case 1:
                try {
                    /*Calendar cal = Calendar.getInstance();
                    fileName = String.format("PRF_IMG_%d_%d_%d_%d_%d_%d.jpg", cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.YEAR), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));*/
                    Constants.FILES_APP.FOLDER_CBF_CACHE.mkdirs();
                    cache = new File(Constants.FILES_APP.FOLDER_CBF_CACHE, fileName);
                    filePath = cache.getAbsolutePath();
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(ProfileEditActivity.this, BuildConfig.APPLICATION_ID + ".provider", cache));
                    } else {
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(cache));
                    }
                    startActivityForResult(intent, ACTIVITY_CAPTURE_IMAGE);
                } catch (Exception e) {
                    Toast.makeText(this, "Please install a suitable Camera application..!!", Toast.LENGTH_SHORT).show();
                    Log.d("Exception", e.getMessage());
                }
                break;
            case 2:
                try {
                    Toast.makeText(this, "Select IMAGE files", Toast.LENGTH_LONG).show();
                    Intent mRequestImageIntent = new Intent(Intent.ACTION_PICK);
                    mRequestImageIntent.setType(Constants.FILE_TYPES.FILE_IMAGE);
                    startActivityForResult(Intent.createChooser(mRequestImageIntent, "Select an Image to upload"), ACTIVITY_CHOOSE_IMAGE);
                } catch (android.content.ActivityNotFoundException e) {
                    Toast.makeText(this, "Please install a suitable File Manager application..!!", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void copyFile(String sourcePath, File destination, boolean delete) throws IOException {
        source = new File(sourcePath);
        moveFile(source, destination, delete);
        isFileSelected = true;
    }

    private void moveFile(File sourceFile, File destinationFile, boolean delete) {
        try {
            FileUtils.copyFile(sourceFile, destinationFile);
            if (delete) {
                sourceFile.delete();
                isFileSelected = false;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void getMimeType(String Filename) {
        Filename = Filename.toLowerCase();
        //Filename = Filename.replace(" ", "");
        Filename = Filename.replaceAll("[^a-zA-Z0-9.-]", "_");
        //String extension = Filename.substring(Filename.lastIndexOf("."));
        String extension = MimeTypeMap.getFileExtensionFromUrl(Filename);
        Log.d("File Extension", extension);
        if (extension != null) {
            fileType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
    }

    public void getFileSize(String passedPath) {
        source = new File(passedPath);
        fileSize = (Float.parseFloat(String.valueOf(source.length() / Constants.FILE_SIZES.ONE_MB)));
    }

    private Bitmap resizeBitmap(Bitmap bm) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        //Log.d(TAG, "resizeBitmap w: " + width + " h: " + height);
        int targetWidth;
        int targetHeight;
        float scaleFactor;
        if (width > height) {
            scaleFactor = ((float) MAX_IMAGE_DIMENSION) / ((float) width);
            targetWidth = MAX_IMAGE_DIMENSION;
            targetHeight = (int) (height * scaleFactor);
        } else {
            scaleFactor = ((float) MAX_IMAGE_DIMENSION) / ((float) height);
            targetHeight = MAX_IMAGE_DIMENSION;
            targetWidth = (int) (width * scaleFactor);
        }

        //Log.d(TAG, "resizeBitmap scaled to w: " + targetWidth + " h: " + targetHeight);
        return Bitmap.createScaledBitmap(bm, targetWidth, targetHeight, true);
    }

    private void showImagePreview(final Bitmap bm) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    finalOutput = new FileOutputStream(cache.getAbsolutePath());
                    bm.compress(Bitmap.CompressFormat.PNG, 100, finalOutput);
                    finalOutput.close();
                    img_contact.setImageBitmap(bm);
                    showImagePicker(imageFormOpen);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void displayContactFromDb(int Id) {
        ContactDBAdapter dbAdapter = new ContactDBAdapter(this);
        dbAdapter.open();
        contact = dbAdapter.getContact(Id);
        dbAdapter.close();

        if (contact.getUserImage() != null) {
            requestStoragePermission(new Runnable() {
                @Override
                public void run() {
                    File contactImage = new File(Constants.FILES_APP.FOLDER_USER_IMAGE.getAbsolutePath() + "/" + contact.getUserImage());
                    if (contactImage.exists()) {
                        final Bitmap bm = BitmapFactory.decodeFile(contactImage.toString());
                        if (bm != null) runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                img_contact.setImageBitmap(bm);
                            }
                        });
                        //Toast.makeText(this,"Loaded from Local assets",Toast.LENGTH_SHORT).show();
                    } else {
                        userProfilePicPath = fetchProfilePicture(Constants.HTTP.BASE_URL + Constants.FOLDERS_ONLINE.FOLDER_USER_IMAGE + contact.getUserImage());
                        //Toast.makeText(this,"Loaded from Online assets",Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
        setContactDetails(contact);
    }

    private void setContactDetails(Contact contact) {
        String Value = "";
        if (contact.getUserFullName() != "" && !"".equals(contact.getUserFullName()) && contact.getUserFullName() != null) {
            Value = contact.getUserFullName();
        }
        edttxt_contact_fullname.setText(Value);

        Value = "Gender";
        int gs = 0;
        if (contact.getUserGender() != "" && !"".equals(contact.getUserGender()) && contact.getUserGender() != null) {
            Value = contact.getUserGender();
            if (Value.equals("male")) {
                gs = 1;
            } else if (Value.equals("female")) {
                gs = 2;
            } else {
                gs = 0;
            }
        }
        spnr_contact_gender.setSelection(gs);

        Value = "";
        if (contact.getCompanyName() != "" && !"".equals(contact.getCompanyName()) && contact.getCompanyName() != null) {
            Value = contact.getCompanyName();
        }
        edttxt_contact_company_name.setText(Value);

        Value = "";
        if (contact.getProductName() != "" && !"".equals(contact.getProductName()) && contact.getProductName() != null) {
            Value = contact.getProductName();
        }
        edttxt_contact_product_name.setText(Value);

        Value = "";
        if (contact.getUserHouseName() != "" && !"".equals(contact.getUserHouseName()) && contact.getUserHouseName() != null) {
            Value = contact.getUserHouseName();
        }
        edttxt_contact_house_name.setText(Value);
        Value = "";
        if (contact.getUserAddress() != "" && !"".equals(contact.getUserAddress()) && contact.getUserAddress() != null) {
            Value = contact.getUserAddress();
        }
        edttxt_contact_address.setText(Value);

        Value = "";
        if (contact.getUserCity() != "" && !"".equals(contact.getUserCity()) && contact.getUserCity() != null) {
            Value = contact.getUserCity();
        }
        edttxt_contact_city.setText(Value);

        Value = "";
        if (contact.getUserMobile() != "" && !"".equals(contact.getUserMobile()) && contact.getUserMobile() != null) {
            Value = contact.getUserMobile();
        }
        edttxt_contact_mobile.setText(Value);

        Value = "";
        if (contact.getUserEmail() != "" && !"".equals(contact.getUserEmail()) && contact.getUserEmail() != null) {
            Value = contact.getUserEmail();
        }
        edttxt_contact_email.setText(Value);

        Value = "";
        if (contact.getUserDescription() != "" && !"".equals(contact.getUserDescription()) && contact.getUserDescription() != null) {
            Value = contact.getUserDescription();
        }
        edttxt_contact_descreption.setText(Value);
    }

    private String fetchProfilePicture(String url) {
        File ContactImage = makeContactImageFile();
        http.get(url, new FileAsyncHttpResponseHandler(ContactImage) {
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        img_contact.setImageResource(R.drawable.profile);
                    }
                });
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                final Bitmap bm = BitmapFactory.decodeFile(file.toString());
                if (bm != null) runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        img_contact.setImageBitmap(bm);
                    }
                });
            }
        });
        return ContactImage.toString();
    }

    private File makeContactImageFile() {
        Constants.FILES_APP.FOLDER_USER_IMAGE.mkdirs();
        File ContactImage = new File(Constants.FILES_APP.FOLDER_USER_IMAGE, contact.getUserImage());
        return ContactImage;
    }

    private void requestStoragePermission(Runnable runAfterStoragePermissionGrant) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int readStoragePermissionStatus = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
            int writeStoragePermissionStatus = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (readStoragePermissionStatus == PackageManager.PERMISSION_DENIED
                    || writeStoragePermissionStatus == PackageManager.PERMISSION_DENIED) {
                this.runAfterStoragePermissionGrant = runAfterStoragePermissionGrant;
                ActivityCompat.requestPermissions(this, new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE}, Constants.PERMISSION_REQUESTS.REQUEST_FILE_PERMISSIONS);
            } else {
                runAfterStoragePermissionGrant.run();
            }
        } else {
            runAfterStoragePermissionGrant.run();
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void requestAllPermissions(Runnable ifAllSetDoWhat) {
        runAfterPermissionGrant = ifAllSetDoWhat;
        int cameraAccessPermissionStatus = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int readStoragePermissionStatus = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int writeStoragePermissionStatus = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (cameraAccessPermissionStatus == PackageManager.PERMISSION_DENIED
                || readStoragePermissionStatus == PackageManager.PERMISSION_DENIED
                || writeStoragePermissionStatus == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(this, new String[]{
                            Manifest.permission.CAMERA,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    Constants.PERMISSION_REQUESTS.REQUEST_ALL_PERMISSION);
        } else {
            App.preferences.edit()
                    .putBoolean(PreferenceKeys.PERMISSION_CAM, true)
                    .putBoolean(PreferenceKeys.PERMISSION_STR, true)
                    .apply();
            allPermissionsSet = true;
        }
        if (allPermissionsSet) {
            if (runAfterPermissionGrant != null) {
                runAfterPermissionGrant.run();
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case ACTIVITY_CAPTURE_IMAGE:
                try {
                    if (cache.exists()) {
                        Log.d("File Path", cache.getAbsolutePath());
                        msgImageBitmap = BitmapFactory.decodeFile(cache.getAbsolutePath());
                        scaledBm = resizeBitmap(msgImageBitmap);
                        showImagePreview(scaledBm);
                        getMimeType(cache.getName());
                        isFileSelected = true;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case ACTIVITY_CHOOSE_IMAGE:
                if (resultCode == RESULT_OK) {
                    // Get the Uri of the selected file
                    fileUri = data.getData();
                    Log.d("Content Uri", fileUri.toString());
                    if (data.getData().getScheme().equals("content")) {
                        Cursor cursor = getBaseContext().getContentResolver().query(fileUri, new String[]{android.provider.MediaStore.Images.ImageColumns.DATA}, null, null, null);
                        if (cursor.moveToFirst()) {
                            fileUri = Uri.parse("file://" + Uri.parse(cursor.getString(0)).getPath());
                        }
                        cursor.close();
                    }
                    Log.d("File Uri", "" + fileUri.toString());
                    // Get the path of the selected file
                    try {
                        filePath = messageFileUtils.getPath(this, fileUri);
                        Log.d("File Path", "" + filePath);
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                    // Get the name of the selected file
                    String tempfileName = fileUri.getLastPathSegment();
                    Log.d("File Name", tempfileName);
                    // Get the type of the selected file
                    getMimeType(tempfileName);
                    Log.d("File Mime", fileType);
                    if (fileType != Constants.FILE_TYPES.FILE_IMAGE_JPEG && fileType != Constants.FILE_TYPES.FILE_IMAGE_JPG && fileType != Constants.FILE_TYPES.FILE_IMAGE_PNG) {
                        Snackbar.make(activity_profile_edit, "Sorry..!! Wrong file type chosen..!!", Snackbar.LENGTH_LONG).show();
                    } else {
                        // Get the size of the selected file
                        getFileSize(filePath);
                        Log.d("File Size", fileSize + "");
                        if (fileSize < Constants.FILE_SIZES.FILE_IMAGE_SIZE) {
                            cache = new File(Constants.FILES_APP.FOLDER_CBF_CACHE, fileName);
                            try {
                                copyFile(filePath, cache, false);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            if (cache.exists()) {
                                msgImageBitmap = BitmapFactory.decodeFile(cache.getAbsolutePath());
                                scaledBm = resizeBitmap(msgImageBitmap);
                                showImagePreview(scaledBm);
                            }
                        } else {
                            Snackbar.make(activity_profile_edit, "Sorry..!! The file size exceeds " + Constants.FILE_SIZES.FILE_IMAGE_SIZE + "MB..!!", Snackbar.LENGTH_LONG).show();
                        }
                    }
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public static boolean isEmailValid(String Email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = Email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_contact:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestAllPermissions(new Runnable() {
                        @Override
                        public void run() {
                            showImagePicker(imageFormOpen);
                        }
                    });
                } else {
                    showImagePicker(imageFormOpen);
                }
                break;
            case R.id.img_pro_edit_camera:
                showFileChooser(1);
                break;
            case R.id.img_pro_edit_gallery:
                showFileChooser(2);
                break;
            case R.id.ll_save_profile:
                if (!updatePressed) {
                    boolean allFieldsOK = true;
                    if (!isFileSelected) {
                        //cache = new File(Constants.FILES_APP.FOLDER_USER_IMAGE, contact.getUserImage());
                        if (contact.getUserImage().equals("")) {
                            Toast.makeText(this, "Provide a User Image..!!", Toast.LENGTH_SHORT).show();
                            allFieldsOK = false;
                            break;
                        } else {
                            cache = new File(Constants.FILES_APP.FOLDER_USER_IMAGE, contact.getUserImage());
                            getMimeType(cache.getName());
                        }
                    }
                    String CompanyName = edttxt_contact_company_name.getText().toString();
                    if (CompanyName.length() == 0) {
                        Toast.makeText(this, "Provide a valid Company Name..!!", Toast.LENGTH_SHORT).show();
                        allFieldsOK = false;
                        break;
                    }
                    String ProductName = edttxt_contact_product_name.getText().toString();
                    if (ProductName.length() == 0) {
                        Toast.makeText(this, "Provide a valid Product Name..!!", Toast.LENGTH_SHORT).show();
                        allFieldsOK = false;
                        break;
                    }
                    String Name = edttxt_contact_fullname.getText().toString();
                    if (Name.length() == 0) {
                        Toast.makeText(this, "Provide a valid Name..!!", Toast.LENGTH_SHORT).show();
                        allFieldsOK = false;
                        break;
                    }
                    String HouseName = edttxt_contact_house_name.getText().toString();
                    String Gender = spnr_contact_gender.getSelectedItem().toString().toLowerCase();
                    if (Gender.equals("gender")) {
                        Toast.makeText(this, "Provide a valid Gender..!!", Toast.LENGTH_SHORT).show();
                        allFieldsOK = false;
                        break;
                    }
                    String Address = edttxt_contact_address.getText().toString();
                    String City = edttxt_contact_city.getText().toString();
                    String Pin = "";
                    String Landline = "";
                    String Mobile = edttxt_contact_mobile.getText().toString();
                    if (Mobile.length() == 0 || !(Mobile.matches("[0-9]{10}"))) {
                        Toast.makeText(this, "Provide a valid Mobile Number..!!", Toast.LENGTH_SHORT).show();
                        allFieldsOK = false;
                        break;
                    }
                    String Email = edttxt_contact_email.getText().toString();
                    boolean validEmail = isEmailValid(Email);
                    if (Email.length() == 0 || !validEmail) {
                        Toast.makeText(this, "Provide a valid Email Address..!!", Toast.LENGTH_SHORT).show();
                        allFieldsOK = false;
                        break;
                    }
                    String UserDescription = edttxt_contact_descreption.getText().toString();
                    if (allFieldsOK) {
                        updatePressed = true;
                        ll_profile_update_animation.setVisibility(View.VISIBLE);
                        App.api.profileUpdate(
                                new TypedString(App.preferences.getString(PreferenceKeys.TOKEN, null)),
                                new TypedString(contact.getPKUserID()),
                                new TypedFile(fileType, cache),
                                new TypedString(contact.getUserImage()),
                                new TypedString(CompanyName),
                                new TypedString(ProductName),
                                new TypedString(Name),
                                new TypedString(HouseName),
                                new TypedString(Gender),
                                new TypedString(Address),
                                new TypedString(City),
                                new TypedString(Pin),
                                new TypedString(Landline),
                                new TypedString(Mobile),
                                new TypedString(Email),
                                new TypedString(UserDescription),
                                new Callback<ProfileUpdate>() {
                                    @Override
                                    public void success(final ProfileUpdate profileUpdate, Response response) {
                                        try {
                                            updatePressed = false;
                                            ll_profile_update_animation.setVisibility(View.GONE);
                                            if (profileUpdate.ProfileUpdateFlag == 1) {
                                                ContactDBAdapter dbAdapter = new ContactDBAdapter(getBaseContext());
                                                dbAdapter.open();
                                                dbAdapter.writeToDb(profileUpdate.Contacts);
                                                dbAdapter.close();
                                                Toast.makeText(ProfileEditActivity.this, "Profile updated successfully", Toast.LENGTH_SHORT).show();
                                                if (isFileSelected) {
                                                    cache.delete();
                                                }
                                                startActivity(new Intent(ProfileEditActivity.this, ContactDetailActivity.class).putExtra(EXTRA_CONTACT_ID, String.valueOf(userID)));
                                                //startActivity(new Intent(ProfileEditActivity.this, ContactDetailActivity.class).putExtra(EXTRA_CONTACT_ID, String.valueOf(App.preferences.getInt(PreferenceKeys.LOGIN_ID, 0))));
                                                finish();
                                            } else {
                                                Toast.makeText(ProfileEditActivity.this, "Sorry..!! Unable to update the profile. Please try after a short while..!!", Toast.LENGTH_SHORT).show();
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        try {
                                            updatePressed = false;
                                            ll_profile_update_animation.setVisibility(View.GONE);
                                            Toast.makeText(ProfileEditActivity.this, "Can't connect to Server! Please check your Network Connection!", Toast.LENGTH_SHORT).show();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                    }
                } else {
                    Toast.makeText(ProfileEditActivity.this, "Profile Updation in progress..!! Please Wait..!!", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (imageFormOpen){
            showImagePicker(imageFormOpen);
            return;
        }
        if (isFileSelected) {
            cache.delete();
        }
        startActivity(new Intent(this, ContactDetailActivity.class).putExtra(EXTRA_CONTACT_ID, String.valueOf(userID)));
        finish();
    }
}
