package com.blidus.catholicbusinessforum.ui;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blidus.catholicbusinessforum.App;
import com.blidus.catholicbusinessforum.R;
import com.blidus.catholicbusinessforum.adapters.uiAdapters.AboutSliderAdapter;
import com.blidus.catholicbusinessforum.controllers.uiControllers.AboutSliderController;
import com.blidus.catholicbusinessforum.global.Constants;
import com.blidus.catholicbusinessforum.global.PreferenceKeys;

public class AboutActivity extends AppCompatActivity implements View.OnClickListener {

    private int colorPrimary, colorAccent, colorBackgroundImage;

    private boolean autoslide = false;
    private Handler handler;
    private ViewPager vp_about_slider;
    private AboutSliderAdapter aboutSliderAdapter;
    private AboutSliderController aboutSliderController;

    private LinearLayout ll_actionbar;

    private TextView txt_governing_bodies_title;
    private TextView txt_governing_body_1_name, txt_governing_body_2_name, txt_governing_body_3_name, txt_governing_body_4_name, txt_governing_body_5_name;
    private TextView txt_terms_and_conditions, txt_powered_by;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        if (App.preferences.getBoolean(PreferenceKeys.USER_DEFINED_THEME_COLOR, false)) {
            colorPrimary = Color.parseColor(App.preferences.getString(PreferenceKeys.USER_DEFINED_THEME_COLOR_PRIMARY, null));
            colorAccent = Color.parseColor(App.preferences.getString(PreferenceKeys.USER_DEFINED_THEME_COLOR_ACCENT, null));
            colorBackgroundImage = Color.parseColor(App.preferences.getString(PreferenceKeys.USER_DEFINED_THEME_COLOR_BACKGROUND_IMAGE, null));
        } else {
            colorPrimary = ContextCompat.getColor(this, R.color.colorPrimary);
            colorAccent = ContextCompat.getColor(this, R.color.colorAccent);
            colorBackgroundImage = ContextCompat.getColor(this, R.color.colorBackgroundImage);
        }
        configView();
    }

    private void configView(){

        handler = new Handler();

        ll_actionbar = (LinearLayout) findViewById(R.id.ll_actionbar);

        sliderStart();
        vp_about_slider = (ViewPager) findViewById(R.id.vp_about_slider);
        aboutSliderAdapter = new AboutSliderAdapter(AboutActivity.this);
        vp_about_slider.setAdapter(aboutSliderAdapter);
        aboutSliderController = new AboutSliderController(AboutActivity.this);
        aboutSliderController.applyViewPagerScrollSpeed(vp_about_slider, 1000);

        txt_governing_bodies_title = (TextView) findViewById(R.id.txt_governing_bodies_title);
        txt_governing_body_1_name = (TextView) findViewById(R.id.txt_governing_body_1_name);
        txt_governing_body_2_name = (TextView) findViewById(R.id.txt_governing_body_2_name);
        txt_governing_body_3_name = (TextView) findViewById(R.id.txt_governing_body_3_name);
        txt_governing_body_4_name = (TextView) findViewById(R.id.txt_governing_body_4_name);
        txt_governing_body_5_name = (TextView) findViewById(R.id.txt_governing_body_5_name);

        txt_terms_and_conditions = (TextView) findViewById(R.id.txt_terms_and_conditions);
        txt_powered_by = (TextView) findViewById(R.id.txt_powered_by);

        setColorToElements();

        txt_terms_and_conditions.setOnClickListener(this);
        txt_powered_by.setOnClickListener(this);
    }

    private void setColorToElements() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(colorPrimary);
        }

        ll_actionbar.setBackgroundColor(colorPrimary);

        txt_governing_bodies_title.setTextColor(colorPrimary);
        txt_governing_body_1_name.setTextColor(colorPrimary);
        txt_governing_body_2_name.setTextColor(colorPrimary);
        txt_governing_body_3_name.setTextColor(colorPrimary);
        txt_governing_body_4_name.setTextColor(colorPrimary);
        txt_governing_body_5_name.setTextColor(colorPrimary);

        txt_terms_and_conditions.setTextColor(colorPrimary);
        txt_powered_by.setTextColor(colorPrimary);

    }

    protected void sliderStart() {
        autoslide = true;

        handler.postDelayed(new Runnable() {
            int page = 0;

            @Override
            public void run() {
                page++;
                if (page == AboutSliderAdapter.IMG_COUNT) page = 0;
                vp_about_slider.setCurrentItem(page, true);
                if (autoslide) handler.postDelayed(this, 3000);
            }
        }, 5000);
    }

    @Override
    protected void onStop() {
        autoslide = false;
        super.onStop();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.txt_powered_by:
                startActivity(new Intent(AboutActivity.this, DevelopersActivity.class));
                finish();
                break;
            case R.id.txt_terms_and_conditions:
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(Constants.HTTP.PRIVACY_POLICY_URL));
                startActivity(i);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(AboutActivity.this, MainActivity.class));
        finish();
    }
}