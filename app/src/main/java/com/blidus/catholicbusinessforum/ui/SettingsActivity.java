package com.blidus.catholicbusinessforum.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.blidus.catholicbusinessforum.App;
import com.blidus.catholicbusinessforum.R;
import com.blidus.catholicbusinessforum.apiCallbacks.PasswordChange;
import com.blidus.catholicbusinessforum.global.PreferenceKeys;

import net.margaritov.preference.colorpicker.ColorPickerDialog;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static androidx.core.graphics.ColorUtils.HSLToColor;
import static androidx.core.graphics.ColorUtils.colorToHSL;

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener {

    private boolean mediaDownload = false;
    private boolean appTheme = false;
    private boolean accountPreference = false;
    private boolean allPasswordChangeFieldsOK = false;
    private boolean passwordChangePressed = false;
    private Handler handler;

    private GradientDrawable form_background_1, form_background_2, form_background_3;

    private FrameLayout fl_actionbar;
    private ImageView img_background;

    private LinearLayout ll_media_download_outer, ll_media_download_title, ll_media_download_contents;
    private TextView txt_media_download_title;
    private ImageView img_download_indicator;
    private Switch sw_image, sw_video, sw_audio, sw_attach;

    private LinearLayout ll_application_color_outer, ll_application_color_title, ll_application_color_contents;
    private TextView txt_application_color_title;
    private ImageView img_application_color_indicator;
    private Button btn_set_default_color, btn_choose_application_color;

    private LinearLayout ll_account_preference_outer, ll_account_preference_title, ll_account_preference_contents;
    private TextView txt_account_preference_title;
    private ImageView img_account_preference_indicator;
    private LinearLayout ll_password_change_form;
    private TextView txt_password_change_title;
    private EditText edttxt_current_password, edttxt_new_password, edttxt_confirm_password;
    private LinearLayout ll_divider_1, ll_divider_2;
    private Button btn_update_password;
    private GradientDrawable ll_password_change_form_background, edttxt_current_password_background, edttxt_new_password_background, edttxt_confirm_password_background, btn_update_password_background;

    private LinearLayout ll_contact_edit_mode_form;
    private TextView txt_contact_edit_mode_title;
    private Switch sw_edit_mode;
    private GradientDrawable ll_contact_edit_mode_form_background;


    private ColorPickerDialog colorPickerDialog;
    private int colorPrimary, colorAccent, colorBackgroundImage, colorMyMessage;
    private Typeface typeFace;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        typeFace = Typeface.createFromAsset(getAssets(),"fonts/bebas.ttf");
        if (App.preferences.getBoolean(PreferenceKeys.USER_DEFINED_THEME_COLOR, false)) {
            colorPrimary = Color.parseColor(App.preferences.getString(PreferenceKeys.USER_DEFINED_THEME_COLOR_PRIMARY, null));
            colorAccent = Color.parseColor(App.preferences.getString(PreferenceKeys.USER_DEFINED_THEME_COLOR_ACCENT, null));
            colorBackgroundImage = Color.parseColor(App.preferences.getString(PreferenceKeys.USER_DEFINED_THEME_COLOR_BACKGROUND_IMAGE, null));
        } else {
            colorPrimary = ContextCompat.getColor(this, R.color.colorPrimary);
            colorAccent = ContextCompat.getColor(this, R.color.colorAccent);
            colorBackgroundImage = ContextCompat.getColor(this, R.color.colorBackgroundImage);
        }
        configView();
    }

    private void configView() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(colorPrimary);
        }

        handler = new Handler();

        fl_actionbar = (FrameLayout) findViewById(R.id.fl_actionbar);
        img_background = (ImageView) findViewById(R.id.img_background);

        ll_media_download_outer = (LinearLayout) findViewById(R.id.ll_media_download_outer);
        ll_media_download_title = (LinearLayout) findViewById(R.id.ll_media_download_title);
        txt_media_download_title = (TextView) findViewById(R.id.txt_media_download_title);
        //txt_media_download_title.setTypeface(typeFace);
        ll_media_download_contents = (LinearLayout) findViewById(R.id.ll_media_download_contents);
        form_background_1 = (GradientDrawable) ll_media_download_contents.getBackground();
        menuAnimation(ll_media_download_contents, 1, false);
        img_download_indicator = (ImageView) findViewById(R.id.img_download_indicator);

        sw_image = (Switch) findViewById(R.id.sw_image);
        sw_image.setChecked(App.preferences.getBoolean(PreferenceKeys.MEDIA_AUTO_DOWNLOAD_IMAGE, false));
        sw_video = (Switch) findViewById(R.id.sw_video);
        sw_video.setChecked(App.preferences.getBoolean(PreferenceKeys.MEDIA_AUTO_DOWNLOAD_VIDEO, false));
        sw_audio = (Switch) findViewById(R.id.sw_audio);
        sw_audio.setChecked(App.preferences.getBoolean(PreferenceKeys.MEDIA_AUTO_DOWNLOAD_AUDIO, false));
        sw_attach = (Switch) findViewById(R.id.sw_attach);
        sw_attach.setChecked(App.preferences.getBoolean(PreferenceKeys.MEDIA_AUTO_DOWNLOAD_FILE, false));


        ll_application_color_outer = (LinearLayout) findViewById(R.id.ll_application_color_outer);
        ll_application_color_title = (LinearLayout) findViewById(R.id.ll_application_color_title);
        txt_application_color_title = (TextView) findViewById(R.id.txt_application_color_title);
        ll_application_color_contents = (LinearLayout) findViewById(R.id.ll_application_color_contents);
        form_background_2 = (GradientDrawable) ll_application_color_contents.getBackground();
        menuAnimation(ll_application_color_contents, 2, false);
        img_application_color_indicator = (ImageView) findViewById(R.id.img_application_color_indicator);

        btn_set_default_color = (Button) findViewById(R.id.btn_set_default_color);
        btn_set_default_color.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        btn_choose_application_color = (Button) findViewById(R.id.btn_choose_application_color);


        ll_account_preference_outer = (LinearLayout) findViewById(R.id.ll_account_preference_outer);
        ll_account_preference_title = (LinearLayout) findViewById(R.id.ll_account_preference_title);
        txt_account_preference_title = (TextView) findViewById(R.id.txt_account_preference_title);
        ll_account_preference_contents = (LinearLayout) findViewById(R.id.ll_account_preference_contents);
        form_background_3 = (GradientDrawable) ll_account_preference_contents.getBackground();
        menuAnimation(ll_account_preference_contents, 3, false);
        img_account_preference_indicator = (ImageView) findViewById(R.id.img_account_preference_indicator);

        ll_password_change_form = (LinearLayout) findViewById(R.id.ll_password_change_form);
        ll_password_change_form_background = (GradientDrawable) ll_password_change_form.getBackground();
        txt_password_change_title = (TextView) findViewById(R.id.txt_password_change_title);
        edttxt_current_password = (EditText) findViewById(R.id.edttxt_current_password);
        ll_divider_1 = (LinearLayout) findViewById(R.id.ll_divider_1);
        //edttxt_current_password_background = (GradientDrawable) edttxt_current_password.getBackground();
        edttxt_new_password = (EditText) findViewById(R.id.edttxt_new_password);
        ll_divider_2 = (LinearLayout) findViewById(R.id.ll_divider_2);
        //edttxt_new_password_background = (GradientDrawable) edttxt_new_password.getBackground();
        edttxt_confirm_password = (EditText) findViewById(R.id.edttxt_confirm_password);
        //edttxt_confirm_password_background = (GradientDrawable) edttxt_confirm_password.getBackground();
        btn_update_password = (Button) findViewById(R.id.btn_update_password);
        btn_update_password_background = (GradientDrawable) btn_update_password.getBackground();

        txt_contact_edit_mode_title = (TextView) findViewById(R.id.txt_contact_edit_mode_title);
        ll_contact_edit_mode_form = (LinearLayout) findViewById(R.id.ll_contact_edit_mode_form);
        ll_contact_edit_mode_form_background = (GradientDrawable) ll_contact_edit_mode_form.getBackground();
        sw_edit_mode = (Switch) findViewById(R.id.sw_edit_mode);
        sw_edit_mode.setChecked(App.preferences.getBoolean(PreferenceKeys.ADMIN_CONTACTS_EDIT_MODE, false));

        if (App.preferences.getInt(PreferenceKeys.LOGIN_TYPE, 0) == 1){
            ll_contact_edit_mode_form.setVisibility(View.VISIBLE);
        }

        setColorToElements();


        ll_media_download_title.setOnClickListener(this);
        ll_application_color_title.setOnClickListener(this);
        ll_account_preference_title.setOnClickListener(this);
        btn_set_default_color.setOnClickListener(this);
        btn_choose_application_color.setOnClickListener(this);
        btn_update_password.setOnClickListener(this);
        sw_image.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                App.preferences.edit().putBoolean(PreferenceKeys.MEDIA_AUTO_DOWNLOAD_IMAGE, isChecked).apply();
            }
        });
        sw_video.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                App.preferences.edit().putBoolean(PreferenceKeys.MEDIA_AUTO_DOWNLOAD_VIDEO, isChecked).apply();
            }
        });
        sw_audio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                App.preferences.edit().putBoolean(PreferenceKeys.MEDIA_AUTO_DOWNLOAD_AUDIO, isChecked).apply();
            }
        });
        sw_attach.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                App.preferences.edit().putBoolean(PreferenceKeys.MEDIA_AUTO_DOWNLOAD_FILE, isChecked).apply();
            }
        });
        sw_edit_mode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                App.preferences.edit().putBoolean(PreferenceKeys.ADMIN_CONTACTS_EDIT_MODE, isChecked).apply();
            }
        });
    }

    private void setColorToElements(){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(colorPrimary);
        }

        fl_actionbar.setBackgroundColor(colorPrimary);
        changeDrawableColor(img_background , colorBackgroundImage);
        txt_media_download_title.setTextColor(colorPrimary);
        sw_image.setTextColor(colorPrimary);
        sw_image.setHighlightColor(colorPrimary);
        sw_image.setDrawingCacheBackgroundColor(colorAccent);
        sw_video.setTextColor(colorPrimary);
        sw_video.setHighlightColor(colorPrimary);
        sw_video.setDrawingCacheBackgroundColor(colorAccent);
        sw_audio.setTextColor(colorPrimary);
        sw_audio.setHighlightColor(colorPrimary);
        sw_audio.setDrawingCacheBackgroundColor(colorAccent);
        sw_attach.setTextColor(colorPrimary);
        sw_attach.setHighlightColor(colorPrimary);
        sw_attach.setDrawingCacheBackgroundColor(colorAccent);
        txt_application_color_title.setTextColor(colorPrimary);
        txt_account_preference_title.setTextColor(colorPrimary);
        changeDrawableColor(img_download_indicator, colorPrimary);
        changeDrawableColor(img_application_color_indicator, colorPrimary);
        changeDrawableColor(img_account_preference_indicator, colorPrimary);
        form_background_1.setColor(ContextCompat.getColor(this, R.color.colorWhite));
        form_background_1.setStroke(3, colorPrimary);
        form_background_2.setColor(ContextCompat.getColor(this, R.color.colorWhite));
        form_background_2.setStroke(3, colorPrimary);
        form_background_3.setColor(ContextCompat.getColor(this, R.color.colorWhite));
        form_background_3.setStroke(3, colorPrimary);
        btn_choose_application_color.setBackgroundColor(colorPrimary);
        ll_password_change_form_background.setColor(ContextCompat.getColor(this, R.color.colorWhite));
        ll_password_change_form_background.setStroke(3, colorPrimary);
        txt_password_change_title.setTextColor(colorPrimary);
        edttxt_current_password.setTextColor(colorPrimary);
        edttxt_current_password.setHintTextColor(colorAccent);
        ll_divider_1.setBackgroundColor(colorPrimary);
        /*edttxt_current_password_background.setColor(ContextCompat.getColor(this, R.color.colorWhite));
        edttxt_current_password_background.setStroke(3, colorPrimary);*/
        edttxt_new_password.setTextColor(colorPrimary);
        edttxt_new_password.setHintTextColor(colorAccent);
        ll_divider_2.setBackgroundColor(colorPrimary);
        /*edttxt_new_password_background.setColor(ContextCompat.getColor(this, R.color.colorWhite));
        edttxt_new_password_background.setStroke(3, colorPrimary);*/
        edttxt_confirm_password.setTextColor(colorPrimary);
        edttxt_confirm_password.setHintTextColor(colorAccent);
        /*edttxt_confirm_password_background.setColor(ContextCompat.getColor(this, R.color.colorWhite));
        edttxt_confirm_password_background.setStroke(3, colorPrimary);*/
        btn_update_password_background.setColor(colorPrimary);

        ll_contact_edit_mode_form_background.setColor(ContextCompat.getColor(this, R.color.colorWhite));
        ll_contact_edit_mode_form_background.setStroke(3, colorPrimary);
        txt_contact_edit_mode_title.setTextColor(colorPrimary);
        sw_edit_mode.setTextColor(colorPrimary);
        sw_edit_mode.setHighlightColor(colorPrimary);
        sw_edit_mode.setDrawingCacheBackgroundColor(colorAccent);

    }

    private void changeDrawableColor(ImageView imageView, int color) {
        Drawable d = imageView.getDrawable();
        d.mutate();
        d.setColorFilter(color, PorterDuff.Mode.MULTIPLY);
        imageView.setImageDrawable(d);
    }

    public void menuAnimation(final View v, int position, final boolean direction) {
        int titleHeight = v.getHeight() * position;
        int viewHeight = v.getHeight();
        int totalDistance;
        if (direction) {
            v.setVisibility(View.VISIBLE);
            totalDistance = 0;
        } else {
            totalDistance = -(viewHeight);
            //totalDistance = -(titleHeight + viewHeight);
        }
        v.animate().translationY(totalDistance)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        if (direction) {
                            v.setVisibility(View.VISIBLE);
                        } else {
                            v.setVisibility(View.GONE);
                        }
                    }
                });
    }

    private void rotate(View v, float degree1, float degree2) {
        final RotateAnimation rotateAnim = new RotateAnimation(degree1, degree2,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f);

        rotateAnim.setDuration(500);
        rotateAnim.setFillAfter(true);
        v.startAnimation(rotateAnim);
    }

    private boolean menuChanged(boolean b, View v1, View v2, int position) {
        int d1, d2;
        if (b) {
            d1 = 90;
            d2 = 0;
        } else {
            d1 = 0;
            d2 = 90;
        }
        b = !b;
        rotate(v2, d1, d2);
        menuAnimation(v1, position, b);
        return b;
    }

    public void changeAllMenus(int a) {
        switch (a) {
            case 1:
                mediaDownload = menuChanged(mediaDownload, ll_media_download_contents, img_download_indicator, 1);
                if (appTheme) {
                    appTheme = menuChanged(appTheme, ll_application_color_contents, img_application_color_indicator, 2);
                }
                if (accountPreference) {
                    accountPreference = menuChanged(accountPreference, ll_account_preference_contents, img_account_preference_indicator, 3);
                }
                break;
            case 2:
                if (mediaDownload) {
                    mediaDownload = menuChanged(mediaDownload, ll_media_download_contents, img_download_indicator, 1);
                }
                appTheme = menuChanged(appTheme, ll_application_color_contents, img_application_color_indicator, 2);
                if (accountPreference) {
                    accountPreference = menuChanged(accountPreference, ll_account_preference_contents, img_account_preference_indicator, 3);
                }
                break;
            case 3:
                if (mediaDownload) {
                    mediaDownload = menuChanged(mediaDownload, ll_media_download_contents, img_download_indicator, 1);
                }
                if (appTheme) {
                    appTheme = menuChanged(appTheme, ll_application_color_contents, img_application_color_indicator, 2);
                }
                accountPreference = menuChanged(accountPreference, ll_account_preference_contents, img_account_preference_indicator, 3);
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_media_download_title:
                changeAllMenus(1);
                break;
            case R.id.ll_application_color_title:
                changeAllMenus(2);
                break;
            case R.id.btn_set_default_color:
                colorPrimary = ContextCompat.getColor(this, R.color.colorPrimary);
                colorAccent = ContextCompat.getColor(this, R.color.colorAccent);
                colorBackgroundImage = ContextCompat.getColor(this, R.color.colorBackgroundImage);
                colorMyMessage = ContextCompat.getColor(this, R.color.colorMyMsg);
                App.preferences.edit()
                        .putBoolean(PreferenceKeys.USER_DEFINED_THEME_COLOR, false)
                        .putString(PreferenceKeys.USER_DEFINED_THEME_COLOR_PRIMARY, null)
                        .putString(PreferenceKeys.USER_DEFINED_THEME_COLOR_ACCENT, null)
                        .putString(PreferenceKeys.USER_DEFINED_THEME_COLOR_BACKGROUND_IMAGE, null)
                        .apply();
                setColorToElements();
                break;
            case R.id.btn_choose_application_color:
                colorPickerDialog = new ColorPickerDialog(SettingsActivity.this, colorPrimary);
                colorPickerDialog.setAlphaSliderVisible(false);
                colorPickerDialog.setHexValueEnabled(false);
                colorPickerDialog.setTitle(R.string.color_picker_title);
                colorPickerDialog.setOnColorChangedListener(new ColorPickerDialog.OnColorChangedListener() {
                    @Override
                    public void onColorChanged(int color) {
                        colorPrimary = color;

                        float[] outHsl1 = new float[3];
                        colorToHSL (colorPrimary, outHsl1);
                        outHsl1[2] = (float) 0.97;
                        colorBackgroundImage =  HSLToColor(outHsl1);

                        float[] outHsl2 = new float[3];
                        colorToHSL (colorPrimary, outHsl2);
                        outHsl2[2] = (float) 0.9;
                        colorMyMessage =  HSLToColor(outHsl2);

                        String colorHexValue = Integer.toHexString(color);
                        String bgImgColorHexValue = Integer.toHexString(colorBackgroundImage);
                        String myMessageColorHexValue = Integer.toHexString(colorMyMessage);
                        colorHexValue = colorHexValue.substring(colorHexValue.length() - 6);
                        bgImgColorHexValue = bgImgColorHexValue.substring(bgImgColorHexValue.length() - 6);
                        myMessageColorHexValue = myMessageColorHexValue.substring(myMessageColorHexValue.length() - 6);
                        colorAccent = Color.parseColor("#88"+colorHexValue);
                        Log.d("Color", colorHexValue);
                        Log.d("Background Color", bgImgColorHexValue);
                        Log.d("My Message Color", myMessageColorHexValue);
                        App.preferences.edit()
                                .putBoolean(PreferenceKeys.USER_DEFINED_THEME_COLOR, true)
                                .putString(PreferenceKeys.USER_DEFINED_THEME_COLOR_PRIMARY, "#FF"+colorHexValue)
                                .putString(PreferenceKeys.USER_DEFINED_THEME_COLOR_ACCENT, "#88"+colorHexValue)
                                .putString(PreferenceKeys.USER_DEFINED_THEME_COLOR_BACKGROUND_IMAGE, "#FF"+bgImgColorHexValue)
                                .putString(PreferenceKeys.USER_DEFINED_THEME_COLOR_MY_MESSAGE, "#FF"+myMessageColorHexValue)
                                .apply();
                        setColorToElements();
                    }
                });
                colorPickerDialog.show();
                break;
            case R.id.ll_account_preference_title:
                changeAllMenus(3);
                break;
            case R.id.btn_update_password:
                if (!passwordChangePressed) {
                    allPasswordChangeFieldsOK = true;
                    String currentPass = edttxt_current_password.getText().toString();
                    if (currentPass.length() == 0) {
                        Toast.makeText(this, "Provide Current Password..!!", Toast.LENGTH_SHORT).show();
                        allPasswordChangeFieldsOK = false;
                        break;
                    }
                    String newPass = edttxt_new_password.getText().toString();
                    if (newPass.length() == 0) {
                        Toast.makeText(this, "Provide New Password..!!", Toast.LENGTH_SHORT).show();
                        allPasswordChangeFieldsOK = false;
                        break;
                    }
                    String confirmPass = edttxt_confirm_password.getText().toString();
                    if (confirmPass.length() == 0) {
                        Toast.makeText(this, "Confirm New Password..!!", Toast.LENGTH_SHORT).show();
                        allPasswordChangeFieldsOK = false;
                        break;
                    } else if (!confirmPass.equals(newPass)) {
                        Toast.makeText(this, "Confirm Password Mismatch..!!", Toast.LENGTH_SHORT).show();
                        allPasswordChangeFieldsOK = false;
                        break;
                    }
                    if (allPasswordChangeFieldsOK) {
                        passwordChangePressed = true;
                        App.api.changePassword(App.preferences.getString(PreferenceKeys.TOKEN, null), App.preferences.getInt(PreferenceKeys.LOGIN_ID, 0), currentPass, newPass, new Callback<PasswordChange>() {
                            @Override
                            public void success(final PasswordChange passwordChange, Response response) {
                                passwordChangePressed = false;
                                if (passwordChange.PasswordUpdateFlag == 1) {
                                    Toast.makeText(SettingsActivity.this, "Password Updated Successful", Toast.LENGTH_SHORT).show();
                                    edttxt_current_password.setText("");
                                    edttxt_new_password.setText("");
                                    edttxt_confirm_password.setText("");
                                    changeAllMenus(3);
                                } else if (passwordChange.PasswordUpdateFlag == 0) {
                                    if (passwordChange.CurrentPasswordFlag == 0) {
                                        Toast.makeText(SettingsActivity.this, "Current Password Incorrect..!!", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(SettingsActivity.this, "Incorrect Current Password..!!", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                passwordChangePressed = false;
                                Toast.makeText(SettingsActivity.this, "Sorry..!! Unable to update password now. Please try after a short while..!!", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                } else {
                    Toast.makeText(SettingsActivity.this, "Password Updation in progress..!! Please try after a short while..!!", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (mediaDownload) {
            changeAllMenus(1);
            return;
        } else if (appTheme){
            changeAllMenus(2);
            return;
        } else if (accountPreference){
            changeAllMenus(3);
            return;
        }
        startActivity(new Intent(SettingsActivity.this, MainActivity.class));
        finish();
    }
}
